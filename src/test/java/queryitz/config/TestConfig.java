package queryitz.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;

@Configuration
public class TestConfig {

  @Bean
  public MultipartResolver multipartResolver() {
    // Im guessing there is a better way to do this???
    // Need this to be null for file upload testing

    return null;
  }
}
