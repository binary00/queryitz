package queryitz.modules.auth;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import queryitz.common.BaseTest;
import queryitz.modules.auth.domain.AuthStatus;
import queryitz.modules.auth.domain.LoginRequest;

import static org.junit.Assert.*;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static queryitz.modules.user.constants.Constants.DEFAULT_USER;
import static queryitz.modules.user.constants.Constants.DEFAULT_USER_PWORD;

public class AuthTests extends BaseTest {

  @Autowired private AuthTestService authService;

  @Test
  public void testAuth() throws Exception {
    LoginRequest loginRequest = new LoginRequest();
    loginRequest.setUsername(DEFAULT_USER);
    loginRequest.setPassword(DEFAULT_USER_PWORD);

    authService.login(mockMvc, session, loginRequest, OK);
    assertEquals(DEFAULT_USER, getSessionUserName());
    AuthStatus status = authService.status(mockMvc, session);
    assertTrue(status.isAuthenticated());
    assertEquals(1, status.getUserID());

    authService.logout(mockMvc, session, OK);
    assertEquals(null, getSessionUserName());
    status = authService.status(mockMvc, session);
    assertFalse(status.isAuthenticated());

    loginRequest.setPassword("invalid");
    authService.login(mockMvc, session, loginRequest, UNAUTHORIZED);
    assertEquals(null, getSessionUserName());
  }

}
