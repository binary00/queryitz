package queryitz.modules.auth;

import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import queryitz.common.JSONTestService;
import queryitz.modules.auth.domain.AuthStatus;
import queryitz.modules.auth.domain.LoginRequest;

@Service
public class AuthTestService extends JSONTestService {

  private static final String BASE = "/auth";

  public void login(MockMvc mockMvc, MockHttpSession session, LoginRequest loginRequest, HttpStatus status) {
    testPost(BASE + "/login", mockMvc, session, loginRequest, null, status);
  }

  public void logout(MockMvc mockMvc, MockHttpSession session, HttpStatus status) {
    testDelete(BASE + "/logout", mockMvc, session, status);
  }

  public AuthStatus status(MockMvc mockMvc, MockHttpSession session) {
    return testGet(BASE + "/status", mockMvc, session, AuthStatus.class);
  }
}
