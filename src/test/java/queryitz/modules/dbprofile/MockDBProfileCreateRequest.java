package queryitz.modules.dbprofile;


import org.springframework.mock.web.MockMultipartFile;
import queryitz.modules.dbprofile.domain.DBProfileCreateRequest;

public class MockDBProfileCreateRequest extends DBProfileCreateRequest {

  private MockMultipartFile driver;

  public MockMultipartFile getDriver() {
    return driver;
  }

  public void setDriver(MockMultipartFile driver) {
    this.driver = driver;
  }
}
