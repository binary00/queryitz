package queryitz.modules.dbprofile;

import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import queryitz.common.JSONTestService;
import queryitz.modules.dbprofile.domain.DBProfile;

import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Service
public class DBProfileTestService extends JSONTestService {

  private static final String BASE = "/dbprofiles";

  public DBProfile createDBProfile(MockMvc mockMvc, MockHttpSession session, MockDBProfileCreateRequest createRequest) {
    MockHttpServletRequestBuilder request = fileUpload(BASE).file(createRequest.getDriver()).session(session);

    request.param("name", createRequest.getName());
    request.param("connectionURL", createRequest.getConnectionURL());
    request.param("username", createRequest.getUsername());
    request.param("password", createRequest.getPassword());

    if(createRequest.getTeamIDs() != null) {
      Set<Long> teamIDs = createRequest.getTeamIDs();
      request.param("teamIDs", StringUtils.join(teamIDs.toArray(), ",").split(","));
    }

    if(createRequest.getUserIDs() != null) {
      Set<Long> userIDs = createRequest.getUserIDs();
      request.param("userIDs", StringUtils.join(userIDs.toArray(), ",").split(","));
    }

    String responseString = null;
    try {
      responseString = mockMvc
          .perform(request.secure(true))
          .andExpect(status().isOk())
          .andReturn()
          .getResponse()
          .getContentAsString();
    } catch(Exception e) {
      e.printStackTrace();
    }

    return jsonUtils.mapObject(responseString, DBProfile.class);
  }

  public DBProfile getDBProfile(MockMvc mockMvc, MockHttpSession session, long id) {
    return getDBProfile(mockMvc, session, id, HttpStatus.OK);
  }

  public DBProfile getDBProfile(MockMvc mockMvc, MockHttpSession session, long id, HttpStatus status) {
    return testGet(BASE + "/" + id, mockMvc, session, DBProfile.class, status);
  }

  public Set<DBProfile> getDBProfiles(MockMvc mockMvc, MockHttpSession session) {
    return testGet(BASE, mockMvc, session, new TypeReference<Set<DBProfile>>(){});
  }

  public void updateDBProfile(MockMvc mockMvc, MockHttpSession session, DBProfile dbProfile, HttpStatus status) {
    testPut(BASE + "/" + dbProfile.getId(), mockMvc, session, dbProfile, status);
  }

  public void deleteDBProfile(MockMvc mockMvc, MockHttpSession session, long id, HttpStatus status) {
    testDelete(BASE + "/" + id, mockMvc, session, status);
  }
}
