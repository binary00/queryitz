package queryitz.modules.dbprofile;


import org.apache.commons.io.FileUtils;
import org.springframework.mock.web.MockMultipartFile;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.dbprofile.domain.DBProfileCreateRequest;
import queryitz.modules.user.domain.User;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static queryitz.common.TestDataUtils.getRandString;

public class DBProfileTestData {

  private static final String DOCKER_IP = "192.168.99.100";

  public static MockDBProfileCreateRequest postgresql93() throws Exception {

    MockDBProfileCreateRequest createRequest = new MockDBProfileCreateRequest();
    createRequest.setName("Test_DockerPostgresql_9.3");
    createRequest.setConnectionURL("jdbc:postgresql://" + DOCKER_IP + ":5432/tester"); // Postgresql 9.3 instance
    createRequest.setUsername("tester");
    createRequest.setPassword("testerP");
    createRequest.setDriver(new MockMultipartFile("driver", getDriverBytes("postgresql-9.3-1102-jdbc41.jar")));

    return createRequest;
  }

  public static MockDBProfileCreateRequest postgresql84() throws Exception {

    MockDBProfileCreateRequest createRequest = new MockDBProfileCreateRequest();
    createRequest.setName("Test_DockerPostgresql_8.4");
    createRequest.setConnectionURL("jdbc:postgresql://" + DOCKER_IP + ":5432/tester"); // Postgresql 8.4 instance
    createRequest.setUsername("tester");
    createRequest.setPassword("testerP");
    createRequest.setDriver(new MockMultipartFile("driver", getDriverBytes("postgresql-8.4-702.jdbc4.jar")));

    return createRequest;
  }

  public static MockDBProfileCreateRequest oracle12c() throws Exception {

    MockDBProfileCreateRequest createRequest = new MockDBProfileCreateRequest();
    createRequest.setName("Test_Oracle_12c");
    createRequest.setConnectionURL("jdbc:oracle:thin:@//" + DOCKER_IP + ":1521/orcl"); // Oracle 12c instance
    createRequest.setUsername("system");
    createRequest.setPassword("manager");
    createRequest.setDriver(new MockMultipartFile("driver", getDriverBytes("ojdbc7.jar")));

    return createRequest;
  }

  public static MockDBProfileCreateRequest oracle11g() throws Exception {

    MockDBProfileCreateRequest createRequest = new MockDBProfileCreateRequest();
    createRequest.setName("Test_Oracle_11g");
    createRequest.setConnectionURL("jdbc:oracle:thin:@//" + DOCKER_IP + ":1521/orcl"); // Oracle 12c instance
    createRequest.setUsername("system");
    createRequest.setPassword("manager");
    createRequest.setDriver(new MockMultipartFile("driver", getDriverBytes("ojdbc6.jar")));

    return createRequest;
  }

  private static byte[] getDriverBytes(String jar) throws Exception {
    File driver = new File(DBProfileTestData.class.getClassLoader().getResource(jar).toURI());
    return FileUtils.readFileToByteArray(driver);
  }

  public static MockDBProfileCreateRequest getRandomCreateRequest() throws Exception {
    MockDBProfileCreateRequest createRequest = postgresql93();
    createRequest.setName(getRandString());

    return createRequest;
  }

  public static void assertDBProfile(DBProfileCreateRequest source, DBProfile dbProfile, User owner) {
    assertNotNull(dbProfile.getId());
    assertEquals(source.getName(), dbProfile.getName());
    assertEquals(source.getConnectionURL(), dbProfile.getConnectionURL());
    assertNotNull(dbProfile.getDriverClass());
    if(owner != null) assertEquals(owner, dbProfile.getOwner());
  }

  public static void assertDBProfile(DBProfile source, DBProfile dbProfile, User owner) {
    assertEquals(source.getId(), dbProfile.getId());
    assertEquals(source.getName(), dbProfile.getName());
    assertEquals(source.getConnectionURL(), dbProfile.getConnectionURL());
    assertEquals(source.getDriverClass(), dbProfile.getDriverClass());
    assertEquals(source.getType(), dbProfile.getType());
    if(owner != null) assertEquals(owner, dbProfile.getOwner());
  }
}
