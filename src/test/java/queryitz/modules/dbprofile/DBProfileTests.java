package queryitz.modules.dbprofile;


import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.MutableAcl;
import queryitz.common.BaseTest;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.permission.service.PermissionService;
import queryitz.modules.team.TeamTestService;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.UserTestService;
import queryitz.modules.user.domain.User;

import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static queryitz.modules.dbprofile.DBProfileTestData.assertDBProfile;
import static queryitz.modules.dbprofile.DBProfileTestData.postgresql93;
import static queryitz.modules.team.TeamTestData.getTeam;
import static queryitz.modules.user.UserTestData.getUserCreateRequest;
import static queryitz.modules.user.constants.Constants.DEFAULT_USER;

public class DBProfileTests extends BaseTest {

  @Autowired private DBProfileTestService service;
  @Autowired private TeamTestService teamService;
  @Autowired private UserTestService userService;
  @Autowired private PermissionService permissionService;

  private User ADMIN_USER, TEST_USER;
  private Team TEST_TEAM;
  private DBProfile TEST_ADMIN_DBPROFILE;

  @Before
  public void setUp() throws Exception {
    setSessionUser(DEFAULT_USER);
    ADMIN_USER = userService.getUser(mockMvc, session, DEFAULT_USER);
    TEST_USER = userService.createUser(mockMvc, session, getUserCreateRequest());

    Team sourceTeam = getTeam(ADMIN_USER, TEST_USER);
    TEST_TEAM = teamService.createTeam(mockMvc, session, sourceTeam);

    MockDBProfileCreateRequest createRequest = postgresql93();
    createRequest.setName("TEST_ADMIN");
    TEST_ADMIN_DBPROFILE = service.createDBProfile(mockMvc, session, createRequest);
  }

  @Test
  public void testCreateDBProfile() throws Exception {
    setSessionUser(DEFAULT_USER);

    MockDBProfileCreateRequest createRequest = postgresql93();
    createRequest.setTeamIDs(Sets.newHashSet(TEST_TEAM.getId()));

    DBProfile dbProfile = service.createDBProfile(mockMvc, session, createRequest);
    assertDBProfile(createRequest, dbProfile, ADMIN_USER);
    assertEquals(TEST_TEAM.getId(), ((Team)dbProfile.getAccessTeams().toArray()[0]).getId());
  }

  @Test
  public void testGetDBProfile() throws Exception {
    setSessionUser(DEFAULT_USER);

    DBProfile gotDBProfile = service.getDBProfile(mockMvc, session, TEST_ADMIN_DBPROFILE.getId());
    assertDBProfile(TEST_ADMIN_DBPROFILE, gotDBProfile, ADMIN_USER);

    Set<DBProfile> dbProfiles = service.getDBProfiles(mockMvc, session);
    Optional<DBProfile> dbProfileOptional = dbProfiles.stream()
              .filter(dbProfile -> dbProfile.getId().equals(TEST_ADMIN_DBPROFILE.getId()))
              .findFirst();

    assertDBProfile(gotDBProfile, dbProfileOptional.get(), ADMIN_USER);
  }

  @Test
  public void testUpdateDBProfile() throws Exception {
    setSessionUser(DEFAULT_USER);

    MockDBProfileCreateRequest createRequest = postgresql93();
    DBProfile dbProfile = service.createDBProfile(mockMvc, session, createRequest);
    assertDBProfile(createRequest, dbProfile, ADMIN_USER);

    dbProfile.setName("New Name");
    service.updateDBProfile(mockMvc, session, dbProfile, BAD_REQUEST);
  }

  @Test
  public void testDeleteDBProfile() throws Exception {
    setSessionUser(DEFAULT_USER);

    MockDBProfileCreateRequest createRequest = postgresql93();
    DBProfile dbProfile = service.createDBProfile(mockMvc, session, createRequest);
    assertDBProfile(createRequest, dbProfile, ADMIN_USER);

    MutableAcl acl = permissionService.getPermissions(dbProfile);
    assertEquals(5, acl.getEntries().size());

    service.deleteDBProfile(mockMvc, session, dbProfile.getId(), OK);
    DBProfile deletedDBProfile = service.getDBProfile(mockMvc, session, dbProfile.getId());
    assertNull(deletedDBProfile);

    acl = permissionService.getPermissions(dbProfile);
    assertNull(acl);
  }
}
