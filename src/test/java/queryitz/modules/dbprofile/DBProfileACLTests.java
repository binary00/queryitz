package queryitz.modules.dbprofile;


import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import queryitz.common.BaseTest;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.team.TeamTestService;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.UserTestService;
import queryitz.modules.user.domain.User;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.HttpStatus.*;
import static queryitz.modules.dbprofile.DBProfileTestData.getRandomCreateRequest;
import static queryitz.modules.team.TeamTestData.getTeam;
import static queryitz.modules.user.UserTestData.getUserCreateRequest;
import static queryitz.modules.user.constants.Constants.DEFAULT_USER;

public class DBProfileACLTests extends BaseTest {

  @Autowired private DBProfileTestService service;
  @Autowired private TeamTestService teamService;
  @Autowired private UserTestService userService;

  private User ADMIN_USER, TEST_USER, TEST_USER_TEAM, TEST_USER_TEAM2, TEST_USER_USER;
  private DBProfile TEST_ADMIN_DBPROFILE, TEST_USER_DBPROFILE;
  private Team TEST_TEAM, TEST_TEAM2;

  @Before
  public void setUp() throws Exception {
    setSessionUser(DEFAULT_USER);
    ADMIN_USER = userService.getUser(mockMvc, session, DEFAULT_USER);
    TEST_USER = userService.createUser(mockMvc, session, getUserCreateRequest());

    TEST_USER_TEAM = userService.createUser(mockMvc, session, getUserCreateRequest());
    TEST_USER_TEAM2 = userService.createUser(mockMvc, session, getUserCreateRequest());
    TEST_USER_USER = userService.createUser(mockMvc, session, getUserCreateRequest());

    Team sourceTeam = getTeam(TEST_USER, TEST_USER_TEAM);
    TEST_TEAM = teamService.createTeam(mockMvc, session, sourceTeam);

    sourceTeam = getTeam(TEST_USER, TEST_USER_TEAM2);
    TEST_TEAM2 = teamService.createTeam(mockMvc, session, sourceTeam);

    TEST_ADMIN_DBPROFILE = service.createDBProfile(mockMvc, session, getRandomCreateRequest());

    setSessionUser(TEST_USER.getUsername());
    MockDBProfileCreateRequest createRequest = getRandomCreateRequest();
    createRequest.setTeamIDs(new HashSet<>(Arrays.asList(TEST_TEAM.getId())));
    createRequest.setUserIDs(new HashSet<>(Arrays.asList(TEST_USER_USER.getId())));
    TEST_USER_DBPROFILE = service.createDBProfile(mockMvc, session, createRequest);
  }

  @Test
  public void testGetDBProfileACLs() throws Exception {
    setSessionUser(DEFAULT_USER);
    service.getDBProfile(mockMvc, session, TEST_ADMIN_DBPROFILE.getId(), OK);

    Set<DBProfile> dbProfiles = service.getDBProfiles(mockMvc, session);
    assertTrue(dbProfiles.containsAll(Arrays.asList(TEST_ADMIN_DBPROFILE, TEST_USER_DBPROFILE)));

    setSessionUser(TEST_USER.getUsername());
    service.getDBProfile(mockMvc, session, TEST_ADMIN_DBPROFILE.getId(), FORBIDDEN);

    dbProfiles = service.getDBProfiles(mockMvc, session);
    assertTrue(dbProfiles.containsAll(Arrays.asList(TEST_USER_DBPROFILE)));

    setSessionUser(TEST_USER_TEAM.getUsername());
    service.getDBProfile(mockMvc, session, TEST_USER_DBPROFILE.getId(), OK);

    setSessionUser(TEST_USER_USER.getUsername());
    service.getDBProfile(mockMvc, session, TEST_USER_DBPROFILE.getId(), OK);

    setSessionUser(TEST_USER_TEAM2.getUsername());
    service.getDBProfile(mockMvc, session, TEST_USER_DBPROFILE.getId(), FORBIDDEN);
  }

  @Test
  public void testCreateDBProfileACLs() throws Exception {
    setSessionUser(DEFAULT_USER);

    DBProfile dbProfile = service.createDBProfile(mockMvc, session, getRandomCreateRequest());
    assertNotNull(dbProfile);

    setSessionUser(TEST_USER.getUsername());
    dbProfile = service.createDBProfile(mockMvc, session, getRandomCreateRequest());
    assertNotNull(dbProfile);
  }

  @Test
  public void testUpdateDBProfileACLs() throws Exception {
    setSessionUser(DEFAULT_USER);

    DBProfile dbProfile = service.createDBProfile(mockMvc, session, getRandomCreateRequest());

    service.updateDBProfile(mockMvc, session, dbProfile, BAD_REQUEST);
  }

  @Test
  public void testDeleteDBProfileACLs() throws Exception {
    setSessionUser(DEFAULT_USER);

    DBProfile dbProfile = service.createDBProfile(mockMvc, session, getRandomCreateRequest());
    service.deleteDBProfile(mockMvc, session, dbProfile.getId(), OK);

    dbProfile = service.createDBProfile(mockMvc, session, getRandomCreateRequest());

    setSessionUser(TEST_USER.getUsername());
    service.deleteDBProfile(mockMvc, session, dbProfile.getId(), FORBIDDEN);

    dbProfile = service.createDBProfile(mockMvc, session, getRandomCreateRequest());
    service.deleteDBProfile(mockMvc, session, dbProfile.getId(), OK);

    dbProfile = service.createDBProfile(mockMvc, session, getRandomCreateRequest());

    setSessionUser(TEST_USER_TEAM.getUsername());
    service.deleteDBProfile(mockMvc, session, dbProfile.getId(), FORBIDDEN);

    setSessionUser(TEST_USER_USER.getUsername());
    service.deleteDBProfile(mockMvc, session, dbProfile.getId(), FORBIDDEN);

    setSessionUser(TEST_USER_TEAM2.getUsername());
    service.deleteDBProfile(mockMvc, session, dbProfile.getId(), FORBIDDEN);

    setSessionUser(DEFAULT_USER);
    service.deleteDBProfile(mockMvc, session, dbProfile.getId(), OK);
  }
}
