package queryitz.modules.team;


import queryitz.modules.team.domain.Team;
import queryitz.modules.user.domain.User;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static queryitz.common.TestDataUtils.getRandString;

public class TeamTestData {

  public static Team getTeam(User...users) {
    Team team = new Team();
    team.setName("team_" + getRandString());
    team.setDescription("Test description " + getRandString());
    if(users != null) team.setUsers(new HashSet<>(Arrays.asList(users)));

    return team;
  }

  public static void assertTeam(Team sourceTeam, Team team) {
    assertEquals(sourceTeam.getName(), team.getName());
    assertEquals(sourceTeam.getDescription(), team.getDescription());
    assertNotNull(team.getId());
    if(sourceTeam.getUsers() != null) {
      sourceTeam.getUsers().forEach(user -> assertTrue(team.getUsers().contains(user)));
    }
  }

  public static void assertTeamUsers(Set<User> sourceUsers, Set<User> users) {
    sourceUsers.forEach(user -> assertTrue(users.contains(user)));
  }
}
