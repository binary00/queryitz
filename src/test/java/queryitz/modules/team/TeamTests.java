package queryitz.modules.team;


import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.MutableAcl;
import queryitz.common.BaseTest;
import queryitz.modules.dbprofile.DBProfileTestService;
import queryitz.modules.dbprofile.MockDBProfileCreateRequest;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.permission.domain.TeamSid;
import queryitz.modules.permission.service.PermissionService;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.UserTestService;
import queryitz.modules.user.domain.User;

import java.util.Set;

import static org.junit.Assert.*;
import static org.springframework.http.HttpStatus.*;
import static queryitz.common.TestDataUtils.assertSid;
import static queryitz.modules.dbprofile.DBProfileTestData.assertDBProfile;
import static queryitz.modules.dbprofile.DBProfileTestData.postgresql93;
import static queryitz.modules.team.TeamTestData.*;
import static queryitz.modules.user.UserTestData.getUserCreateRequest;
import static queryitz.modules.user.constants.Constants.DEFAULT_USER;

public class TeamTests extends BaseTest {

  @Autowired private TeamTestService teamService;
  @Autowired private UserTestService userService;
  @Autowired private DBProfileTestService dbProfileService;
  @Autowired private PermissionService permissionService;

  private User ADMIN_USER, TEST_USER, TEST_USER1;
  private String TEST_USERNAME;
  private Team TEST_TEAM;
  private Set<User> TEST_TEAM_USERS;

  @Before
  public void setUp() throws Exception {
    setSessionUser(DEFAULT_USER);
    ADMIN_USER = userService.getUser(mockMvc, session, DEFAULT_USER);
    TEST_USER = userService.createUser(mockMvc, session, getUserCreateRequest());
    TEST_USERNAME = TEST_USER.getUsername();
    TEST_USER1 = userService.createUser(mockMvc, session, getUserCreateRequest());

    setSessionUser(DEFAULT_USER);
    Team sourceTeam = getTeam(ADMIN_USER, TEST_USER);
    TEST_TEAM = teamService.createTeam(mockMvc, session, sourceTeam);
    TEST_TEAM_USERS = teamService.getTeamUsers(mockMvc, session, TEST_TEAM.getId());
    TEST_TEAM.setUsers(TEST_TEAM_USERS);
  }

  @Test
  public void testCreateTeam() throws Exception {
    Team sourceTeam = getTeam(ADMIN_USER, TEST_USER);
    Team team = teamService.createTeam(mockMvc, session, sourceTeam);
    Set<User> users = teamService.getTeamUsers(mockMvc, session, team.getId());
    team.setUsers(users);
    assertTeam(sourceTeam, team);
  }

  @Test
  public void testCreateTeamACLs() throws Exception {
    setSessionUser(DEFAULT_USER);
    Team sourceTeam = getTeam(ADMIN_USER, TEST_USER);
    teamService.createTeam(mockMvc, session, sourceTeam, OK);

    setSessionUser(TEST_USERNAME);
    sourceTeam = getTeam();

    Team team = teamService.createTeam(mockMvc, session, sourceTeam, FORBIDDEN);
    assertNull(team);
  }

  @Test
  public void testGetTeam() throws Exception {
    setSessionUser(DEFAULT_USER);

    Team team = teamService.getTeam(mockMvc, session, TEST_TEAM.getId());
    Set<User> users = teamService.getTeamUsers(mockMvc, session, team.getId());
    team.setUsers(users);
    assertTeam(TEST_TEAM, team);

    team = teamService.getTeamByName(mockMvc, session, TEST_TEAM.getName());
    team.setUsers(users);
    assertTeam(TEST_TEAM, team);

    Set<Team> teams = teamService.getTeams(mockMvc, session);
    assertTrue(teams.contains(TEST_TEAM));
  }

  @Test
  public void testGetTeamACLs() throws Exception {
    setSessionUser(TEST_USERNAME);

    Team team = teamService.getTeam(mockMvc, session, TEST_TEAM.getId());
    Set<User> users = teamService.getTeamUsers(mockMvc, session, team.getId());
    team.setUsers(users);
    assertTeam(TEST_TEAM, team);

    team = teamService.getTeamByName(mockMvc, session, TEST_TEAM.getName());
    team.setUsers(users);
    assertTeam(TEST_TEAM, team);

    Set<Team> teams = teamService.getTeams(mockMvc, session);
    assertTrue(teams.contains(TEST_TEAM));
  }

  @Test
  public void testGetTeamUsers() throws Exception {
    setSessionUser(DEFAULT_USER);

    Set<User> teamUsers = teamService.getTeamUsers(mockMvc, session, TEST_TEAM.getId());
    assertTeamUsers(TEST_TEAM_USERS, teamUsers);

    teamUsers = teamService.getTeamUsersByName(mockMvc, session, TEST_TEAM.getName());
    assertTeamUsers(TEST_TEAM_USERS, teamUsers);
  }

  @Test
  public void testGetTeamUsersACLs() throws Exception {
    setSessionUser(TEST_USERNAME);

    Set<User> teamUsers = teamService.getTeamUsers(mockMvc, session, TEST_TEAM.getId());
    assertTeamUsers(TEST_TEAM_USERS, teamUsers);

    teamUsers = teamService.getTeamUsersByName(mockMvc, session, TEST_TEAM.getName());
    assertTeamUsers(TEST_TEAM_USERS, teamUsers);
  }

  @Test
  public void testUpdateTeam() throws Exception {
    setSessionUser(DEFAULT_USER);
    Team sourceTeam = getTeam(ADMIN_USER, TEST_USER);
    Team team = teamService.createTeam(mockMvc, session, sourceTeam);
    Set<User> sourceTeamUsers = teamService.getTeamUsers(mockMvc, session, team.getId());
    sourceTeamUsers.add(TEST_USER1);

    sourceTeam.setName("TEAM_UPDATE");
    sourceTeam.setUsers(sourceTeamUsers);
    teamService.updateTeam(mockMvc, session, sourceTeam, BAD_REQUEST); // TODO updates don't work

    /*team = teamService.getTeam(mockMvc, session, sourceTeam.getId());
    Set<User> teamUsers = teamService.getTeamUsers(mockMvc, session, sourceTeam.getId());
    team.setUsers(teamUsers);

    assertTeam(sourceTeam, team);*/
  }

  @Test
  public void testAddUserToTeam() throws Exception {
    setSessionUser(DEFAULT_USER);

    Team localTestTeam = teamService.createTeam(mockMvc, session, getTeam(TEST_USER));
    Set<User> users = teamService.getTeamUsers(mockMvc, session, localTestTeam.getId());
    assertEquals(1, users.size());

    teamService.addUserToTeam(mockMvc, session, localTestTeam.getId(), TEST_USER1.getId());
    users = teamService.getTeamUsers(mockMvc, session, localTestTeam.getId());
    assertEquals(2, users.size());

    // TODO throw exception for duplicate users
  }

  @Test
  public void testUpdateTeamACLs() throws Exception {
    setSessionUser(TEST_USERNAME);

    Team team = teamService.getTeam(mockMvc, session, TEST_TEAM.getId());
    Set<User> teamUsers = teamService.getTeamUsers(mockMvc, session, TEST_TEAM.getId());
    teamUsers.add(TEST_USER1);

    team.setUsers(teamUsers);
    team.setName("FAIL_UPDATE");
    teamService.updateTeam(mockMvc, session, team, FORBIDDEN);

    team = teamService.getTeam(mockMvc, session, TEST_TEAM.getId());
    teamUsers = teamService.getTeamUsers(mockMvc, session, TEST_TEAM.getId());
    team.setUsers(teamUsers);

    assertTeam(TEST_TEAM, team);
  }

  @Test
  public void testDeleteTeam() throws Exception {
    setSessionUser(DEFAULT_USER);

    Team sourceTeam = getTeam(ADMIN_USER, TEST_USER);
    Team team = teamService.createTeam(mockMvc, session, sourceTeam);

    teamService.deleteTeam(mockMvc, session, team.getId(), OK);
    Team deletedTeam = teamService.getTeam(mockMvc, session, team.getId());
    Set<User> deletedTeamUsers = teamService.getTeamUsers(mockMvc, session, team.getId());

    assertNull(deletedTeam);
    assertNull(deletedTeamUsers);

    // Just want to make sure users weren't deleted as well
    User testUser = userService.getUser(mockMvc, session, TEST_USER.getId());
    assertNotNull(testUser);
  }

  @Test
  public void testDeleteTeamACLs() throws Exception {
    setSessionUser(TEST_USERNAME);

    teamService.deleteTeam(mockMvc, session, TEST_TEAM.getId(), FORBIDDEN);
  }

  @Test
  public void testDeleteTeamUser() throws Exception {
    setSessionUser(DEFAULT_USER);

    Team localTestTeam = teamService.createTeam(mockMvc, session, getTeam(TEST_USER, TEST_USER1));
    Set<User> users = teamService.getTeamUsers(mockMvc, session, localTestTeam.getId());
    assertEquals(2, users.size());

    teamService.deleteUserFromTeam(mockMvc, session, localTestTeam.getId(), TEST_USER.getId());

    users = teamService.getTeamUsers(mockMvc, session, localTestTeam.getId());
    assertEquals(1, users.size());
  }

  @Test
  public void testDeleteTeamDBProfilePermissions() throws Exception {
    setSessionUser(DEFAULT_USER);

    Team sourceTeam1 = getTeam(ADMIN_USER, TEST_USER);
    Team team1 = teamService.createTeam(mockMvc, session, sourceTeam1);

    Team sourceTeam2 = getTeam(ADMIN_USER, TEST_USER, TEST_USER1);
    Team team2 = teamService.createTeam(mockMvc, session, sourceTeam2);

    MockDBProfileCreateRequest dbProfileCreateRequest = postgresql93();
    dbProfileCreateRequest.setTeamIDs(Sets.newHashSet(team1.getId(), team2.getId()));
    DBProfile dbProfile = dbProfileService.createDBProfile(mockMvc, session, dbProfileCreateRequest);

    TeamSid sid = new TeamSid(team2);
    MutableAcl acl = permissionService.getPermissions(dbProfile);
    assertSid(sid, acl, true);

    teamService.deleteTeam(mockMvc, session, team2.getId(), OK);

    acl = permissionService.getPermissions(dbProfile);
    assertSid(sid, acl, false);
  }
}
