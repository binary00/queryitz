package queryitz.modules.team;


import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import queryitz.common.JSONTestService;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.domain.User;

import java.util.Set;

@Service
public class TeamTestService extends JSONTestService {

  private static final String BASE = "/teams";

  public Team createTeam(MockMvc mockMvc, MockHttpSession session, Team team) {
    return testPost(BASE, mockMvc, session, team, Team.class);
  }

  public Team createTeam(MockMvc mockMvc, MockHttpSession session, Team team, HttpStatus status) {
    return testPost(BASE, mockMvc, session, team, Team.class, status);
  }

  public Team getTeam(MockMvc mockMvc, MockHttpSession session, Long id) {
    return testGet(BASE + "/" + id, mockMvc, session, Team.class);
  }

  public Set<Team> getTeams(MockMvc mockMvc, MockHttpSession session) {
    return testGet(BASE, mockMvc, session, new TypeReference<Set<Team>>(){});
  }

  public Team getTeamByName(MockMvc mockMvc, MockHttpSession session, String name) {
    return testGet(BASE + "/name/" + name, mockMvc, session, Team.class);
  }

  public Set<User> getTeamUsers(MockMvc mockMvc, MockHttpSession session, Long id) {
    return testGet(BASE + "/" + id + "/users", mockMvc, session, new TypeReference<Set<User>>(){});
  }

  public Set<User> getTeamUsersByName(MockMvc mockMvc, MockHttpSession session, String name) {
    return testGet(BASE + "/name/" + name + "/users", mockMvc, session, new TypeReference<Set<User>>(){});
  }

  public void updateTeam(MockMvc mockMvc, MockHttpSession session, Team team, HttpStatus status) {
    testPut(BASE + "/" + team.getId(), mockMvc, session, team, status);
  }

  public void addUserToTeam(MockMvc mockMvc, MockHttpSession session, Long id, Long userID) {
    testPut(BASE + "/" + id + "/users/" + userID, mockMvc, session, "");
  }

  public void deleteTeam(MockMvc mockMvc, MockHttpSession session, Long id, HttpStatus status) {
    testDelete(BASE + "/" + id, mockMvc, session, status);
  }

  public void deleteUserFromTeam(MockMvc mockMvc, MockHttpSession session, Long id, Long userID) {
    testDelete(BASE + "/" + id + "/users/" + userID, mockMvc, session);
  }
}
