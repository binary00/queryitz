package queryitz.modules.dbindex;


import org.springframework.mock.web.MockHttpSession;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import queryitz.common.JSONTestService;
import queryitz.modules.dbindex.domain.result.*;

@Service
public class DBIndexTestService extends JSONTestService {

  private static final String BASE = "/dbindex";

  public DatabaseResult getDatabaseResult(MockMvc mockMvc, MockHttpSession session, long dbProfileID) {
    return testGet(BASE + "/" + dbProfileID, mockMvc, session, DatabaseResult.class);
  }

  public void reloadDatabase(MockMvc mockMvc, MockHttpSession session, long dbProfileID) {
    testDelete(BASE + "/" + dbProfileID, mockMvc, session);
  }

  public SchemasResult getSchemas(MockMvc mockMvc, MockHttpSession session, long dbProfileID) {
    return testGet(BASE + "/" + dbProfileID + "/schemas", mockMvc, session, SchemasResult.class);
  }

  public TablesResult getTables(MockMvc mockMvc, MockHttpSession session, long dbProfileID, String schema) {
    return testGet(BASE + "/" + dbProfileID + "/schemas/" + schema + "/tables", mockMvc, session, TablesResult.class);
  }

  public ColumnsResult getColumns(MockMvc mockMvc, MockHttpSession session, long dbProfileID, String schema, String table) {
    return testGet(BASE + "/" + dbProfileID + "/schemas/" + schema + "/tables/" + table + "/columns", mockMvc, session, ColumnsResult.class);
  }

  public ColumnResult getColumn(MockMvc mockMvc, MockHttpSession session, long dbProfileID, String schema, String table, String column) {
    return testGet(BASE + "/" + dbProfileID + "/schemas/" + schema + "/tables/" + table + "/columns/" + column, mockMvc, session, ColumnResult.class);
  }
}

