package queryitz.modules.dbindex;


import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import queryitz.common.BaseTest;
import queryitz.modules.dbindex.domain.result.DatabaseResult;
import queryitz.modules.dbindex.dto.IndexStateDTO;
import queryitz.modules.dbprofile.DBProfileTestService;
import queryitz.modules.dbprofile.MockDBProfileCreateRequest;
import queryitz.modules.dbprofile.domain.DBProfile;

import static org.junit.Assert.*;
import static queryitz.modules.dbprofile.DBProfileTestData.postgresql93;
import static queryitz.modules.user.constants.Constants.DEFAULT_USER;

public class DBIndexTests extends BaseTest {

  @Autowired private DBIndexTestService dbIndexService;
  @Autowired private DBIndexStateTestService dbIndexStateTestService;
  @Autowired private DBProfileTestService dbProfileTestService;

  private DBProfile TEST_ADMIN_DBPROFILE;

  @Before
  public void setUp() throws Exception {
    setSessionUser(DEFAULT_USER);

    MockDBProfileCreateRequest createRequest = postgresql93();
    createRequest.setName("TEST_ADMIN");

    TEST_ADMIN_DBPROFILE = dbProfileTestService.createDBProfile(mockMvc, session, createRequest);
  }

  @Test
  public void testDBIndex() throws Exception {

    DatabaseResult result = dbIndexService.getDatabaseResult(mockMvc, session, TEST_ADMIN_DBPROFILE.getId());
    assertNull(result);

    Thread.sleep(5000);

    IndexStateDTO indexState = dbIndexStateTestService.getIndexState(mockMvc, session, TEST_ADMIN_DBPROFILE.getId());
    assertEquals(TEST_ADMIN_DBPROFILE.getId().longValue(), indexState.getDbProfileID());
    assertFalse(indexState.isRunning());
    assertNotNull(indexState.getHistory().get(0).getStartTime());
    assertNotNull(indexState.getHistory().get(0).getStopTime());
    assertNotNull(indexState.getHistory().get(0).getDuration());
  }
}
