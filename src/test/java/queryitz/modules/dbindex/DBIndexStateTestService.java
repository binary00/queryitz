package queryitz.modules.dbindex;

import org.springframework.mock.web.MockHttpSession;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import queryitz.common.JSONTestService;
import queryitz.modules.dbindex.domain.GlobalIndexState;
import queryitz.modules.dbindex.dto.IndexStateDTO;

@Service
public class DBIndexStateTestService extends JSONTestService {

  private static final String BASE = "/dbindexstate";

  public GlobalIndexState getGlobalIndexState(MockMvc mockMvc, MockHttpSession session) {
    return testGet(BASE, mockMvc, session, GlobalIndexState.class);
  }

  public IndexStateDTO getIndexState(MockMvc mockMvc, MockHttpSession session, long dbProfileID) {
    return testGet(BASE + "/" + dbProfileID, mockMvc, session, IndexStateDTO.class);
  }
}
