package queryitz.modules.dbindex.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IndexStateDTO {

  private long dbProfileID;
  private String startTime;
  private int schemaThreadCount;
  private int tableThreadCount;
  private boolean running;
  private int dbThreadCount;
  private long userID;
  private List<HistoryDTO> history;

  public long getDbProfileID() {
    return dbProfileID;
  }

  public void setDbProfileID(long dbProfileID) {
    this.dbProfileID = dbProfileID;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public int getSchemaThreadCount() {
    return schemaThreadCount;
  }

  public void setSchemaThreadCount(int schemaThreadCount) {
    this.schemaThreadCount = schemaThreadCount;
  }

  public int getTableThreadCount() {
    return tableThreadCount;
  }

  public void setTableThreadCount(int tableThreadCount) {
    this.tableThreadCount = tableThreadCount;
  }

  public boolean isRunning() {
    return running;
  }

  public void setRunning(boolean running) {
    this.running = running;
  }

  public int getDbThreadCount() {
    return dbThreadCount;
  }

  public void setDbThreadCount(int dbThreadCount) {
    this.dbThreadCount = dbThreadCount;
  }

  public long getUserID() {
    return userID;
  }

  public void setUserID(long userID) {
    this.userID = userID;
  }

  public List<HistoryDTO> getHistory() {
    return history;
  }

  public void setHistory(List<HistoryDTO> history) {
    this.history = history;
  }
}
