package queryitz.modules.user;


import queryitz.modules.user.domain.User;
import queryitz.modules.user.domain.UserCreateRequest;
import queryitz.modules.user.enums.Role;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static queryitz.common.TestDataUtils.getRandString;

public class UserTestData {

  public static User getUser() {

    String userPass = "test_" + getRandString();

    User user = new User();
    user.setUsername(userPass);
    user.setPassword(userPass);
    user.setTempPassword(false);
    user.setEnabled(true);
    user.setEmail(userPass + "@ejecta.io");

    return user;
  }

  public static void assertUser(User sourceUser, User user) {

    assertEquals(sourceUser.getUsername(), user.getUsername());
    assertNotNull(user.getPassword());
    assertEquals(sourceUser.isTempPassword(), user.isTempPassword());
    assertEquals(sourceUser.isEnabled(), user.isEnabled());
    assertEquals(sourceUser.getEmail(), user.getEmail());

    assertNotNull(user.getId());
  }

  public static UserCreateRequest getUserCreateRequest() {

    UserCreateRequest createRequest = new UserCreateRequest();
    createRequest.setUser(getUser());
    createRequest.setRole(Role.ROLE_USER);

    return createRequest;
  }
}
