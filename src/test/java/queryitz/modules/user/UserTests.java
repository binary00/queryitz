package queryitz.modules.user;


import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.crypto.password.PasswordEncoder;
import queryitz.common.BaseTest;
import queryitz.modules.dbprofile.DBProfileTestService;
import queryitz.modules.dbprofile.MockDBProfileCreateRequest;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.permission.domain.UserSid;
import queryitz.modules.permission.service.PermissionService;
import queryitz.modules.user.domain.PasswordChangeRequest;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.domain.UserCreateRequest;

import static org.junit.Assert.*;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static queryitz.common.TestDataUtils.assertSid;
import static queryitz.modules.dbprofile.DBProfileTestData.assertDBProfile;
import static queryitz.modules.dbprofile.DBProfileTestData.postgresql93;
import static queryitz.modules.user.UserTestData.assertUser;
import static queryitz.modules.user.UserTestData.getUserCreateRequest;
import static queryitz.modules.user.constants.Constants.DEFAULT_USER;

public class UserTests extends BaseTest {

  @Autowired private UserTestService usersService;
  @Autowired private DBProfileTestService dbProfileService;
  @Autowired private PermissionService permissionService;
  @Autowired private PasswordEncoder passwordEncoder;

  private User ADMIN_USER;

  @Before
  public void setUp() throws Exception {
    setSessionUser(DEFAULT_USER);
    ADMIN_USER = usersService.getUser(mockMvc, session, DEFAULT_USER);
  }

  @Test
  public void testCreateUser() throws Exception {
    setSessionUser(DEFAULT_USER);

    UserCreateRequest createRequest = getUserCreateRequest();
    User user = usersService.createUser(mockMvc, session, createRequest);
    assertUser(createRequest.getUser(), user);

    // Only Admin role users can create users
    setSessionUser(user.getUsername());

    createRequest = getUserCreateRequest();
    user = usersService.createUser(mockMvc, session, createRequest, FORBIDDEN);
    assertNull(user);
  }

  @Test
  public void testUpdateUser() throws Exception {
    setSessionUser(DEFAULT_USER);

    UserCreateRequest createRequest = getUserCreateRequest();
    User user = usersService.createUser(mockMvc, session, createRequest);

    user.setUsername("test_update");
    usersService.updateUser(mockMvc, session, user);

    User updatedUser = usersService.getUser(mockMvc, session, user.getId());
    assertEquals(user.getUsername(), updatedUser.getUsername());

    // Only admin and users can update their account
    setSessionUser(updatedUser.getUsername());

    user.setUsername("bob");
    usersService.updateUser(mockMvc, session, user, FORBIDDEN);
  }

  @Test
  public void testDeleteUser() throws Exception {
    setSessionUser(DEFAULT_USER);

    UserCreateRequest createRequest = getUserCreateRequest();
    User user = usersService.createUser(mockMvc, session, createRequest);
    setSessionUser(user.getUsername());

    MockDBProfileCreateRequest dbProfileCreateRequest = postgresql93();
    DBProfile dbProfile = dbProfileService.createDBProfile(mockMvc, session, dbProfileCreateRequest);
    assertDBProfile(dbProfileCreateRequest, dbProfile, user);

    setSessionUser(DEFAULT_USER);
    usersService.deleteUser(mockMvc, session, user.getId());
    User deletedUser = usersService.getUser(mockMvc, session, user.getId());
    assertNull(deletedUser);

    DBProfile deletedDBProfile = dbProfileService.getDBProfile(mockMvc, session, dbProfile.getId());
    assertNull(deletedDBProfile);
  }

  @Test
  public void testDeleteUserDBProfilePermissions() throws Exception {
    setSessionUser(DEFAULT_USER);

    UserCreateRequest createRequest = getUserCreateRequest();
    User user = usersService.createUser(mockMvc, session, createRequest);

    MockDBProfileCreateRequest dbProfileCreateRequest = postgresql93();
    dbProfileCreateRequest.setUserIDs(Sets.newHashSet(user.getId()));

    DBProfile dbProfile = dbProfileService.createDBProfile(mockMvc, session, dbProfileCreateRequest);
    assertDBProfile(dbProfileCreateRequest, dbProfile, ADMIN_USER);
    assertEquals(user.getId(), ((User)dbProfile.getAccessUsers().toArray()[0]).getId());

    UserSid sid = new UserSid(user);
    MutableAcl acl = permissionService.getPermissions(dbProfile);
    assertSid(sid, acl, true);

    usersService.deleteUser(mockMvc, session, user.getId());
    User deletedUser = usersService.getUser(mockMvc, session, user.getId());
    assertNull(deletedUser);

    DBProfile deletedDBProfile = dbProfileService.getDBProfile(mockMvc, session, dbProfile.getId());
    assertNotNull(deletedDBProfile);

    acl = permissionService.getPermissions(dbProfile);
    assertSid(sid, acl, false);
  }

  @Test
  public void testUpdatePassword() throws Exception {
    setSessionUser(DEFAULT_USER);

    UserCreateRequest createRequest = getUserCreateRequest();
    String originalPassword = createRequest.getUser().getPassword();
    User user = usersService.createUser(mockMvc, session, createRequest);

    setSessionUser(user.getUsername());

    String newPassword = "test123NewPassword";
    PasswordChangeRequest changeRequest = new PasswordChangeRequest();
    changeRequest.setUsername(user.getUsername());
    changeRequest.setPassword(originalPassword);
    changeRequest.setNewPassword(newPassword);

    usersService.changePassword(mockMvc, session, user, changeRequest);

    user = usersService.getUser(mockMvc, session, user.getId());
    assertTrue(passwordEncoder.matches(newPassword, user.getPassword()));
  }

  @Test
  public void testUnauthorizedUpdatePassword() throws Exception {
    setSessionUser(DEFAULT_USER);

    UserCreateRequest createRequest1 = getUserCreateRequest();
    User user1 = usersService.createUser(mockMvc, session, createRequest1);

    UserCreateRequest createRequest2 = getUserCreateRequest();
    String originalPassword2 = createRequest2.getUser().getPassword();
    User user2 = usersService.createUser(mockMvc, session, createRequest2);

    setSessionUser(user1.getUsername());

    String newPassword = "test123NewPassword";
    PasswordChangeRequest changeRequest = new PasswordChangeRequest();
    changeRequest.setUsername(user2.getUsername());
    changeRequest.setPassword(originalPassword2);
    changeRequest.setNewPassword(newPassword);

    usersService.changePassword(mockMvc, session, user2, changeRequest, FORBIDDEN);
    assertFalse(passwordEncoder.matches(newPassword, user2.getPassword()));
  }

  @Test
  public void testAdminUpdatePassword() throws Exception {
    setSessionUser(DEFAULT_USER);

    String newPassword = "test123NewPassword";

    PasswordChangeRequest changeRequest = new PasswordChangeRequest();
    changeRequest.setUsername(DEFAULT_USER);
    changeRequest.setPassword("admin");
    changeRequest.setNewPassword(newPassword);

    usersService.changePassword(mockMvc, session, ADMIN_USER, changeRequest);

    User user = usersService.getUser(mockMvc, session, ADMIN_USER.getId());
    assertTrue(passwordEncoder.matches(newPassword, user.getPassword()));
  }
}
