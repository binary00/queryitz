package queryitz.modules.user;


import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import queryitz.common.JSONTestService;
import queryitz.modules.user.domain.PasswordChangeRequest;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.domain.UserCreateRequest;

@Service
public class UserTestService extends JSONTestService {

  private static final String BASE = "/users";

  public User createUser(MockMvc mockMvc, MockHttpSession session, UserCreateRequest request) {
    return testPost(BASE, mockMvc, session, request, User.class);
  }

  public User createUser(MockMvc mockMvc, MockHttpSession session, UserCreateRequest request, HttpStatus status) {
    return testPost(BASE, mockMvc, session, request, User.class, status);
  }

  public User getUser(MockMvc mockMvc, MockHttpSession session, Long id) {
    return testGet(BASE + "/" + id, mockMvc, session, User.class);
  }

  public User getUser(MockMvc mockMvc, MockHttpSession session, String username) {
    return testGet(BASE + "/username/" + username, mockMvc, session, User.class);
  }

  public void updateUser(MockMvc mockMvc, MockHttpSession session, User user) {
    testPut(BASE + "/" + user.getId(), mockMvc, session, user);
  }

  public void updateUser(MockMvc mockMvc, MockHttpSession session, User user, HttpStatus status) {
    testPut(BASE + "/" + user.getId(), mockMvc, session, user, status);
  }

  public void changePassword(MockMvc mockMvc, MockHttpSession session, User user, PasswordChangeRequest changeRequest) {
    testPut(BASE + "/" + user.getId() + "/password", mockMvc, session, changeRequest);
  }

  public void changePassword(MockMvc mockMvc, MockHttpSession session, User user, PasswordChangeRequest changeRequest, HttpStatus status) {
    testPut(BASE + "/" + user.getId() + "/password", mockMvc, session, changeRequest, status);
  }

  public void deleteUser(MockMvc mockMvc, MockHttpSession session, Long id) {
    testDelete(BASE + "/" + id, mockMvc, session);
  }
}
