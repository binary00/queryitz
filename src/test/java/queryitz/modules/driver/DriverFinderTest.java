package queryitz.modules.driver;


import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class DriverFinderTest {

  @Test
  public void testPostgresql() throws Exception {

    // 8.4 (Released May 2010)
    assertEquals("org.postgresql.Driver", getDriverClassName("postgresql-8.4-702.jdbc4.jar"));

    // 9.3 (Released November 2013)
    assertEquals("org.postgresql.Driver", getDriverClassName("postgresql-9.3-1102-jdbc41.jar"));

  }

  @Test
  public void testMysql() throws Exception {

    // 5.1.1 (Released June 2007)
    assertEquals("com.mysql.jdbc.Driver", getDriverClassName("mysql-connector-java-5.1.1.jar"));

    // 5.1.31 (Released May 2014)
    assertEquals("com.mysql.jdbc.Driver", getDriverClassName("mysql-connector-java-5.1.31.jar"));

  }

  @Test
  public void testOracle() throws Exception {

    // 11g 11.2.0.4
    assertEquals("oracle.jdbc.OracleDriver", getDriverClassName("ojdbc6.jar"));

    // 12c 12.1.0.2
    assertEquals("oracle.jdbc.OracleDriver", getDriverClassName("ojdbc7.jar"));

  }

  @Test
  public void testDB2() throws Exception {

    // 9.7.0.1
    assertEquals("com.ibm.db2.jcc.DB2Driver", getDriverClassName("db2jcc4-9.7.0.1.jar"));

    // 10.1.0.4
    assertEquals("com.ibm.db2.jcc.DB2Driver", getDriverClassName("db2jcc4-10.1.0.4.jar"));

    // 10.5.0.4
    assertEquals("com.ibm.db2.jcc.DB2Driver", getDriverClassName("db2jcc4-10.5.0.4.jar"));

  }

  private String getDriverClassName(String driverJar) throws Exception {
    File driver = new File(this.getClass().getClassLoader().getResource(driverJar).toURI());
    byte[] driverBytes = FileUtils.readFileToByteArray(driver);

    DriverFinder finder = new DriverFinder(driverBytes);
    return finder.getDriverClassName();
  }
}
