package queryitz.modules.query;


import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import queryitz.common.BaseTest;
import queryitz.modules.dbprofile.DBProfileTestService;
import queryitz.modules.dbprofile.MockDBProfileCreateRequest;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.query.domain.Query;
import queryitz.modules.user.constants.Constants;

import java.io.File;

public class QueryTest extends BaseTest {

  @Autowired private DBProfileTestService dbProfileService;
  @Autowired private QueryTestService queryService;

  @Test
  @Ignore
  public void testBasicQuery() throws Exception {
    setSessionUser(Constants.DEFAULT_USER);

    DBProfile dbProfile = dbProfileService.createDBProfile(mockMvc, session, getCreateRequest());

    Query query = new Query();
    query.setDbProfileID(dbProfile.getId());
    query.setQuery("CREATE TABLE USERB (\n" +
        "      ID BIGINT,\n" +
        "      USERNAME VARCHAR(255) NOT NULL,\n" +
        "      PASSWORD VARCHAR(255),\n" +
        "      ENABLED BOOLEAN NOT NULL,\n" +
        "      PRIMARY KEY(ID),\n" +
        "      UNIQUE (USERNAME)\n" +
        "    );");

    Query queryResult = queryService.doQuery(mockMvc, session, query);
    System.out.println(queryResult);

   /* query.setQuery("INSERT INTO USERB VALUES(1, 'blargh', 'pword', true);");
    queryResult = queryService.doQuery(mockMvc, session, query);
    System.out.println(queryResult);

    query.setQuery("SELECT * FROM USERB;");
    queryResult = queryService.doQuery(mockMvc, session, query);
    System.out.println(queryResult);*/
  }

  @Test
  @Ignore
  public void testBasicQuery3() throws Exception {

    setSessionUser(Constants.DEFAULT_USER);

    DBProfile dbProfile = dbProfileService.createDBProfile(mockMvc, session, getCreateRequest3());
    Query query = new Query();
    query.setDbProfileID(dbProfile.getId());
    query.setQuery("select * from all_users"); // No semi-colon..oracle doesn't like it???? Think its multi line queries only.

    long startTime = System.currentTimeMillis();
    Query queryResult = queryService.doQuery(mockMvc, session, query);
    System.out.println("Time : " + (System.currentTimeMillis() - startTime) + "ms " +  queryResult);

    startTime = System.currentTimeMillis();
    queryResult = queryService.doQuery(mockMvc, session, query);
    System.out.println("Time2 : " + (System.currentTimeMillis() - startTime) + "ms " +  queryResult);
  }

  public MockDBProfileCreateRequest getCreateRequest() {
    MockDBProfileCreateRequest createRequest = new MockDBProfileCreateRequest();

    createRequest.setName("Tester");
    //createRequest.setConnectionURL("jdbc:postgresql://192.168.59.103:5432/tester?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory");
    createRequest.setConnectionURL("jdbc:postgresql://192.168.59.103:5432/tester");
    createRequest.setUsername("tester");
    createRequest.setPassword("testerP");

    byte[] driverBytes = new byte[0];
    try {
      File driver = new File(this.getClass().getClassLoader().getResource("postgresql-9.3-1102-jdbc41.jar").toURI());
      driverBytes = FileUtils.readFileToByteArray(driver);
    } catch(Exception e) {
      e.printStackTrace();
    }

    createRequest.setDriver(new MockMultipartFile("driver", driverBytes));

    return createRequest;
  }

  public MockDBProfileCreateRequest getCreateRequest2() {
    MockDBProfileCreateRequest createRequest = new MockDBProfileCreateRequest();

    createRequest.setName("Tester2");
    //createRequest.setConnectionURL("jdbc:postgresql://192.168.59.103:5432/tester?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory");
    createRequest.setConnectionURL("jdbc:postgresql://192.168.59.103:5432/tester");
    createRequest.setUsername("tester");
    createRequest.setPassword("testerP");

    byte[] driverBytes = new byte[0];
    try {
      File driver = new File(this.getClass().getClassLoader().getResource("postgresql-8.4-702.jdbc4.jar").toURI());
      driverBytes = FileUtils.readFileToByteArray(driver);
    } catch(Exception e) {
      e.printStackTrace();
    }

    createRequest.setDriver(new MockMultipartFile("driver", driverBytes));

    return createRequest;
  }

  public MockDBProfileCreateRequest getCreateRequest3() {
    MockDBProfileCreateRequest createRequest = new MockDBProfileCreateRequest();

    createRequest.setName("Oracle");
    createRequest.setConnectionURL("jdbc:oracle:thin:@//192.168.59.103:1521/orcl");
    createRequest.setUsername("system");
    createRequest.setPassword("manager");

    byte[] driverBytes = new byte[0];
    try {
      File driver = new File(this.getClass().getClassLoader().getResource("ojdbc7.jar").toURI());
      driverBytes = FileUtils.readFileToByteArray(driver);
    } catch(Exception e) {
      e.printStackTrace();
    }

    createRequest.setDriver(new MockMultipartFile("driver", driverBytes));

    return createRequest;
  }
}
