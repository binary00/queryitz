package queryitz.modules.query;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.sockjs.client.RestTemplateXhrTransport;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;
import queryitz.Application;
import queryitz.modules.auth.domain.LoginRequest;
import queryitz.modules.query.client.SSLStandardWebSocketClient;
import queryitz.modules.query.client.StompMessageHandler;
import queryitz.modules.query.client.StompSession;
import queryitz.modules.query.client.WebSocketStompClient;
import queryitz.modules.query.domain.Query;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.apache.http.conn.ssl.SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
import static org.junit.Assert.fail;
import static org.springframework.http.HttpStatus.OK;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@Ignore
public class QueryWebSocketTest {

  static {
    System.setProperty("textdb.allow_full_path", "true");
  }

  @Value("${local.server.port}") int port;

  @Test
  public void testBlah() throws Exception {

    String sessionCookie = authenticate();

    URI uri = new URI("wss://localhost:" + port + "/query");

    WebSocketHttpHeaders headers = new WebSocketHttpHeaders();
    headers.add("Cookie", sessionCookie);

    List<Transport> transports = new ArrayList<>();
    transports.add(new WebSocketTransport(new SSLStandardWebSocketClient(sslContextBuilder().build())));
    RestTemplateXhrTransport xhrTransport = new RestTemplateXhrTransport(getRestTemplate());
    xhrTransport.setRequestHeaders(headers);
    transports.add(xhrTransport);

    SockJsClient sockJsClient = new SockJsClient(transports);

    WebSocketStompClient stompClient = new WebSocketStompClient(uri, headers, sockJsClient);
    stompClient.setMessageConverter(new MappingJackson2MessageConverter());
    final CountDownLatch latch = new CountDownLatch(1);

    stompClient.connect(new StompMessageHandler() {

      private StompSession stompSession;

      @Override
      public void afterConnected(StompSession stompSession, StompHeaderAccessor headers) {
        stompSession.subscribe("/user/topic/query/message", null);
        this.stompSession = stompSession;

        Query query = new Query();
        query.setDbProfileID(1);
        query.setQuery("SELECT * FROM Persons;");

        this.stompSession.send("/ws/query", query);
      }

      @Override
      public void handleMessage(Message<byte[]> message) {
        try {
          StompHeaderAccessor headers = StompHeaderAccessor.wrap(message);
          String json = new String((byte[]) message.getPayload(), Charset.forName("UTF-8"));
          System.out.println(json);
        } catch(Throwable t) {

        } finally {
          this.stompSession.disconnect();
          latch.countDown();
        }
      }

      @Override
      public void handleError(Message<byte[]> message) {
        System.out.println("Error: " + new String(message.getPayload(), Charset.forName("UTF-8")));
      }

      @Override
      public void handleReceipt(String receiptId) {}

      @Override
      public void afterDisconnected() {}
    });

    if(!latch.await(10, TimeUnit.SECONDS)) {
      fail("Message not received");
    }

  }

  private String authenticate() throws Exception {

    LoginRequest loginRequest = new LoginRequest("admin", "admin");

    ResponseEntity<String> entity  = getRestTemplate().postForEntity(getURL() + "/auth/login", loginRequest, String.class);

    if(entity.getStatusCode().equals(OK)) {
      List<String> setCookieEntries = entity.getHeaders().get("Set-Cookie");
      return setCookieEntries.get(0);
    }

    return null;
  }

  private TestRestTemplate getRestTemplate() throws Exception {
    SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContextBuilder().build(), ALLOW_ALL_HOSTNAME_VERIFIER);

    HttpClient httpClient = HttpClients
        .custom()
        .setSSLSocketFactory(socketFactory)
        .build();

    TestRestTemplate testRestTemplate = new TestRestTemplate();
    ((HttpComponentsClientHttpRequestFactory) testRestTemplate.getRequestFactory()).setHttpClient(httpClient);

    return testRestTemplate;
  }

  private SSLContextBuilder sslContextBuilder() throws Exception {
    return new SSLContextBuilder()
        .loadTrustMaterial(null, new TrustSelfSignedStrategy());
  }

  private String getURL() {
    return "https://localhost:" + this.port;
  }
}
