package queryitz.modules.query;

import org.springframework.mock.web.MockHttpSession;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import queryitz.common.JSONTestService;
import queryitz.modules.query.domain.Query;

@Service
public class QueryTestService extends JSONTestService {

  private static final String BASE = "/query";

  public Query doQuery(MockMvc mockMvc, MockHttpSession session, Query query) {

    try {
      return testPost(BASE, mockMvc, session, query, Query.class);
    } catch(Exception e) {
      e.printStackTrace();
    }

    return null;
  }
}
