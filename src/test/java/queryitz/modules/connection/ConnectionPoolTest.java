package queryitz.modules.connection;


import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import queryitz.common.BaseTest;
import queryitz.modules.dbprofile.DBProfileTestService;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.query.QueryTestService;
import queryitz.modules.query.domain.Query;

import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static queryitz.common.TestDataUtils.getRandString;
import static queryitz.modules.dbprofile.DBProfileTestData.postgresql93;
import static queryitz.modules.user.constants.Constants.DEFAULT_USER;

public class ConnectionPoolTest extends BaseTest {

  @Autowired private DBProfileTestService dbProfileTestService;
  @Autowired private QueryTestService queryService;
  @Autowired private ConnectionPoolManager connectionPoolManager;

  private DBProfile dbProfile;

  @Before
  public void setUp() throws Exception {
    setSessionUser(DEFAULT_USER);
    dbProfile = dbProfileTestService.createDBProfile(mockMvc, session, postgresql93());

    Query query = new Query();
    query.setDbProfileID(dbProfile.getId());
    query.setQuery("CREATE TABLE CONNECTIONPOOLINGTEST (\n" +
        "      ID BIGINT,\n" +
        "      USERNAME VARCHAR(255) NOT NULL,\n" +
        "      PASSWORD VARCHAR(255),\n" +
        "      ENABLED BOOLEAN NOT NULL,\n" +
        "      PRIMARY KEY(ID),\n" +
        "      UNIQUE (USERNAME)\n" +
        "    );");

    queryService.doQuery(mockMvc, session, query);

    IntStream.range(1, 1000)
        .parallel()
        .forEach(i -> {
          Query insertQuery = new Query();
          insertQuery.setDbProfileID(dbProfile.getId());
          insertQuery.setQuery("INSERT INTO CONNECTIONPOOLINGTEST VALUES(" + i + ", '" + getRandString() + "', 'pword', true);");
          queryService.doQuery(mockMvc, session, insertQuery);
        });
  }

  /**
   * This tests 2 things. Pools are actually working and that with reusing
   * drivers and using connection pools that our queries come back accurate
   * by threading.
   *
   * @throws Exception
   */
  @Test
  public void testConnectionPooling() throws Exception {

    setSessionUser(DEFAULT_USER);
    ConnectionPool connectionPool = connectionPoolManager.getPool(dbProfile);

    IntStream.range(1, 1000)
        .parallel()
        .forEach(i -> {
          Query query = new Query(dbProfile.getId(), "SELECT * FROM CONNECTIONPOOLINGTEST where ID = " + i + ";");

          Query queryResult = queryService.doQuery(mockMvc, session, query);
          assertEquals(i, queryResult.getResultData().getRowResults().get(0).getColumns().get(0).getValue());
        });

    assertTrue(connectionPool.getMaxTotal() > 2);
  }
}
