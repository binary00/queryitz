package queryitz.modules.connection;


import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import queryitz.common.BaseTest;
import queryitz.modules.dbprofile.DBProfileTestService;
import queryitz.modules.dbprofile.domain.DBProfile;

import java.sql.Connection;
import java.sql.DatabaseMetaData;

import static org.junit.Assert.assertEquals;
import static queryitz.modules.dbprofile.DBProfileTestData.*;
import static queryitz.modules.user.constants.Constants.DEFAULT_USER;

public class ConnectionDriverVersionTest extends BaseTest {

  @Autowired private DBProfileTestService dbProfileTestService;
  @Autowired private ConnectionPoolManager connectionPoolManager;

  @Test
  public void testPostgresql() throws Exception {
    setSessionUser(DEFAULT_USER);
    DBProfile dbProfPostgresql93 = dbProfileTestService.createDBProfile(mockMvc, session, postgresql93());

    setSessionUser(DEFAULT_USER);
    ConnectionPool connectionPool = connectionPoolManager.getPool(dbProfPostgresql93);
    Connection connection = connectionPool.borrowObject();
    DatabaseMetaData databaseMetaData = connection.getMetaData();
    assertEquals("PostgreSQL 9.3 JDBC4.1 (build 1102)", databaseMetaData.getDriverVersion());
    connectionPool.returnObject(connection);

    DBProfile dbProfPostgresql84 = dbProfileTestService.createDBProfile(mockMvc, session, postgresql84());

    setSessionUser(DEFAULT_USER);
    connectionPool = connectionPoolManager.getPool(dbProfPostgresql84);
    connection = connectionPool.borrowObject();
    databaseMetaData = connection.getMetaData();
    assertEquals("PostgreSQL 8.4 JDBC4 (build 702)", databaseMetaData.getDriverVersion());
    connectionPool.returnObject(connection);
  }

  @Test
  @Ignore("Need to ignore until we find a better way with docker")
  public void testOracle() throws Exception {
    setSessionUser(DEFAULT_USER);
    DBProfile dbProfOracle12c = dbProfileTestService.createDBProfile(mockMvc, session, oracle12c());

    setSessionUser(DEFAULT_USER);
    ConnectionPool connectionPool = connectionPoolManager.getPool(dbProfOracle12c);
    Connection connection = connectionPool.borrowObject();
    DatabaseMetaData databaseMetaData = connection.getMetaData();
    assertEquals("12.1.0.2.0", databaseMetaData.getDriverVersion());

    DBProfile dbProfOracle11g = dbProfileTestService.createDBProfile(mockMvc, session, oracle11g());

    setSessionUser(DEFAULT_USER);
    connectionPool = connectionPoolManager.getPool(dbProfOracle11g);
    connection = connectionPool.borrowObject();
    databaseMetaData = connection.getMetaData();
    assertEquals("11.2.0.4.0", databaseMetaData.getDriverVersion());
    connectionPool.returnObject(connection);
  }
}
