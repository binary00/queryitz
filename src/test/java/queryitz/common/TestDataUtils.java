package queryitz.common;


import org.springframework.security.acls.model.AccessControlEntry;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.Sid;

import java.util.Random;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestDataUtils {

  private static final Random rand = new Random(System.currentTimeMillis());

  public static String getRandString() {
    return Long.toUnsignedString(rand.nextLong(), 36);
  }

  public static void assertSid(Sid sid, MutableAcl acl, boolean contains) {
    boolean containsSid = false;

    if(acl.getEntries() != null) {
      for(AccessControlEntry accessControlEntry : acl.getEntries()) {
        if(accessControlEntry.getSid().equals(sid)) {
          containsSid = true;
          break;
        }
      }
    }

    if(contains) {
      assertTrue(containsSid);
    } else {
      assertFalse(containsSid);
    }
  }
}
