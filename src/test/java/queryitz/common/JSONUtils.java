package queryitz.common;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JSONUtils {

  public String getJSON(Object obj) {

    if(obj != null) {
      try {
        return getMapper().writeValueAsString(obj);
      } catch(IOException e) {
        e.printStackTrace();
      }
    }

    return null;
  }

  public <T> T mapObject(String json, Class<T> clazz) {

    if(!json.isEmpty() && clazz != null) {
      try {
        return getMapper().readValue(json, clazz);
      } catch(IOException e) {
        e.printStackTrace();
      }
    }

    return null;
  }

  public <T> T mapObject(String json, TypeReference<T> typeReference) {

    if(!json.isEmpty() && typeReference != null) {
      try {
        return getMapper().readValue(json, typeReference);
      } catch(IOException e) {
        e.printStackTrace();
      }
    }

    return null;
  }

  private ObjectMapper getMapper() {
    return new ObjectMapper();
  }
}
