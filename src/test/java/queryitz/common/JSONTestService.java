package queryitz.common;

import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Component
public class JSONTestService {

  public static final MediaType JSON_CHARSET = new MediaType("application", "json", Charset.forName("UTF-8"));

  @Autowired
  protected JSONUtils jsonUtils;

  public <T> T testPost(String endpoint, MockMvc mockMvc, MockHttpSession session, Object object, Class<T> clazz) {
    return testPost(endpoint, mockMvc, session, object, clazz, OK);
  }

  public <T> T testPost(String endpoint, MockMvc mockMvc, MockHttpSession session, Object object, Class<T> clazz, HttpStatus status) {

    String content = null;
    try {
      content = mockMvc
          .perform(post(endpoint)
              .secure(true)
              .session(session)
              .content(jsonUtils.getJSON(object))
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().is(status.value()))
          .andReturn()
          .getResponse()
          .getContentAsString();
    } catch(Exception e) {
      e.printStackTrace();
    }

    return jsonUtils.mapObject(content, clazz);
  }

  public <T> T testGet(String endpoint, MockMvc mockMvc, MockHttpSession session, Class<T> clazz, HttpStatus status)  {
    return jsonUtils.mapObject(performGet(endpoint, mockMvc, session, status), clazz);
  }

  public <T> T testGet(String endpoint, MockMvc mockMvc, MockHttpSession session, Class<T> clazz)  {
    return jsonUtils.mapObject(performGet(endpoint, mockMvc, session, OK), clazz);
  }

  public <T> T testGet(String endpoint, MockMvc mockMvc, MockHttpSession session, TypeReference<T> typeReference)  {
    return jsonUtils.mapObject(performGet(endpoint, mockMvc, session, OK), typeReference);
  }

  private String performGet(String endpoint, MockMvc mockMvc, MockHttpSession session, HttpStatus status) {
    String content = null;
    try {
      content = mockMvc
          .perform(get(endpoint)
              .secure(true)
              .session(session))
          .andExpect(status().is(status.value()))
          .andReturn()
          .getResponse()
          .getContentAsString();
    } catch(Exception e) {
      e.printStackTrace();
    }

    return content;
  }

  public void testPut(String endpoint, MockMvc mockMvc, MockHttpSession session, Object object) {
    testPut(endpoint, mockMvc, session, object, OK);
  }

  public void testPut(String endpoint, MockMvc mockMvc, MockHttpSession session, Object object, HttpStatus status) {

    try {
      mockMvc
          .perform(put(endpoint)
              .secure(true)
              .session(session)
              .content(jsonUtils.getJSON(object))
              .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().is(status.value()));
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  public void testDelete(String endpoint, MockMvc mockMvc, MockHttpSession session) {
    testDelete(endpoint, mockMvc, session, OK);
  }

  public void testDelete(String endpoint, MockMvc mockMvc, MockHttpSession session, HttpStatus status) {

    try {
      mockMvc
          .perform(delete(endpoint)
              .secure(true)
              .session(session))
          .andExpect(status().is(status.value()));
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
}
