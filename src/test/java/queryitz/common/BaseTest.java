package queryitz.common;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import queryitz.Application;

import javax.annotation.PostConstruct;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public abstract class BaseTest {

  static {
    System.setProperty("textdb.allow_full_path", "true");
  }

  @Autowired protected WebApplicationContext wac;
  @Autowired protected MockHttpSession session;
  @Autowired protected MockHttpServletRequest request;
  @Autowired protected UserDetailsService userDetailsService;
  @Autowired protected FilterChainProxy springSecurityFilterChain;

  protected MockMvc mockMvc;

  @PostConstruct
  public void setup() {
    if(mockMvc == null) {
      this.mockMvc = webAppContextSetup(this.wac)
          .addFilters(this.springSecurityFilterChain)
          .build();
    }
  }

  protected void setSessionUser(String userName) {
    UsernamePasswordAuthenticationToken principal = getPrincipal(userName);
    SecurityContextHolder.getContext().setAuthentication(principal);
    session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, new MockSecurityContext(principal));
  }

  protected SecurityContext getSessionUser() {
    Object obj = null;

    try {
      obj = session.getAttribute(SPRING_SECURITY_CONTEXT_KEY);
    } catch (Exception e) {}

    return (obj != null) ? (SecurityContext) obj : null;
  }

  protected String getSessionUserName() {
    SecurityContext context = getSessionUser();
    return (context != null && context.getAuthentication() != null) ? context.getAuthentication().getName() : null;
  }

  protected UsernamePasswordAuthenticationToken getPrincipal(String username) {

    UserDetails user = this.userDetailsService.loadUserByUsername(username);

    if(user != null) {
      UsernamePasswordAuthenticationToken authentication =
          new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());

      return authentication;
    }

    return null;
  }

  public static class MockSecurityContext implements SecurityContext {

    private static final long serialVersionUID = -1386535243513362694L;

    private Authentication authentication;

    public MockSecurityContext(Authentication authentication) {
      this.authentication = authentication;
    }

    @Override
    public Authentication getAuthentication() {
      return this.authentication;
    }

    @Override
    public void setAuthentication(Authentication authentication) {
      this.authentication = authentication;
    }
  }
}
