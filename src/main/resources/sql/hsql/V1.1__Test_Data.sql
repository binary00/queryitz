INSERT INTO users VALUES (1, 'admin', '$2a$10$d67gTPto2T0tbpkqoQEQMe5qxM/b3OZQ9r.RkQXsmOm5qIN0Bzz3a', 'admin@haha.com', true, false);
INSERT INTO user_role VALUES (1, 'ROLE_ADMIN', 1);
INSERT INTO user_attr VALUES (1, 'GRAVATAR_HASH', 'f1c19b7362b86b6af5e71e6a41e54a46');

INSERT INTO acl_sid VALUES (1, true, 'admin');
INSERT INTO acl_class VALUES (1, 'queryitz.modules.dbprofile.domain.DBProfile');

INSERT INTO db_profile VALUES (1, 'Docker Postgresql 9.1', 'org.postgresql.Driver', 'jdbc:postgresql://192.168.99.100:5432/tester', 'POSTGRESQL', 1, 1);
INSERT INTO driver VALUES (1, LOAD_FILE('src/main/resources/ci/postgresql93/postgresql-9.3-1102.resourcejar'));
INSERT INTO db_profile_creds VALUES (1, 1, 'tester', '94561bd095dd0d1d9386e1b6cd2d5b8abdc0141f641a2e6d9485ae8e20cbb884f930e90b03db62', '1234');
INSERT INTO acl_object_identity VALUES (1, 1, 1, NULL, 1, true);
INSERT INTO acl_entry VALUES (1, 1, 0, 1, 16, true, false, false);
INSERT INTO acl_entry VALUES (2, 1, 1, 1, 1, true, false, false);
INSERT INTO saved_session values (1, 1, LOAD_FILE('src/main/resources/ci/postgresql93/init.sql', 'UTF-8'));

INSERT INTO db_profile VALUES (2, 'Oracle 12c', 'oracle.jdbc.OracleDriver', 'jdbc:oracle:thin:@//192.168.59.103:1521/orcl', 'ORACLE', 2, 1);
INSERT INTO driver VALUES (2, LOAD_FILE('src/main/resources/ci/oracle12c/ojdbc7.resourcejar'));
INSERT INTO db_profile_creds VALUES (2, 1, 'system', '06c5435f363e80dfaf2cd046c5fe7ae4673d182f4c255c5e6bbea997b1a073b6610e94d45c5db7', '1234');
INSERT INTO acl_object_identity VALUES (2, 1, 2, NULL, 1, true);
INSERT INTO acl_entry VALUES (3, 2, 0, 1, 16, true, false, false);
INSERT INTO acl_entry VALUES (4, 2, 1, 1, 1, true, false, false);
INSERT INTO saved_session values (2, 1, LOAD_FILE('src/main/resources/ci/oracle12c/init.sql', 'UTF-8'));

INSERT INTO users VALUES (2, 'test', '$2a$10$yQGHSh2Y.V7M7l0Faa.g6uc0gUOVOar8x0OdbXEWOIfvjfu9gGbXu', 'test@ejecta.io', true, false);
INSERT INTO user_role VALUES (2, 'ROLE_USER', 2);
INSERT INTO acl_sid VALUES (2, true, 'test');
INSERT INTO db_profile_creds VALUES (1, 2, 'tester', '94561bd095dd0d1d9386e1b6cd2d5b8abdc0141f641a2e6d9485ae8e20cbb884f930e90b03db62', '1234');
INSERT INTO acl_entry VALUES (5, 1, 2, 2, 16, true, false, false);
INSERT INTO acl_entry VALUES (6, 1, 3, 2, 1, true, false, false);
INSERT INTO saved_session values (1, 2, LOAD_FILE('src/main/resources/ci/postgresql93/init.sql', 'UTF-8'));

INSERT INTO users VALUES (3, 'test2', '$2a$10$yQGHSh2Y.V7M7l0Faa.g6uc0gUOVOar8x0OdbXEWOIfvjfu9gGbXu', 'test2@ejecta.io', true, false);
INSERT INTO user_role VALUES (3, 'ROLE_USER', 3);

INSERT INTO team VALUES (1, 'Test team for test users.', 'Test Team 1');
INSERT INTO team_users VALUES (1, 2);
INSERT INTO team_users VALUES (1, 3);