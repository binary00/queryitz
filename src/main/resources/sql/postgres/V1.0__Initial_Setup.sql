create table acl_sid(
  id bigserial not null primary key,
  principal boolean not null,
  sid varchar(100) not null,constraint unique_uk_1 unique(sid, principal)
);

create table acl_class(
  id bigserial not null primary key,
  class varchar(100) not null,
  constraint unique_uk_2 unique(class)
);

create table acl_object_identity(
  id bigserial primary key,
  object_id_class bigint not null,
  object_id_identity bigint not null,
  parent_object bigint,
  owner_sid bigint,
  entries_inheriting boolean not null,
  constraint unique_uk_3 unique(object_id_class,object_id_identity),
  constraint foreign_fk_1 foreign key(parent_object) references acl_object_identity(id),
  constraint foreign_fk_2 foreign key(object_id_class) references acl_class(id),
  constraint foreign_fk_3 foreign key(owner_sid) references acl_sid(id)
);

create table acl_entry(
  id bigserial primary key,
  acl_object_identity bigint not null,
  ace_order int not null,
  sid bigint not null,
  mask integer not null,
  granting boolean not null,
  audit_success boolean not null,
  audit_failure boolean not null,
  constraint unique_uk_4 unique(acl_object_identity,ace_order),
  constraint foreign_fk_4 foreign key(acl_object_identity) references acl_object_identity(id),
  constraint foreign_fk_5 foreign key(sid) references acl_sid(id)
);

CREATE TABLE db_profile (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(255) NOT NULL,
  driver_class VARCHAR(255) NOT NULL,
  connection_url VARCHAR(255) NOT NULL,
  type VARCHAR(255) NOT NULL,
  driver_id BIGINT NULL,
  owner_id BIGINT
);

CREATE TABLE driver (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  bytes OID NOT NULL
);

CREATE TABLE db_profile_creds (
  db_profile_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  username VARCHAR(255),
  password VARCHAR(255),
  salt VARCHAR(255),
  PRIMARY KEY (db_profile_id, user_id)
);

CREATE TABLE saved_session(
  db_profile_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  queries TEXT,
  PRIMARY KEY (db_profile_id, user_id)
);

CREATE TABLE saved_result (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  query TEXT,
  result TEXT,
  db_profile_id BIGINT,
  user_id BIGINT,
  FOREIGN KEY (db_profile_id, user_id) REFERENCES saved_session (db_profile_id, user_id)
);

CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  email VARCHAR(254) NOT NULL,
  enabled BOOLEAN NOT NULL,
  temp_password BOOLEAN NOT NULL,
  CONSTRAINT USERS_EMAIL UNIQUE (email),
  CONSTRAINT USERS_USERNAME UNIQUE (username)
);

CREATE TABLE user_attr (
  id BIGSERIAL NOT NULL,
  name VARCHAR(255) NOT NULL,
  value VARCHAR(255),
  PRIMARY KEY (id, name),
  FOREIGN KEY (id) REFERENCES users (id)
);

CREATE TABLE user_role (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  role VARCHAR(50) NOT NULL,
  user_id BIGINT,
  FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE team (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  description VARCHAR(255),
  name VARCHAR(255) NOT NULL,
  CONSTRAINT TEAM_NAME UNIQUE (name)
);

CREATE TABLE team_users (
  team_id BIGINT NOT NULL,
  users_id BIGINT NOT NULL,
  FOREIGN KEY (team_id) REFERENCES team (id),
  FOREIGN KEY (users_id) REFERENCES users (id)
);

CREATE TABLE dbprofile_teams (
  dbprofile_id BIGINT NOT NULL,
  team_id BIGINT NOT NULL,
  FOREIGN KEY (team_id) REFERENCES team (id),
  FOREIGN KEY (dbprofile_id) REFERENCES db_profile (id)
);

CREATE TABLE dbprofile_users (
  dbprofile_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (dbprofile_id) REFERENCES db_profile (id)
);