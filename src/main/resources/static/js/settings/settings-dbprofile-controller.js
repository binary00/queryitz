queryitzApp.controller('SettingsDBProfileCtrl', ['$scope', 'dbProfileService',  function($scope, dbProfileService) {

  if($scope.authStatus && $scope.authStatus.authenticated) {
    dbProfileService.getCurrentUsersProfiles().success(function (data) {
      $scope.dbProfiles = data;
    });
  }

  $scope.setSelected = function(dbProfile) {
    angular.forEach($scope.dbProfiles, function(iterProfile) {
      if(dbProfile.id == iterProfile.id) {
        $scope.selectedDBProfile = iterProfile;
        iterProfile.active = true;
      } else {
        iterProfile.active = false;
      }
    });
  };

  $scope.deleteDBProfile = function(dbProfile) {
    angular.forEach($scope.dbProfiles, function(iterProfile, index) {
      if(iterProfile.id == dbProfile.id) {

        dbProfileService.delete(dbProfile).success(function(){
          $scope.dbProfiles.splice(index, 1);
          $scope.selectedDBProfile = null;
        });

        return;
      }
    });
  };

  $scope.initCreateDBProfile = function() {
    if($scope.showCreateDBProfile) {
      $scope.showCreateDBProfile = false;
    } else {
      $scope.showCreateDBProfile = true;
    }
  };

  $scope.createDBProfile = function(dbProfileCreateRequest) {
    dbProfileService.create(dbProfileCreateRequest).success(function(data){
      $scope.dbProfiles.push(data);
      $scope.dbProfileCreateRequest = {};
      $('input#driver').val(null);
      $scope.showCreateDBProfile = false;
    });
  };
}]);
