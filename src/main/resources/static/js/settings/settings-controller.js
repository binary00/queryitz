queryitzApp.controller('SettingsCtrl', ['$scope', '$routeParams',  function($scope, $routeParams) {

  var settingsPath = 'partials/settings/';

  $scope.settingsModules = [
    {
      key: 'dbprofiles',
      templateURL: settingsPath + 'dbprofiles.html',
      linkURL: '#/settings/dbprofiles',
      linkText: 'DB Profiles',
      active: true
    },
    {
      key: 'teams',
      templateURL: settingsPath + 'teams.html',
      linkURL: '#/settings/teams',
      linkText: 'Teams',
      active: false
    },
    {
      key: 'users',
      templateURL: settingsPath + 'users.html',
      linkURL: '#/settings/users',
      linkText: 'Users',
      active: false
    }
  ];

  angular.forEach($scope.settingsModules, function(module){
    if(module.key == $routeParams.setting) {
      module.active = true;
      $scope.selectedModule = module;
      return;
    } else {
      module.active = false;
    }
  });

}]);
