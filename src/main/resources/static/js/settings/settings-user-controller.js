queryitzApp.controller('SettingsUserCtrl', ['$scope', 'userService',  function($scope, userService) {

  initData();

  if($scope.authStatus && $scope.authStatus.authenticated) {
    userService.getAllUsers().success(function(users) {
      $scope.users = users;
    });

    userService.getRoles().success(function(roles) {
      $scope.roles = roles;
    });
  }

  $scope.setSelected = function(user) {
    angular.forEach($scope.users, function(iterUser) {
      if(user.id == iterUser.id) {
        $scope.selectedUser = iterUser;
        iterUser.active = true;
      } else {
        iterUser.active = false;
      }
    });
  };

  $scope.initCreateUser = function() {
    if($scope.showCreateUser) {
      $scope.showCreateUser = false;
    } else {
      $scope.showCreateUser = true;
    }
  };

  $scope.createUser = function(userCreateRequest) {
    userService.createUser(userCreateRequest).success(function(user) {
      $scope.users.push(user);
      $scope.userCreateRequest = null;
      $scope.showCreateUser = false;
    });
  };

  $scope.deleteUser = function(user) {
    userService.delete(user.id).success(function() {
      angular.forEach($scope.users, function(iterUser, index) {
        if(user.id == iterUser.id) {
          $scope.selectedUser = null;
          $scope.users.splice(index, 1);
          return;
        }
      });
    });
  };

  $scope.changePassword = function(user, passwordChangeRequest) {
    passwordChangeRequest.username = user.username;
    userService.updatePassword(user.id, passwordChangeRequest).success(function() {
      $scope.showPasswordChangedAlert = true;
      setTimeout(function(){
        $scope.showPasswordChange = false;
        $scope.showPasswordChangedAlert = false;
        $scope.$apply();
      }, 3000);
    }).error(function() {
      $scope.showPasswordChangedFailedAlert = true;
      setTimeout(function(){
        $scope.showPasswordChangedFailedAlert = false;
        $scope.$apply();
      }, 3000);
    });
  };

  function initData() {
    $scope.showPasswordChange = false;
    $scope.showPasswordChangedAlert = false;
    $scope.showPasswordChangedFailedAlert = false;
  }

}]);
