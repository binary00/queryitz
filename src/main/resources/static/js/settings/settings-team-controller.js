queryitzApp.controller('SettingsTeamCtrl', ['$scope', 'teamService', 'userService',  function($scope, teamService, userService) {

  if($scope.authStatus && $scope.authStatus.authenticated) {
    teamService.getTeams().success(function (data) {
      $scope.teams = data;
    });
  }

  $scope.setSelected = function(team) {
    $scope.showUsers = false;
    $scope.teamUsers = [];

    angular.forEach($scope.teams, function(iterTeam) {
      if(team.id == iterTeam.id) {
        $scope.selectedTeam = iterTeam;
        iterTeam.active = true;
      } else {
        iterTeam.active = false;
      }
    });
  };

  $scope.showTeamUsers = function(team) {
    if($scope.showUsers) {
      $scope.showUsers = false;
      $scope.teamUsers = [];
    } else {
      teamService.getTeamUsers(team.id).success(function(data) {
        $scope.showUsers = true;
        $scope.teamUsers = data;
      });
    }
  };

  $scope.deleteTeamUser = function(team, user) {
    teamService.deleteUserFromTeam(team.id, user.id).success(function() {
      angular.forEach($scope.teamUsers, function(iterUser, index) {
        if(user.id == iterUser.id) $scope.teamUsers.splice(index, 1);
      });
    });
  };

  $scope.initCreateTeam = function() {
    if($scope.showCreateTeam) {
      $scope.showCreateTeam = false;
    } else {
      $scope.showCreateTeam = true;
    }
  };

  $scope.createTeam = function(team) {
    teamService.createTeam(team).success(function(data) {
      $scope.teams.push(data);
      $scope.teamCreateRequest = {};
      $scope.showCreateTeam = false;
    });
  };

  $scope.deleteTeam = function(team) {
    teamService.deleteTeam(team.id).success(function() {
      angular.forEach($scope.teams, function(iterTeam, index) {
        if(team.id == iterTeam.id) {
          $scope.teams.splice(index, 1);
          $scope.selectedTeam = null;
        }
      });
    });
  };

  $scope.addUser = function(userName, team) {
    userService.getUserByUsername(userName).success(function(user) {
      if(user) {
        teamService.addUserToTEam(team.id, user.id).success(function() {
          $scope.teamUsers.push(user);
          $scope.userSelected = null;
        });
      }
    });
  };
}]);
