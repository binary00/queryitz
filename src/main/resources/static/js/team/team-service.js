queryitzApp.factory('teamService', ['$http', function($http) {

  var baseURL = '/teams';
  var teamService = {};

  teamService.createTeam = function(team) {
    return $http.post(baseURL, team);
  };

  teamService.getTeams = function() {
    return $http.get(baseURL);
  };

  teamService.getTeamUsers = function(teamID) {
    return $http.get(baseURL + "/" + teamID + "/users");
  };

  teamService.addUserToTEam = function(teamID, userID) {
    return $http.put(baseURL + "/" + teamID + "/users/" + userID);
  };

  teamService.deleteTeam = function(teamID) {
    return $http.delete(baseURL + "/" + teamID);
  };

  teamService.deleteUserFromTeam = function(teamID, userID) {
    return $http.delete(baseURL + "/" + teamID + "/users/" + userID);
  };

  return teamService;
}]);