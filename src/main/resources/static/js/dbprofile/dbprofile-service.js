queryitzApp.factory('dbProfileService', ['$http', function($http) {

  var baseURL = '/dbprofiles';
  var currentDBID = -1;
  var dbProfileService = {};

  dbProfileService.getCurrentUsersProfiles = function() {
    return $http.get(baseURL);
  };

  dbProfileService.create = function(request) {
    var fd = new FormData();
    angular.forEach(request, function(value, key) {
      fd.append(key, value);
    });

    return $http.post(baseURL, fd, {
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined}
    });
  };

  dbProfileService.delete = function(dbProfile) {
    return $http.delete(baseURL + '/' + dbProfile.id);
  };

  dbProfileService.getCurrentDBID = function() {
    if(currentDBID < 0) currentDBID = localStorage.currentDBID;
    return currentDBID;
  };

  dbProfileService.setCurrentDBID = function(newCurrentDBID) {
    localStorage.currentDBID = newCurrentDBID;
    currentDBID = newCurrentDBID;
  };

  return dbProfileService;
}]);