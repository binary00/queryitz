queryitzApp.controller('DBProfileCtrl', ['$scope', 'dbProfileService', function($scope, dbProfileService) {

  $scope.$watch('authStatus.authenticated', function() {
    initDBProfiles();
  });

  $scope.changeDBProfile = function(dbID){
    dbProfileService.setCurrentDBID(dbID);
    angular.forEach($scope.dbProfiles, function(dbProfile) {
      var active = (dbProfile.id == dbID);
      dbProfile.active = active ? 'active' : null;
      if(active) $scope.$parent.$broadcast("activeDBProfile", dbProfile);
    });
  };

  $scope.initCreateDBProfile = function() {
    if($scope.showCreateDBProfile) {
      $scope.showCreateDBProfile = false;
    } else {
      $scope.showCreateDBProfile = true;
    }
  };

  $scope.createDBProfile = function(dbProfileCreateRequest) {
    dbProfileService.create(dbProfileCreateRequest).success(function(data){
      $scope.dbProfiles.push(data);
      $scope.dbProfileCreateRequest = {};
      $('input#driver').val(null);
      $scope.showCreateDBProfile = false;
    });
  };

  function initDBProfiles() {
    $scope.showCreateDBProfile = false;
    $scope.dbProfileCreateRequest = {};
    $scope.dbProfiles = [];
    dbProfileService.setCurrentDBID(-1);

    if($scope.authStatus && $scope.authStatus.authenticated) {
      dbProfileService.getCurrentUsersProfiles().success(function (data) {
        var dbProfiles = data;
        $scope.dbProfiles = dbProfiles;
        var dbID = dbProfileService.getCurrentDBID();

        if(dbID < 0 && dbProfiles && dbProfiles.length > 0) {
          dbID = dbProfiles[0].id;
        }

        $scope.changeDBProfile(dbID);
      });
    }
  }

}]);
