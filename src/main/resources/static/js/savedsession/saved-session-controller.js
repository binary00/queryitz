queryitzApp.controller('SavedSessionCtrl', ['$scope', 'savedSessionService', 'dbProfileService', function($scope, savedSessionService, dbProfileService) {

  $scope.$watch('authStatus.authenticated', function() {
    initData();
  });

  var timer;
  $scope.$watch('sessionQueries', function() {
    if(timer) clearTimeout(timer);
    timer = setTimeout(saveSessionQuery, 1000);
  });

  $scope.$watch(function () { return dbProfileService.getCurrentDBID(); }, function(dbID) {
    getSavedSessionQuery(dbID);
  });

  function saveSessionQuery() {
    var queries = $scope.sessionQueries;
    var dbID = dbProfileService.getCurrentDBID();

    if(queries && dbID) {
      savedSessionService.save(dbID, queries);
    }
  }

  function getSavedSessionQuery(dbID) {
    if(dbID && dbID > -1) {
      savedSessionService.get(dbID).success(function(data){
        $scope.sessionQueries = data.queries;
      });
    }
  }

  function initData() {
    $scope.sessionQueries = null;
  }

}]);
