queryitzApp.factory('savedSessionService', ['$http', function($http) {

  var baseURL = '/savedsessions';
  var savedSessionService = {};

  savedSessionService.get = function(dbProfileID) {
    return $http.get(baseURL + "/dbprofile/" + dbProfileID)
  };

  savedSessionService.save = function(dbProfileID, sessionQueries, savedSessionRequest) {

    if(savedSessionRequest == null) {
      savedSessionRequest = {"dbProfileID" : dbProfileID, "queries" : sessionQueries};
    }

    return $http.post(baseURL, savedSessionRequest);
  };

  return savedSessionService;
}]);