queryitzApp.controller('HeaderCtrl', ['$scope', function($scope) {

  $scope.headerLinks = initLinks();

  $scope.$on('authMessage', function() {
    $scope.headerLinks = initLinks();
  });

  $scope.setActive = function(key) {
    angular.forEach($scope.headerLinks,  function(result, index) {
      if(key == index) {
        $scope.headerLinks[index].active = true;
      } else {
        $scope.headerLinks[index].active = false;
      }
    });
  };

  function initLinks() {

    var url = window.location.hash;

    return [
      { href: "#/", text: "Query", active: url == '#/' },
      { href: "#/settings", text: "Settings", active: url.match('^#/settings/?.?') }
    ];
  }
}]);
