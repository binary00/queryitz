queryitzApp.controller('QueryCtrl', ['$scope', 'queryService', 'dbProfileService', function($scope, queryService, dbProfileService) {

  initData();

  Mousetrap.bind(['command+enter', 'ctrl+enter'], function() {
    doQuery();
    return false;
  });

  $scope.$on('authMessage', function() {
    initData();
  });

  $scope.$on('queryMessage', function(event, data) {
    processQueryMessage(data);
  });

  $scope.cancelQuery = function(query) {
    queryService.asyncQueryCancel(query);
  };

  $scope.selected = function(query) {
    angular.forEach($scope.results, function(result) {
      result.active = false;
    });
    query.active = true;
  };

  $scope.closeTab = function(query) {
    angular.forEach($scope.results, function(result, key) {
      if(result.queryID == query.queryID) {
        $scope.results.splice(key, 1);
        return;
      }
    });

    if($scope.results.slice(-1)[0]){
      $scope.results.slice(-1)[0].active = true;
    }

    if(query.status == 'RUNNING') {
      queryService.asyncQueryCancel(query);
    }
  };

  function doQuery() {
    var queryString = window.getSelection().toString();
    var dbID = dbProfileService.getCurrentDBID();

    if (queryString && dbID) {
      queryService.asyncQuery(dbID, queryString, null);
    }
  }

  function processQueryMessage(queryMessage) {

    switch(queryMessage.status) {

      case "RUNNING":
        angular.forEach($scope.results, function(result) {
          result.active = false;
        });

        queryMessage.statusClass = 'label-primary';
        queryMessage.active = true;
        $scope.results.push(queryMessage);

        $scope.$apply();
        break;

      case "FINISHED":
        angular.forEach($scope.results, function(result) {
          result.active = false;

          if(result.queryID == queryMessage.queryID) {
            result.status = queryMessage.status;
            result.error = queryMessage.error;
            result.errorString = queryMessage.errorString;
            result.rowResults = queryMessage.rowResults;
            result.columnHeaders = queryMessage.columnHeaders;
            result.elapsedTime = queryMessage.elapsedTime;
            result.resultData = queryMessage.resultData;
            result.statusClass = 'label-success';
            result.active = true;
          }
        });

        $scope.$apply();
        break;

      case "CANCELLED":
        angular.forEach($scope.results, function(result) {
          result.active = false;

          if(result.queryID == queryMessage.queryID && result.status != 'FINISHED') {
            result.status = queryMessage.status;
            result.statusClass = 'label-warning';
            result.active = true;
          }
        });

        $scope.$apply();
        break;

      case "EXCEPTION":
        angular.forEach($scope.results, function(result) {
          result.active = false;

          if(result.queryID == queryMessage.queryID && result.status != 'FINISHED') {
            result.status = queryMessage.status;
            result.error = queryMessage.error;
            result.errorString = queryMessage.errorString;
            result.statusClass = 'label-danger';
            result.active = true;
          }
        });

        $scope.$apply();
        break;
    }
  }

  function initData() {
    $scope.results = [];
  }
}]);