queryitzApp.factory('queryService', ['$rootScope', '$http', 'authService', function($rootScope, $http, authService) {

  var baseURL = '/query';
  var queryService = {};
  var stomp;

  stompConnect();

  $rootScope.$on('authMessage', function(event, data) {
    switch (data) {
      case 'login':
        stompConnect();
        break;
      case 'logout':
        stompDisconnect();
        break;
      default:
        break;
    }
  });

  function stompConnect() {
    authService.status().success(function(data) {
      if(data.authenticated) {
        var socket = new SockJS('/query');
        stomp = Stomp.over(socket);
        stomp.debug = {}; // Turned off debugging
        stomp.connect({}, stompSubscribe, stompFailureCallback);
      }
    });
  }

  function stompDisconnect() {
    if(stomp) stomp.disconnect();
  }

  function stompFailureCallback(error) {
    console.log('STOMP: ' + error);
    setTimeout(stompConnect, 5000);
    console.log('STOMP: Reconecting in 5 seconds');
  }

  function stompSubscribe() {
    stomp.subscribe('/user/topic/query/message', function(queryMsg) {
      var queryResult = JSON.parse(queryMsg.body);
      $rootScope.$broadcast("queryMessage", queryResult);
    });
  }

  queryService.query = function(dbProfileID, queryString, query) {

    if(!query) {
      query = {"dbProfileID" : dbProfileID, "query" : queryString};
    }

    return $http.post(baseURL, query);
  };

  queryService.asyncQuery = function(dbProfileID, queryString, query) {
    if(!query) {
      query = {"dbProfileID" : dbProfileID, "query" : queryString};
    }

    stomp.send("/ws/query", {}, JSON.stringify(query));
  };

  queryService.asyncQueryCancel = function(query) {
    stomp.send("/ws/query/cancel", {}, JSON.stringify(query));
  };

  return queryService;
}]);