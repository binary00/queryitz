queryitzApp.controller('AuthCtrl', ['$scope', 'authService', 'userService', function($scope, authService, userService) {

  authSetup();

  $scope.$on('authMessage', function(event, data) {
    authSetup();

    if(data == 'login-failure') {
      $scope.showLoginFailedAlert = true;
      setTimeout(function() {
        $scope.showLoginFailedAlert = false;
        $scope.$apply();
      }, 3000)
    }
  });

  $scope.login = function() {
    authService.login($scope.loginRequest);
  };

  $scope.logout = function() {
    authService.logout();
  };

  function authSetup() {
    $scope.showLoginFailedAlert = false;
    $scope.loginRequest = {};

    authService.status().success(function(data) {
      $scope.authStatus = data;

      if($scope.authStatus.authenticated) {
        userService.get(data.userID).success(function (data) {
          $scope.authedUser = data;
        });
      } else {
        $scope.authedUser = {};
      }
    });
  }
}]);
