queryitzApp.factory('authService', ['$rootScope', '$http', function($rootScope, $http) {

  var baseURL = '/auth';
  var authService = {};

  authService.status = function() {
    return $http.get(baseURL + '/status');
  };

  authService.login = function(loginRequest) {
    $http.post(baseURL + '/login', loginRequest).success(function(data) {
      $rootScope.$broadcast("authMessage", "login");
    }).error(function() {
      $rootScope.$broadcast("authMessage", "login-failure");
    });
  };

  authService.logout = function() {
    return $http.delete(baseURL + '/logout').success(function(data) {
      $rootScope.$broadcast("authMessage", "logout");
    });
  };

  return authService;
}]);
