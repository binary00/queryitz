queryitzApp.factory('userService', ['$http', function($http) {

  var baseURL = '/users';
  var userService = {};

  userService.createUser = function(userCreateRequest) {
    return $http.post(baseURL, userCreateRequest);
  };

  userService.get = function(id) {
    return $http.get(baseURL + "/" + id);
  };

  userService.getUserByUsername = function(username) {
    return $http.get(baseURL + "/username/" + username);
  };

  userService.getAllUsers = function() {
    return $http.get(baseURL);
  };

  userService.getRoles = function() {
    return $http.get(baseURL + "/roles");
  };

  userService.delete = function(id) {
    return $http.delete(baseURL + "/" + id);
  };

  userService.updatePassword = function(id, passwordChangeRequest) {
    return $http.put(baseURL + "/" + id + "/password", passwordChangeRequest);
  };

  return userService;
}]);