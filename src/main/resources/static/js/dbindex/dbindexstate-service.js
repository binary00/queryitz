queryitzApp.factory('dbIndexStateService', ['$rootScope', '$http', 'authService', function($rootScope, $http, authService) {

  var baseURL = '/dbindexstate';
  var dbIndexStateService = {};
  var stomp;

  stompConnect();

  $rootScope.$on('authMessage', function(event, data) {
    switch (data) {
      case 'login':
        stompConnect();
        break;
      case 'logout':
        stompDisconnect();
        break;
      default:
        break;
    }
  });

  function stompConnect() {
    authService.status().success(function(data) {
      if(data.authenticated) {
        var socket = new SockJS('/dbindexstatemsg');
        stomp = Stomp.over(socket);
        stomp.debug = {}; // Turned off debugging
        stomp.connect({}, stompSubscribe, stompFailureCallback);
      }
    });
  }

  function stompDisconnect() {
    if(stomp) stomp.disconnect();
  }

  function stompFailureCallback(error) {
    console.log('STOMP: ' + error);
    setTimeout(stompConnect, 5000);
    console.log('STOMP: Reconecting in 5 seconds');
  }

  function stompSubscribe() {
    stomp.subscribe('/user/topic/dbindexstatemsg/message', function(indexStateMsg) {
      var indexState = JSON.parse(indexStateMsg.body);
      $rootScope.$broadcast("indexStateMessage", indexState);
    });
  }

  dbIndexStateService.getGlobalState = function() {
    return $http.get(baseURL);
  };

  dbIndexStateService.getState = function(dbProfileID) {
    return $http.get(baseURL + "/" + dbProfileID);
  };

  return dbIndexStateService;
}]);