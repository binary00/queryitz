queryitzApp.controller('QueryDBIndexCtrl', ['$scope', 'dbIndexService', 'dbIndexStateService', function($scope, dbIndexService, dbIndexStateService) {

  var dbProfileID;
  $scope.dbLoading = false;
  $scope.fieldTypes = ['nullable', 'size', 'typeName'];

  $scope.$on('activeDBProfile', function(event, dbProfile) {
    dbIndexStateService.getState(dbProfile.id).success(function(indexState) {
      setLoadingIcon(indexState);
    });

    $scope.dbNavName = dbProfile.name;
    dbProfileID = dbProfile.id;

    initTreeStructure();
  });

  $scope.$on('indexStateMessage', function(event, indexState) {
    if(indexState.dbProfileID == dbProfileID) {
      setLoadingIcon(indexState);
      initTreeStructure();
    }
  });

  function setLoadingIcon(indexState) {
    if(indexState.running) {
      $scope.dbLoading = true;
    } else {
      $scope.dbLoading = false;
    }
  }

  function initTreeStructure() {
    dbIndexService.getSchemas(dbProfileID)
      .success(function(data){
        $scope.schemas = [];

        angular.forEach(data.schemas, function(schema) {
          $scope.schemas.push({name: schema});
        });
      });
  }

  $scope.refreshDBIndex = function() {
    dbIndexStateService.getState(dbProfileID).success(function(indexState) {
      if(!indexState.running) dbIndexService.reloadDBIndex(dbProfileID);
    });
  };

  $scope.getTables = function(selectedSchema) {
    if(selectedSchema.show) {
      selectedSchema.show = false;
    } else {
      selectedSchema.show = true;

      dbIndexService.getTables(dbProfileID, selectedSchema.name)
        .success(function (data) {
          var tables = [];

          angular.forEach(data.tables, function (table) {
            tables.push({name: table});
          });

          angular.forEach($scope.schemas, function (schema) {
            if(schema.name == selectedSchema.name) {
              schema.tables = tables;
              return;
            }
          });
        });
    }
  };

  $scope.getColumns = function(selectedSchema, selectedTable) {
    if(selectedTable.show) {
      selectedTable.show = false;
    } else {
      selectedTable.show = true;

      dbIndexService.getColumns(dbProfileID, selectedSchema.name, selectedTable.name)
        .success(function(data) {
          var columns = [];

          angular.forEach(data.columns, function(column) {
            columns.push({name: column});
          });

          angular.forEach($scope.schemas, function(schema){
            if(schema.name == selectedSchema.name) {
              angular.forEach(schema.tables, function(table){
                if(table.name == selectedTable.name) {
                  table.columns = columns;
                  return;
                }
              });
            }
          });
        });
    }
  };

  $scope.getColumn = function(selectedSchema, selectedTable, selectedColumn) {
    if(selectedColumn.show) {
      selectedColumn.show = false;
    } else {
      selectedColumn.show = true;

      dbIndexService.getColumn(dbProfileID, selectedSchema.name, selectedTable.name, selectedColumn.name)
        .success(function (data) {

          angular.forEach($scope.schemas, function (schema) {
            if(schema.name == selectedSchema.name) {
              angular.forEach(schema.tables, function (table) {
                if(table.name == selectedTable.name) {

                  angular.forEach(table.columns, function (column) {
                    if (column.name == selectedColumn.name) {
                      column.data = data.column;
                      return;
                    }
                  });
                }
              });
            }
          });
        });
    }
  }

}]);