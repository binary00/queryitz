queryitzApp.factory('dbIndexService', ['$http', function($http) {

  var baseURL = '/dbindex';
  var dbIndexService = {};

  dbIndexService.reloadDBIndex = function(dbProfileID) {
    return $http.delete(baseURL + "/" + dbProfileID);
  };

  dbIndexService.getSchemas = function(dbProfileID) {
    return $http.get(baseURL + "/" + dbProfileID + '/schemas');
  };

  dbIndexService.getTables = function(dbProfileID, schema) {
    return $http.get(baseURL + "/" + dbProfileID + '/schemas/' + schema + "/tables");
  };

  dbIndexService.getColumns = function(dbProfileID, schema, table) {
    return $http.get(baseURL + "/" + dbProfileID + '/schemas/' + schema + "/tables/" + table + "/columns");
  };

  dbIndexService.getColumn = function(dbProfileID, schema, table, column) {
    return $http.get(baseURL + "/" + dbProfileID + '/schemas/' + schema + "/tables/" + table + "/columns/" + column);
  };

  return dbIndexService;
}]);