/**
 * No clue what I'm doing with structuring angular js. Trial by error I guess.
 */
var queryitzApp = angular.module('app', ['ngRoute', 'cust.directives']);
queryitzApp.config(['$routeProvider', function($routeProvider){

  $routeProvider.
    when("/", {
      templateUrl: "partials/query.html"
    }).
    when("/settings/:setting?",{
      templateUrl: "partials/settings.html"
    }).
    otherwise({
      redirectTo: "/"
    });
}]);