CREATE TABLE ORACLE_Persons (
  PersonID number(5),
  LastName varchar2(255),
  FirstName varchar2(255),
  Address varchar2(255),
  City varchar2(255)
);
INSERT INTO ORACLE_Persons VALUES (1, 'Oracle', 'Dude', '123 Test St', 'Town');
SELECT * FROM ORACLE_Persons;