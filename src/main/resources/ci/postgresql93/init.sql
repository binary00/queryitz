CREATE TABLE POSTGRE_Persons (
  PersonID int,
  LastName varchar(255),
  FirstName varchar(255),
  Address varchar(255),
  City varchar(255)
);
INSERT INTO POSTGRE_Persons VALUES (1, 'Dude', 'Test', '123 Test St', 'Town');
SELECT * FROM POSTGRE_Persons;