/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.config;

import net.sf.ehcache.CacheManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.acls.domain.*;
import org.springframework.security.acls.jdbc.BasicLookupStrategy;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.acls.jdbc.LookupStrategy;
import org.springframework.security.acls.model.AclCache;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.security.acls.model.PermissionGrantingStrategy;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import queryitz.modules.permission.CustomPermissionEvaluator;
import queryitz.modules.user.enums.Role;

import javax.sql.DataSource;
import java.util.Arrays;


@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityAclConfig extends GlobalMethodSecurityConfiguration {

  @Value("${spring.datasource.platform}")
  private String platform;

  @Bean
  public MutableAclService aclService(DataSource dataSource, LookupStrategy lookupStrategy, AclCache aclCache, PermissionEvaluator permissionEvaluator) {

    JdbcMutableAclService mutableAclService = new JdbcMutableAclService(dataSource, lookupStrategy, aclCache);

    // POSTGRE needs sequence queries set, HSQLDB works out of the box so nothing is needed
    if("postgresql".equals(platform)) {
      mutableAclService.setClassIdentityQuery("select currval(pg_get_serial_sequence('acl_class', 'id'))");
      mutableAclService.setSidIdentityQuery("select currval(pg_get_serial_sequence('acl_sid', 'id'))");
    }

    setPermissionEvaluator(Arrays.asList(permissionEvaluator));

    return mutableAclService;
  }

  @Bean @Lazy @Scope(proxyMode = ScopedProxyMode.INTERFACES)
  public PermissionEvaluator permissionEvaluator() {
    return new CustomPermissionEvaluator();
  }

  @Bean
  public AclCache aclCache(CacheManager cacheManager, PermissionGrantingStrategy permissionGrantingStrategy,
                           AclAuthorizationStrategy aclAuthorizationStrategy) {

    cacheManager.addCacheIfAbsent("aclCache");
    return new EhCacheBasedAclCache(cacheManager.getCache("aclCache"), permissionGrantingStrategy, aclAuthorizationStrategy);
  }

  @Bean
  public LookupStrategy lookupStrategy(DataSource dataSource, AclCache aclCache,
                                       AclAuthorizationStrategy aclAuthorizationStrategy, PermissionGrantingStrategy permissionGrantingStrategy) {

    return new BasicLookupStrategy(dataSource, aclCache, aclAuthorizationStrategy, permissionGrantingStrategy);
  }

  @Bean
  public AclAuthorizationStrategy aclAuthorizationStrategy() {
    return new AclAuthorizationStrategyImpl(grantedAuthorities());
  }

  public GrantedAuthority[] grantedAuthorities() {
    GrantedAuthority[] grantedAuthorities = new GrantedAuthority[1];
    grantedAuthorities[0] = new SimpleGrantedAuthority(Role.ADMIN);

    return grantedAuthorities;
  }

  @Bean
  public PermissionGrantingStrategy permissionGrantingStrategy() {
    return new DefaultPermissionGrantingStrategy(auditLogger());
  }

  public AuditLogger auditLogger() {
    return new ConsoleAuditLogger();
  }

  @Bean
  public RoleHierarchy roleHierarchy() {
    RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
    roleHierarchy.setHierarchy("ROLE_ADMIN > ROLE_USER");

    // Guess this works.
    DefaultMethodSecurityExpressionHandler expressionHandler = (DefaultMethodSecurityExpressionHandler) super.getExpressionHandler();
    expressionHandler.setRoleHierarchy(roleHierarchy);

    return roleHierarchy;
  }
}
