/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.config;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class DockerConfig {

  @Bean
  public DockerClient getDockerClient() {

    // TODO most of these should be filtered properties or env variables
    String home = System.getProperty("user.home");

    // Why are these different? No clue. Betting im missing something.
    File windowsCertPath = new File(home + "/.docker/machine/machines/default/");
    File macCertPath = new File(home + "/.boot2docker/certs/boot2docker-vm");
    File certPath;

    /*if(windowsCertPath.exists()) {
      certPath = windowsCertPath;
    } else {
      certPath = macCertPath;
    }*/

    DockerClientConfig config = DockerClientConfig
        .createDefaultConfigBuilder()
        .withUri("https://192.168.99.100:2376")
        .withDockerCertPath(windowsCertPath.getAbsolutePath())
        .build();

    return DockerClientBuilder.getInstance(config).build();
  }
}
