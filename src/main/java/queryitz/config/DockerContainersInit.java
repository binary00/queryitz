/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import queryitz.docker.ContainerStartupConfig;
import queryitz.docker.DockerService;
import queryitz.docker.LogVerify;

import javax.annotation.PostConstruct;

/**
 *
 * Not a configuration class. It does instantiate docker containers in
 * dev mode to for the DBProfiles that are automatically created
 * from the database datainit scripts from DBConfig.
 *
 */
@Component
public class DockerContainersInit {

  @Value("${spring.datasource.platform}")
  private String platform;

  @Autowired private DockerService dockerService;

  private static final ContainerStartupConfig postgresql93;
  private static final ContainerStartupConfig oracle12c;

  static {
    postgresql93 = new ContainerStartupConfig();
    postgresql93.setContainerName("ci-postgresql-9.3");
    postgresql93.setImageName("binary00/qumite-ci-postgresql-9.3");
    postgresql93.setCheckImage(false);
    postgresql93.setCleanOldContainer(true);
    postgresql93.addPortBinding(5432, 5432);

    oracle12c = new ContainerStartupConfig();
    oracle12c.setContainerName("ci-oracle-12c");
    oracle12c.setImageName("binary00/qumite-ci-oracle-12c");
    oracle12c.setCheckImage(false);
    oracle12c.setCleanOldContainer(true);
    oracle12c.setPrivileged(true);
    oracle12c.addPortBinding(1521, 1521);
    oracle12c.setLogVerify(new LogVerify("Service \"ORCL\" has 1 instance(s)"));
  }

  @PostConstruct
  private void init() {

    // Only meant for development
    if(!"postgresql".equals(platform)) {
      dockerService.startupContainer(postgresql93);
      //dockerService.startupContainer(oracle12c);
    }
  }
}
