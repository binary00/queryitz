/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.driver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import queryitz.modules.dbprofile.domain.DBProfile;

import java.sql.Driver;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DriverManager {

  static final Logger LOG = LoggerFactory.getLogger(DriverManager.class);

  private static final Map<Long, Driver> drivers = new ConcurrentHashMap<>();

  /**
   * getDriver
   *
   * We want to "cache" our drivers for a DBProfile. Loading a driver from
   * the classloader is a heavy action. So if we're populating a connection pool
   * with connections we can reuse drivers instead of loading them every time
   * from a new classloader. Also should save some memory.
   *
   * @param dbProfile
   * @return
   */
  public static Driver getDriver(DBProfile dbProfile) {

    Driver driver = drivers.get(dbProfile.getId());
    if(driver == null) {
      DriverClassLoader classLoader = new DriverClassLoader(dbProfile.getDriver().getBytes());

      try {
        driver = (Driver)classLoader.findClass(dbProfile.getDriverClass()).newInstance();
        drivers.put(dbProfile.getId(), driver);
      } catch(Exception e) {
        LOG.error("Unable to get driver for " + dbProfile.getName(), e);
      }
    }

    return driver;
  }
}
