/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.driver;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class DriverFinder extends DriverClassLoader {

  public DriverFinder(byte[] driverData) {
    super(driverData);
  }

  public String getDriverClassName() {
    String driver = getDriverClassName40Plus();
    return driver != null ? driver : getDriverClassNameOldSchool();
  }

  /**
   * getDriverClassName40Plus
   *
   * This is for JDBC 4.0+ versions of drivers. They have an
   * explicit location for their driver classes that implement
   * java.sql.Driver.
   *
   * META-INF/services/java.sql.Driver
   *
   * So instead of looping through the whole jar to find one that implements
   * java.sql.Driver, its faster to just find the location and return the string.
   *
   * There can be multiple classes referenced. Just returning the first one for now.
   *
   * @return
   */
  private String getDriverClassName40Plus() {

    try {
      JarInputStream jarInputStream = new JarInputStream(new ByteArrayInputStream(driverData));

      JarEntry jarEntry;
      while((jarEntry = jarInputStream.getNextJarEntry()) != null) {
        if(!jarEntry.isDirectory()) {

          String entryName = jarEntry.getName();
          if("META-INF/services/java.sql.Driver".equals(entryName)) {
            ByteArrayOutputStream bOS = new ByteArrayOutputStream();
            int count;
            byte data[] = new byte[2156];
            while((count = jarInputStream.read(data, 0, 2156)) != -1){
              bOS.write(data, 0, count);
            }

            byte[] bytes = bOS.toByteArray();
            bOS.flush();
            bOS.close();

            String[] drivers = new String(bytes).split("\n");
            return drivers[0].trim();
          }
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * getDriverClassNameOldSchool
   *
   * JDBC drivers less than version 4.0 we will have to explicitly find
   * the class that implements java.sql.Driver. There can be more than one
   * in some cases. Slower process because you have to define a class each
   * time.
   *
   * @return
   */
  private String getDriverClassNameOldSchool() {
    Set<String> drivers = new HashSet<>();

    try {
      JarInputStream jarInputStream = new JarInputStream(new ByteArrayInputStream(driverData));

      JarEntry jarEntry;
      while((jarEntry = jarInputStream.getNextJarEntry()) != null) {
        if(!jarEntry.isDirectory()) {

          String entryName = jarEntry.getName();
          if(entryName.endsWith(".class")) {
            String className = entryName.replaceAll("/", "\\.").replace(".class", "");

            try {
              Class loadedClass = findClass(className);
              if(loadedClass != null) {
                Class[] implementedClasses = loadedClass.getInterfaces();
                for(Class implementedClass : implementedClasses) {
                  if(implementedClass.getName().equals("java.sql.Driver")) {
                    drivers.add(loadedClass.getName());
                  }
                }
              }
            } catch(Exception e) {
            }
          }
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    if(drivers.size() > 1) {
      // Right now the .Driver is for Mysql drivers..There could be a bunch of cases.
      // May just want to let the user pick.
      Optional<String> optional = drivers.stream().filter(driver -> driver.contains(".Driver")).findFirst();
      if(optional != null) return optional.get();
    }

    return drivers.iterator().next().trim();
  }
}
