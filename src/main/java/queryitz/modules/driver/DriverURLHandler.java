/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.driver;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class DriverURLHandler extends URLStreamHandler {

  public static final String PROTOCOL = "mem://";
  private DriverClassLoader driverClassLoader;

  public DriverURLHandler(DriverClassLoader driverClassLoader) {
    this.driverClassLoader = driverClassLoader;
  }

  @Override
  protected void parseURL(URL u, String spec, int start, int limit) {
    String path = spec.replace(PROTOCOL, "");
    super.setURL(u, null, null, 0, null, null, path, null, null);
  }

  @Override
  protected URLConnection openConnection(URL url) throws IOException {
    return new DriverURLConnection(url, driverClassLoader);
  }

  private static class DriverURLConnection extends URLConnection {
    private String fileName;
    private DriverClassLoader classLoader;

    public DriverURLConnection(URL url, DriverClassLoader classLoader) {
      super(url);
      this.classLoader = classLoader;
      fileName = url.getPath();
    }

    @Override
    public void connect() throws IOException {
    }

    @Override
    public InputStream getInputStream() throws IOException {
      return new ByteArrayInputStream(classLoader.getBytesForResource(fileName));
    }
  }
}
