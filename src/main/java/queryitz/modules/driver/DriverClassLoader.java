/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.driver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import static queryitz.modules.driver.DriverURLHandler.PROTOCOL;

public class DriverClassLoader extends ClassLoader {

  static final Logger LOG = LoggerFactory.getLogger(DriverClassLoader.class);

  private HashMap<String, Class> classes = new HashMap<>();
  protected byte[] driverData;

  public DriverClassLoader(byte[] driverData) {
    super(DriverClassLoader.class.getClassLoader());
    this.driverData = driverData;
  }

  @Override
  protected URL findResource(String name) {
    return super.findResource(name);
  }

  @Override
  public InputStream getResourceAsStream(String name) {
    return new ByteArrayInputStream(getBytesForResource(name));
  }

  @Override
  public Class loadClass(String className) throws ClassNotFoundException {
    return findClass(className);
  }

  @Override
  public URL getResource(String name) {

    try {
      return new URL(null, PROTOCOL + name, new DriverURLHandler(this));
    } catch(Throwable e) {
      LOG.error("Unable to create in driver url: ", e);
    }

    return super.getResource(name);
  }

  @Override
  public synchronized Class<?> findClass(String name) throws ClassNotFoundException {
    Class loadedClass;

    // Only want to load classes pertaining to this driver from this class
    // loader. Using the super class loader could mean we're loading the wrong
    // version of driver.
    // List below comes from the sources:
    //     http://docs.oracle.com/javase/7/docs/api/
    //     http://docs.oracle.com/javase/8/docs/api/
    if(name.startsWith("java") || name.startsWith("javax") || name.startsWith("org.ietf.jgss") ||
        name.startsWith("org.omg") || name.startsWith("org.w3c.dom") || name.startsWith("org.xml.sax") ||
        name.startsWith("sun.")) {

      try {
        loadedClass = super.getParent().loadClass(name);
        if(loadedClass != null) return loadedClass;
      } catch(Throwable e) {
        LOG.info("Failed to load class from parent classloader " + name, e);
      }
    }

    loadedClass = classes.get(name);
    if(loadedClass != null) return loadedClass;

    String className = name.replaceAll("\\.", "/").concat(".class");
    byte[] classBytes = getBytesForResource(className);

    if(classBytes != null){
      try {
        loadedClass = defineClass(name, classBytes, 0, classBytes.length);
        classes.put(name, loadedClass);
      } catch(Throwable e) {
        // This could be from a class implementing a third party interface
        // to be used in some scenarios. In our case we should never need these
        // classes.
        LOG.info("Failed to load localized class " + name, e);
      }
    } else {
      LOG.trace("Couldn't load class: " + name);
    }

    return loadedClass;
  }

  protected byte[] getBytesForResource(String name){
    byte[] bytes = null;

    try {
      JarInputStream jarInputStream = new JarInputStream(new ByteArrayInputStream(driverData));
      JarEntry jarEntry;
      while((jarEntry = jarInputStream.getNextJarEntry()) != null) {
        if(!jarEntry.isDirectory() && jarEntry.getName().equals(name)) {
          ByteArrayOutputStream bOS = new ByteArrayOutputStream();
          int count;
          byte data[] = new byte[2156];
          while((count = jarInputStream.read(data, 0, 2156)) != -1){
            bOS.write(data, 0, count);
          }

          bytes = bOS.toByteArray();
          bOS.flush();
          bOS.close();
        }

        jarInputStream.closeEntry();
        if(bytes != null) break;
      }

      jarInputStream.close();
    } catch(Exception e){
      e.printStackTrace();
    }

    return bytes;
  }
}
