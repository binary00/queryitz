/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DBProfileCredsID implements Serializable {

  private Long dbProfileID;
  private Long userID;

  public DBProfileCredsID() {
  }

  public DBProfileCredsID(Long dbProfileID, Long userID) {
    this.dbProfileID = dbProfileID;
    this.userID = userID;
  }

  @Column(name = "DB_PROFILE_ID")
  public Long getDbProfileID() {
    return dbProfileID;
  }

  public void setDbProfileID(Long dbProfileID) {
    this.dbProfileID = dbProfileID;
  }

  @Column(name = "USER_ID")
  public Long getUserID() {
    return userID;
  }

  public void setUserID(Long userID) {
    this.userID = userID;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    DBProfileCredsID that = (DBProfileCredsID) o;

    if (dbProfileID != null ? !dbProfileID.equals(that.dbProfileID) : that.dbProfileID != null) return false;
    if (userID != null ? !userID.equals(that.userID) : that.userID != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = dbProfileID != null ? dbProfileID.hashCode() : 0;
    result = 31 * result + (userID != null ? userID.hashCode() : 0);
    return result;
  }
}
