/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.domain;

import queryitz.modules.dbprofile.enums.DBType;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.domain.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "DB_PROFILE")
public class DBProfile implements Serializable {

  private Long id;
  private String name;
  private String driverClass;
  private String connectionURL;
  private DBType type;
  private Driver driver;
  private User owner;
  private Set<Team> accessTeams;
  private Set<User> accessUsers;

  @Id
  @Column(name = "ID", updatable = false)
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Column(name = "NAME", nullable = false, length = 255)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Column(name = "DRIVER_CLASS", nullable = false, length = 255, updatable = false)
  public String getDriverClass() {
    return driverClass;
  }

  public void setDriverClass(String driverClass) {
    this.driverClass = driverClass;
  }

  @Column(name = "CONNECTION_URL", nullable = false, length = 255)
  public String getConnectionURL() {
    return connectionURL;
  }

  public void setConnectionURL(String connectionURL) {
    this.connectionURL = connectionURL;
  }

  @Enumerated(EnumType.STRING)
  @Column(name = "TYPE", nullable = false, length = 255, updatable = false)
  public DBType getType() {
    return type;
  }

  public void setType(DBType type) {
    this.type = type;
  }

  @OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
  @JoinColumn(name = "DRIVER_ID")
  public Driver getDriver() {
    return driver;
  }

  public void setDriver(Driver driver) {
    this.driver = driver;
  }

  @OneToOne
  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  @OneToMany(targetEntity = Team.class, fetch = FetchType.EAGER)
  @JoinTable(
      name = "dbprofile_teams",
      joinColumns = @JoinColumn(name = "dbprofile_id"),
      inverseJoinColumns = @JoinColumn(name = "team_id")
  )
  public Set<Team> getAccessTeams() {
    return accessTeams;
  }

  public void setAccessTeams(Set<Team> accessTeams) {
    this.accessTeams = accessTeams;
  }

  @OneToMany(targetEntity = User.class, fetch = FetchType.EAGER)
  @JoinTable(
      name = "dbprofile_users",
      joinColumns = @JoinColumn(name = "dbprofile_id"),
      inverseJoinColumns = @JoinColumn(name = "user_id")
  )
  public Set<User> getAccessUsers() {
    return accessUsers;
  }

  public void setAccessUsers(Set<User> accessUsers) {
    this.accessUsers = accessUsers;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    DBProfile dbProfile = (DBProfile) o;

    if(!id.equals(dbProfile.id)) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public String toString() {
    return "DBProfile{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", driverClass='" + driverClass + '\'' +
        ", connectionURL='" + connectionURL + '\'' +
        ", type=" + type +
        ", owner=" + owner +
        ", accessTeams=" + accessTeams +
        '}';
  }
}
