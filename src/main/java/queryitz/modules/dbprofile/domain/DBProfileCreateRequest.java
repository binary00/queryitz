/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DBProfileCreateRequest {

  private String name;
  private String connectionURL;
  private String username;
  private String password;
  private MultipartFile driver;
  private Set<Long> userIDs;
  private Set<Long> teamIDs;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getConnectionURL() {
    return connectionURL;
  }

  public void setConnectionURL(String connectionURL) {
    this.connectionURL = connectionURL;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public MultipartFile getDriver() {
    return driver;
  }

  public void setDriver(MultipartFile driver) {
    this.driver = driver;
  }

  public Set<Long> getUserIDs() {
    return userIDs;
  }

  public void setUserIDs(Set<Long> userIDs) {
    this.userIDs = userIDs;
  }

  public Set<Long> getTeamIDs() {
    return teamIDs;
  }

  public void setTeamIDs(Set<Long> teamIDs) {
    this.teamIDs = teamIDs;
  }
}
