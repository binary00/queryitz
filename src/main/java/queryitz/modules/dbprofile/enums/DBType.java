/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.enums;

import java.net.URI;
import java.net.URISyntaxException;

public enum DBType {

  MYSQL(null),
  POSTGRESQL("public"),
  ORACLE(null),
  DB2(null),
  CASSANDRA(null),
  HSQLDB(null);

  private final String defaultTablesSchema;

  DBType(String defaultTablesSchema){
    this.defaultTablesSchema = defaultTablesSchema;
  };

  public String getDefaultTablesSchema(){
    return defaultTablesSchema;
  }

  public static DBType getDBType(String dbTypeString){
    if(dbTypeString != null){
      for(DBType dbType : DBType.values()){
        if(dbType.name().equalsIgnoreCase(dbTypeString)) return dbType;
      }
    }

    return null;
  }

  public static DBType getDBTypeFromURL(String connectionURL) {

    if(connectionURL != null) {
      String modifiedURL = connectionURL.substring(5);
      try {
        URI uri = new URI(modifiedURL);
        return getDBType(uri.getScheme());
      } catch(URISyntaxException e) {
        e.printStackTrace();
      }
    }

    return null;
  }
}
