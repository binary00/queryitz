/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import queryitz.modules.dbprofile.domain.DBProfileCreds;
import queryitz.modules.dbprofile.domain.DBProfileCredsID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
@Repository("dbProfileCredsDao")
public class DBProfileCredsDao {

  @PersistenceContext
  private EntityManager entityManager;

  public DBProfileCreds get(long dbProfileID, long userID) {

    DBProfileCredsID id = new DBProfileCredsID(dbProfileID, userID);

    return (DBProfileCreds) getSession().get(DBProfileCreds.class, id);
  }

  public DBProfileCreds save(DBProfileCreds dbProfileCreds) {
    Session session = getSession();

    session.save(dbProfileCreds);
    session.flush();
    session.refresh(dbProfileCreds);

    return dbProfileCreds;
  }

  public void delete(long dbProfileID, long userID) {

    DBProfileCreds dbProfileCreds = get(dbProfileID, userID);
    if(dbProfileCreds != null) getSession().delete(dbProfileCreds);

  }

  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }
}
