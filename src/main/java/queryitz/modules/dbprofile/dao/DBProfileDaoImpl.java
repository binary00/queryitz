/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;

@Repository("dbProfileDao")
public class DBProfileDaoImpl implements DBProfileDao {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public Set<DBProfile> getAllProfiles() {
    Criteria criteria = getSession().createCriteria(DBProfile.class);

    return new HashSet<>(criteria.list());
  }

  @Override
  public Set<DBProfile> getOwnedProfiles(User user) {
    Criteria criteria = getSession().createCriteria(DBProfile.class);
    criteria.add(Restrictions.eq("owner", user));

    return new HashSet<>(criteria.list());
  }

  @Override
  public Set<DBProfile> getTeamAccessProfiles(Team team) {
    Criteria criteria = getSession().createCriteria(DBProfile.class);
    criteria.createAlias("accessTeams", "accessTeam");
    criteria.add(Restrictions.eq("accessTeam.id", team.getId()));

    return new HashSet<>(criteria.list());
  }

  @Override
  public Set<DBProfile> getUserAccessProfiles(User user) {
    Criteria criteria = getSession().createCriteria(DBProfile.class);
    criteria.createAlias("accessUsers", "accessUser");
    criteria.add(Restrictions.eq("accessUser.id", user.getId()));

    return new HashSet<>(criteria.list());
  }

  @Override
  public DBProfile saveProfile(DBProfile dbProfile) {
    Session session = getSession();

    session.save(dbProfile);
    session.flush();
    session.refresh(dbProfile);

    return dbProfile;
  }

  @Override
  public void deleteProfile(DBProfile dbProfile) {
    getSession().delete(dbProfile);
  }

  @Override
  public DBProfile updateProfile(DBProfile dbProfile) {

    return null;
  }

  @Override
  public DBProfile getProfile(Long dbProfileID) {
    Criteria criteria = getSession().createCriteria(DBProfile.class);
    criteria.add(Restrictions.eq("id", dbProfileID));

    return (DBProfile) criteria.uniqueResult();
  }

  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }
}
