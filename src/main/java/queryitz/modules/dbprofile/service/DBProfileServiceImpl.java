/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import queryitz.modules.dbprofile.dao.DBProfileCredsDao;
import queryitz.modules.dbprofile.dao.DBProfileDao;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.dbprofile.domain.DBProfileCreateRequest;
import queryitz.modules.dbprofile.domain.DBProfileCreds;
import queryitz.modules.dbprofile.domain.DBProfileCredsID;
import queryitz.modules.permission.domain.TeamSid;
import queryitz.modules.permission.domain.UserSid;
import queryitz.modules.permission.service.PermissionService;
import queryitz.modules.querysession.service.SavedSessionService;
import queryitz.modules.team.domain.Team;
import queryitz.modules.team.service.TeamService;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.service.UserService;

import java.util.Set;

import static org.springframework.security.acls.domain.BasePermission.ADMINISTRATION;
import static org.springframework.security.acls.domain.BasePermission.READ;

@Service
@Transactional
public class DBProfileServiceImpl implements DBProfileService {

  @Autowired private DBProfileDao dbProfileDao;
  @Autowired private DBProfileCredsDao profileCredsDao;
  @Autowired private UserService userService;
  @Autowired private TeamService teamService;
  @Autowired private SavedSessionService savedSessionService;
  @Autowired private ModelMapper mapper;
  @Autowired private PermissionService permissionService;

  @Value("${encryption.password}")
  private String encryptionPassword;

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN') || hasPermission(#dbProfile, 'WRITE')")
  public DBProfile updateDBProfile(DBProfile dbProfile) throws Exception {

    // Update is complicated. Another day.
    throw new Exception("Method not implemented");

    //return dbProfileDao.updateProfile(dbProfile);
  }

  @Override
  @PreAuthorize("hasRole('ROLE_USER')")
  public DBProfile createDBProfile(DBProfileCreateRequest createRequest) {
    User currentUser = userService.getCurrentUser();

    DBProfile dbProfile = mapper.map(createRequest, DBProfile.class);
    dbProfile.setOwner(currentUser);

    Set<Team> accessTeams = teamService.getTeamsFromIDs(createRequest.getTeamIDs());
    dbProfile.setAccessTeams(accessTeams);

    Set<User> accessUsers = userService.getUsersFromIDs(createRequest.getUserIDs());
    dbProfile.setAccessUsers(accessUsers);

    dbProfile = dbProfileDao.saveProfile(dbProfile);
    final DBProfile finalDbProfile = dbProfile;

    permissionService.addPermission(finalDbProfile, new UserSid(currentUser), ADMINISTRATION);
    if(accessTeams != null) accessTeams.forEach(team -> addTeamPermission(finalDbProfile, team, READ));
    if(accessUsers != null) accessUsers.forEach(user -> addUserPermission(finalDbProfile, user, READ));

    createDBProfileCreds(dbProfile.getId(), createRequest.getUsername(), createRequest.getPassword());

    return dbProfile;
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN') || hasPermission(#id, 'queryitz.modules.dbprofile.domain.DBProfile', 'READ')")
  public DBProfile getDBProfile(long id) {
    return dbProfileDao.getProfile(id);
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN') || hasPermission(#id, 'queryitz.modules.dbprofile.domain.DBProfile', 'DELETE')")
  public void deleteDBProfile(long id) {
    DBProfile dbProfile = dbProfileDao.getProfile(id);

    if(dbProfile != null) {
      dbProfileDao.deleteProfile(dbProfile);
      profileCredsDao.delete(id, userService.getCurrentUser().getId());
      permissionService.removeAllPermissions(dbProfile);
      savedSessionService.deleteSavedSession(id);
    }
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteUserDBProfiles(User user) {
    Set<DBProfile> ownedDBProfiles = dbProfileDao.getOwnedProfiles(user);
    ownedDBProfiles.forEach((dbProfile) -> deleteDBProfile(dbProfile.getId()));
  }

  @Override
  @PreAuthorize("hasRole('ROLE_USER')")
  @PostFilter("hasRole('ROLE_ADMIN') || hasPermission(filterObject, 'READ')")
  public Set<DBProfile> getAllProfiles() {
    return dbProfileDao.getAllProfiles();
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN') || hasPermission(#dbProfileID, 'queryitz.modules.dbprofile.domain.DBProfile', 'CREATE')")
  public DBProfileCreds createDBProfileCreds(long dbProfileID, String username, String password) {

    String salt = KeyGenerators.string().generateKey();
    String encryptedPassword = Encryptors.delux(encryptionPassword, salt).encrypt(password);

    DBProfileCreds dbProfileCreds = new DBProfileCreds();
    dbProfileCreds.setUsername(username);
    dbProfileCreds.setPassword(encryptedPassword);
    dbProfileCreds.setSalt(salt);

    DBProfileCredsID dbProfileCredsID = new DBProfileCredsID(dbProfileID, userService.getCurrentUser().getId());
    dbProfileCreds.setId(dbProfileCredsID);

    return profileCredsDao.save(dbProfileCreds);
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN') || hasPermission(#dbProfileID, 'queryitz.modules.dbprofile.domain.DBProfile', 'READ')")
  public DBProfileCreds getDBProfileCreds(long dbProfileID) {
    User user = userService.getCurrentUser();

    if(user != null) {
      DBProfileCreds profileCreds = profileCredsDao.get(dbProfileID, user.getId());
      if(profileCreds != null) {
        String decryptedPassword =
                Encryptors.delux(encryptionPassword, profileCreds.getSalt()).decrypt(profileCreds.getPassword());

        profileCreds.setPassword(decryptedPassword);
        profileCreds.setSalt(null);
        return profileCreds;
      }
    }

    return null;
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void addTeamPermission(DBProfile dbProfile, Team team, Permission permission) {
    permissionService.addPermission(dbProfile, new TeamSid(team), permission);
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void removeAllTeamPermissions(Team team) {
    Set<DBProfile> accessedProfiles = dbProfileDao.getTeamAccessProfiles(team);
    for(DBProfile accessedProfile : accessedProfiles) {
      accessedProfile.getAccessTeams().remove(team);
      dbProfileDao.updateProfile(accessedProfile);
      permissionService.removePermissions(accessedProfile, new TeamSid(team));
    }
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void addUserPermission(DBProfile dbProfile, User user, Permission permission) {
    permissionService.addPermission(dbProfile, new UserSid(user), permission);
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void removeAllUserPermissions(User user) {
    Set<DBProfile> accessedProfiles = dbProfileDao.getUserAccessProfiles(user);
    for(DBProfile accessedProfile : accessedProfiles) {
      accessedProfile.getAccessUsers().remove(user);
      dbProfileDao.updateProfile(accessedProfile);
      permissionService.removePermissions(accessedProfile, new UserSid(user));
    }
  }
}
