/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.service;

import org.springframework.security.acls.model.Permission;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.dbprofile.domain.DBProfileCreateRequest;
import queryitz.modules.dbprofile.domain.DBProfileCreds;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.domain.User;

import java.util.Set;

public interface DBProfileService {

  public DBProfile updateDBProfile(DBProfile dbProfile) throws Exception;
  public DBProfile createDBProfile(DBProfileCreateRequest dbProfileCreateRequest);
  public DBProfile getDBProfile(long id);
  public void deleteDBProfile(long id);
  public void deleteUserDBProfiles(User user);
  public Set<DBProfile> getAllProfiles();
  public DBProfileCreds createDBProfileCreds(long dbProfileID, String username, String password);
  public DBProfileCreds getDBProfileCreds(long dbProfileID);
  public void addTeamPermission(DBProfile dbProfile, Team team, Permission permission);
  public void removeAllTeamPermissions(Team team);
  public void addUserPermission(DBProfile dbProfile, User user, Permission permission);
  public void removeAllUserPermissions(User user);
}
