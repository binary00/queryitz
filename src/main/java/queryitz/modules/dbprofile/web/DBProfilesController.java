/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.dbprofile.domain.DBProfileCreateRequest;
import queryitz.modules.dbprofile.service.DBProfileService;

import java.util.Set;

@RestController
@RequestMapping("dbprofiles")
public class DBProfilesController {

  @Autowired
  private DBProfileService dbProfileService;

  @ResponseBody
  @RequestMapping(method = RequestMethod.POST)
  public DBProfile createDBProfile(DBProfileCreateRequest dbProfileCreateRequest) {
    return dbProfileService.createDBProfile(dbProfileCreateRequest);
  }

  @ResponseBody
  @RequestMapping(value = "{id}", method = RequestMethod.GET)
  public DBProfile getDBProfile(@PathVariable long id) {
    return dbProfileService.getDBProfile(id);
  }

  @ResponseBody
  @RequestMapping(method = RequestMethod.GET)
  public Set<DBProfile> getAllDBProfiles() {
    return dbProfileService.getAllProfiles();
  }

  @ResponseBody
  @RequestMapping(value = "{id}", method = RequestMethod.PUT)
  public void updateDBProfile(@RequestBody DBProfile dbProfile) throws Exception {
    dbProfileService.updateDBProfile(dbProfile);
  }

  @ResponseBody
  @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
  public void deleteDBProfile(@PathVariable long id) {
    dbProfileService.deleteDBProfile(id);
  }

  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ExceptionHandler(AccessDeniedException.class)
  public void handleAccessDenied() {
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(Exception.class)
  public void handleError() {
  }
}
