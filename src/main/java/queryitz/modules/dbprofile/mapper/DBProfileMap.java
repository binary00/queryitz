/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbprofile.mapper;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;
import org.springframework.web.multipart.MultipartFile;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.dbprofile.domain.DBProfileCreateRequest;
import queryitz.modules.dbprofile.domain.Driver;
import queryitz.modules.dbprofile.enums.DBType;
import queryitz.modules.driver.DriverFinder;

import java.io.IOException;

public class DBProfileMap extends PropertyMap<DBProfileCreateRequest, DBProfile> {

  @Override
  protected void configure() {
    using(driverClassConverter).map(source.getDriver()).setDriverClass(null);
    using(driverConverter).map(source.getDriver()).setDriver(null);
    using(dbTypeConverter).map(source.getConnectionURL()).setType(null);
  }

  Converter<MultipartFile, Driver> driverConverter = new AbstractConverter<MultipartFile, Driver>() {

    @Override
    protected Driver convert(MultipartFile multipartFile) {

      if(multipartFile != null) {
        try {
          return new Driver(multipartFile.getBytes());
        } catch(IOException e) {
          e.printStackTrace();
        }
      }

      return new Driver(new byte[0]);
    }
  };

  Converter<MultipartFile, String> driverClassConverter = new AbstractConverter<MultipartFile, String>() {

    @Override
    protected String convert(MultipartFile multipartFile) {

      if(multipartFile != null) {
        try {
          DriverFinder finder = new DriverFinder(multipartFile.getBytes());
          return finder.getDriverClassName();
        } catch(IOException e) {
          e.printStackTrace();
        }
      }

      return null;
    }
  };

  Converter<String, DBType> dbTypeConverter = new AbstractConverter<String, DBType>() {

    @Override
    protected DBType convert(String connectionURL) {
      return DBType.getDBTypeFromURL(connectionURL);
    }
  };
}
