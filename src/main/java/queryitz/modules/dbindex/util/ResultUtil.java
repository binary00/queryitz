/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.util;

import queryitz.modules.dbindex.domain.db.Column;
import queryitz.modules.dbindex.domain.db.Database;
import queryitz.modules.dbindex.domain.db.Schema;
import queryitz.modules.dbindex.domain.db.Table;
import queryitz.modules.dbindex.domain.result.*;

import java.util.ArrayList;
import java.util.List;

public class ResultUtil {

  public static Result getResult(DatabaseResult databaseResult) {
    return getResult(databaseResult, null, null, null);
  }

  public static Result getResult(DatabaseResult databaseResult, String schemaName) {
    return getResult(databaseResult, schemaName, null, null);
  }

  public static Result getResult(DatabaseResult databaseResult, String schemaName, String tableName) {
    return getResult(databaseResult, schemaName, tableName, null);
  }

  public static Result getResult(DatabaseResult databaseResult, String schemaName, String tableName, String columnName) {

    if(databaseResult != null && databaseResult.getDatabase() != null) {
      Database database = databaseResult.getDatabase();

      if (database != null) {
        List<Schema> schemaList = database.getSchemas();

        if (schemaList != null) {

          if (schemaName == null) {
            List<String> schemas = new ArrayList<>();
            schemaList.forEach(schema -> schemas.add(schema.getName()));

            return new SchemasResult(schemas);
          } else {

            List<Table> tableList = null;
            for (Schema schema : schemaList) {
              if (schema.getName().equals(schemaName)) {
                tableList = schema.getTables();
              }
            }

            if (tableList != null) {
              if (tableName == null) {
                List<String> tables = new ArrayList<>();
                tableList.forEach(table -> tables.add(table.getName()));

                return new TablesResult(tables);
              } else {

                List<Column> columnList = null;
                for (Table table : tableList) {
                  if (table.getName().equals(tableName)) {
                    columnList = table.getColumns();
                  }
                }

                if (columnList != null) {
                  if (columnName == null) {
                    List<String> columns = new ArrayList<>();
                    columnList.forEach(column -> columns.add(column.getName()));

                    return new ColumnsResult(columns);
                  } else {

                    for (Column column : columnList) {
                      if (column.getName().equals(columnName)) {
                        return new ColumnResult(column);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    return null;
  }

}
