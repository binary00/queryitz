/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.domain;

import java.time.Duration;
import java.time.LocalDateTime;

public class IndexTime {

  private LocalDateTime startTime;
  private LocalDateTime stopTime;
  private Duration duration;

  public IndexTime(LocalDateTime startTime, LocalDateTime stopTime) {
    this.startTime = startTime;
    this.stopTime = stopTime;
    this.duration = Duration.between(startTime, stopTime);
  }

  public String getStartTime() {
    return startTime.toString();
  }

  public String getStopTime() {
    return stopTime.toString();
  }

  public String getDuration() {
    return duration.toString();
  }
}
