/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.domain;

import queryitz.modules.user.domain.User;

import java.time.LocalDateTime;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import static queryitz.modules.dbindex.domain.IndexState.State.*;

public class IndexState {

  protected enum State {
    CREATED,
    RUNNING,
    FINISHED
  }

  private long dbProfileID;
  private User user;
  private ArrayBlockingQueue<IndexTime> historyQueue;
  private LocalDateTime startTime;
  private AtomicInteger dbThreadCount;
  private AtomicInteger schemaThreadCount;
  private AtomicInteger tableThreadCount;
  private State state;
  private IndexTime[] history;

  public IndexState() {
  }

  public IndexState(long dbProfileID, User user) {
    this.dbProfileID = dbProfileID;
    this.user = user;

    historyQueue = new ArrayBlockingQueue(10);
    dbThreadCount = new AtomicInteger(0);
    schemaThreadCount = new AtomicInteger(0);
    tableThreadCount = new AtomicInteger(0);
    state = CREATED;
  }

  public long getDbProfileID() {
    return dbProfileID;
  }

  public long getUserID() {
    return user.getId();
  }

  public AtomicInteger getDBThreadCount() {
    return dbThreadCount;
  }

  public void incrementDBThreadCount() {
    dbThreadCount.incrementAndGet();
  }

  public void decrementDBThreadCount() {
    dbThreadCount.decrementAndGet();
  }

  public AtomicInteger getSchemaThreadCount() {
    return schemaThreadCount;
  }

  public void incrementSchemaThreadCount() {
    schemaThreadCount.incrementAndGet();
  }

  public void decrementSchemaThreadCount() {
    schemaThreadCount.decrementAndGet();
  }

  public AtomicInteger getTableThreadCount() {
    return tableThreadCount;
  }

  public void incrementTableThreadCount() {
    tableThreadCount.incrementAndGet();
  }

  public void decrementTableThreadCount() {
    tableThreadCount.decrementAndGet();
  }

  public void startRunning() {
    state = RUNNING;
    startTime = LocalDateTime.now();
  }

  public void stopRunning() {
    IndexTime indexTime = new IndexTime(startTime, LocalDateTime.now());
    state = FINISHED;
    startTime = null;

    try {
      historyQueue.add(indexTime);
    } catch(IllegalStateException e) {
      historyQueue.remove();
      historyQueue.add(indexTime);
    }
  }

  public boolean isRunning() {
    return state.equals(RUNNING);
  }

  public String getStartTime() {
    return startTime != null ? startTime.toString() : "";
  }

  public IndexTime[] getHistory() {
    history = new IndexTime[historyQueue.size()];
    historyQueue.toArray(history);

    return history;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    IndexState indexKey = (IndexState) o;

    if(dbProfileID != indexKey.dbProfileID) return false;
    if(user.getId() != indexKey.getUserID()) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (dbProfileID ^ (dbProfileID >>> 32));
    result = 31 * result + (int) (user.getId() ^ (user.getId() >>> 32));
    return result;
  }
}
