/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import queryitz.modules.dbindex.domain.result.DatabaseResult;
import queryitz.modules.dbindex.domain.result.Result;
import queryitz.modules.dbindex.service.DatabaseIndexService;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.service.UserService;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static queryitz.modules.dbindex.util.ResultUtil.getResult;

@RestController
@RequestMapping("dbindex")
public class DBIndexController {

  @Autowired private DatabaseIndexService databaseIndexService;
  @Autowired private UserService userService;

  @RequestMapping("{dbProfileID}")
  public DatabaseResult getDatabase(@PathVariable("dbProfileID") long dbProfileID) {
    User user = userService.getCurrentUser();
    return databaseIndexService.getSchemaResult(dbProfileID, user.getId());
  }

  @RequestMapping(value = "{dbProfileID}", method = DELETE)
  public void reloadDatabase(@PathVariable("dbProfileID") long dbProfileID) {
    User user = userService.getCurrentUser();
    databaseIndexService.reloadSchemaResult(dbProfileID, user.getId());
  }

  @RequestMapping("{dbProfileID}/schemas")
  public Result getSchemas(@PathVariable("dbProfileID") long dbProfileID) {
    return getResult(getDatabase(dbProfileID));
  }

  @RequestMapping("{dbProfileID}/schemas/{schema}/tables")
  public Result getTables(@PathVariable("dbProfileID") long dbProfileID,
                                @PathVariable("schema") String schema) {

    return getResult(getDatabase(dbProfileID), schema);
  }

  @RequestMapping("{dbProfileID}/schemas/{schema}/tables/{table}/columns")
  public Result getColumns(@PathVariable("dbProfileID") long dbProfileID,
                                @PathVariable("schema") String schema,
                                @PathVariable("table") String table) {

    return getResult(getDatabase(dbProfileID), schema, table);
  }

  @RequestMapping("{dbProfileID}/schemas/{schema}/tables/{table}/columns/{column}")
  public Result getColumn(@PathVariable("dbProfileID") long dbProfileID,
                                @PathVariable("schema") String schema,
                                @PathVariable("table") String table,
                                @PathVariable("column") String column) {

    return getResult(getDatabase(dbProfileID), schema, table, column);
  }
}
