/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.index;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import queryitz.modules.connection.ConnectionPool;
import queryitz.modules.dbindex.domain.IndexState;
import queryitz.modules.dbindex.domain.db.Schema;
import queryitz.modules.dbindex.domain.db.Table;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class SchemaIndexRunnable implements Callable<Boolean> {

  static final Logger LOG = LoggerFactory.getLogger(SchemaIndexRunnable.class);

  private ConnectionPool connectionPool;
  private ThreadPoolTaskExecutor databaseExecutor;
  private Schema schema;
  private IndexState indexKey;

  public SchemaIndexRunnable(
      ConnectionPool connectionPool,
      ThreadPoolTaskExecutor databaseExecutor,
      Schema schema,
      IndexState indexKey) {

    this.connectionPool = connectionPool;
    this.databaseExecutor = databaseExecutor;
    this.schema = schema;
    this.indexKey = indexKey;
  }

  @Override
  public Boolean call() throws Exception {
    indexKey.incrementSchemaThreadCount();
    Connection connection = null;
    ResultSet tablesResultSet = null;

    try {
      connection = connectionPool.borrowObject();
      DatabaseMetaData databaseMetaData = connection.getMetaData();

      tablesResultSet = databaseMetaData.getTables(null, schema.getName(), "%", null);
      if(tablesResultSet != null) {
        List<Table> tables = new ArrayList<>();
        while(tablesResultSet.next()){
          Table dbTableView = new Table();
          dbTableView.setCatalog(tablesResultSet.getString(1));
          dbTableView.setSchema(tablesResultSet.getString(2));
          dbTableView.setName(tablesResultSet.getString(3));
          dbTableView.setType(tablesResultSet.getString(4));
          dbTableView.setRemarks(tablesResultSet.getString(5));

          tables.add(dbTableView);
        }

        tables.forEach(table -> databaseExecutor.submit(new TableIndexRunnable(connectionPool, table, indexKey)));

        schema.setTables(tables);
        return true;
      }
    } catch(Exception e) {
      LOG.error("", e);
      throw e;
    } finally {
      try {
        tablesResultSet.close();
      } catch(Exception e) {
        LOG.error("", e);
      }

      if(connection != null) connectionPool.returnObject(connection);
      indexKey.decrementSchemaThreadCount();
    }

    return false;
  }
}
