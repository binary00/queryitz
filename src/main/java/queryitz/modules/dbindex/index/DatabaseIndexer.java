/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.index;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.concurrent.ListenableFuture;
import queryitz.modules.connection.ConnectionPool;
import queryitz.modules.connection.ConnectionPoolManager;
import queryitz.modules.dbindex.domain.IndexState;
import queryitz.modules.dbindex.domain.db.Database;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.dbprofile.service.DBProfileService;
import queryitz.modules.user.domain.User;

/**
 * DatabaseIndexer
 *
 * Not really super proud of this service. Needs to be
 * way smarter with resources, but for now it's just
 * working and faster than a single threaded instance.
 *
 */
@Service
@Transactional
public class DatabaseIndexer {

  @Autowired private ConnectionPoolManager connectionPoolManager;
  @Autowired private DBProfileService dbProfileService;
  @Autowired private CacheManager cacheManager;
  @Autowired private SimpMessagingTemplate messagingTemplate;
  @Autowired private IndexManager indexManager;

  @Async
  public synchronized void index(long dbProfileID, User user) throws Exception {
    IndexState indexState = indexManager.get(dbProfileID, user);

    if(!indexState.isRunning()) {
      DBProfile dbProfile = dbProfileService.getDBProfile(dbProfileID);
      dbProfile.getDriver().getBytes();

      ConnectionPool connectionPool = connectionPoolManager.getPool(dbProfile);
      ThreadPoolTaskExecutor databaseExecutor = new ThreadPoolTaskExecutor();
      databaseExecutor.setCorePoolSize(1);
      databaseExecutor.initialize();

      ListenableFuture<Database> databaseFuture =
          databaseExecutor.submitListenable(
              new DatabaseIndexCallable(dbProfile, connectionPool, indexState));

      messagingTemplate.convertAndSendToUser(user.getUsername(), "/topic/dbindexstatemsg/message", indexState);
      databaseFuture.addCallback(new DatabaseIndexCallback(cacheManager, user, dbProfile, indexManager, messagingTemplate));
    }
  }

}
