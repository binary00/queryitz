/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.index;

import org.springframework.cache.CacheManager;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.util.concurrent.ListenableFutureCallback;
import queryitz.modules.dbindex.domain.IndexState;
import queryitz.modules.dbindex.domain.db.Database;
import queryitz.modules.dbindex.domain.result.DatabaseResult;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.user.domain.User;

public class DatabaseIndexCallback implements ListenableFutureCallback<Database> {

  private CacheManager cacheManager;
  private User user;
  private DBProfile dbProfile;
  private IndexManager indexManager;
  private SimpMessagingTemplate messagingTemplate;

  public DatabaseIndexCallback(CacheManager cacheManager, User user, DBProfile dbProfile, IndexManager indexManager, SimpMessagingTemplate messagingTemplate) {
    this.cacheManager = cacheManager;
    this.user = user;
    this.dbProfile = dbProfile;
    this.indexManager = indexManager;
    this.messagingTemplate = messagingTemplate;
  }

  @Override
  public void onSuccess(Database database) {
    cacheManager.getCache("schemaResult").put(dbProfile.getId() + "-" + user.getId(), new DatabaseResult(database));
    sendMessage();
  }

  @Override
  public void onFailure(Throwable throwable) {
    sendMessage();
  }

  private void sendMessage() {
    messagingTemplate.convertAndSendToUser(user.getUsername(), "/topic/dbindexstatemsg/message", indexManager.get(dbProfile.getId(), user));
  }
}
