/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.index;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import queryitz.modules.connection.ConnectionPool;
import queryitz.modules.dbindex.domain.IndexState;
import queryitz.modules.dbindex.domain.db.Column;
import queryitz.modules.dbindex.domain.db.Table;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class TableIndexRunnable implements Callable<Boolean> {

  static final Logger LOG = LoggerFactory.getLogger(TableIndexRunnable.class);

  private ConnectionPool connectionPool;
  private Table table;
  private IndexState indexKey;

  public TableIndexRunnable(ConnectionPool connectionPool, Table table, IndexState indexKey) {
    this.connectionPool = connectionPool;
    this.table = table;
    this.indexKey = indexKey;
  }

  @Override
  public Boolean call() throws Exception {
    indexKey.incrementTableThreadCount();
    Connection connection = null;
    ResultSet columnResultSet = null;

    try {
      connection = connectionPool.borrowObject();
      DatabaseMetaData databaseMetaData = connection.getMetaData();

      columnResultSet = databaseMetaData.getColumns(null, null, table.getName(), null);
      if(columnResultSet != null){
        List<Column> columns = new ArrayList<>();
        while(columnResultSet.next()){
          Column column = new Column();
          column.setName(columnResultSet.getString(4));
          column.setDataType(columnResultSet.getString(5));
          column.setTypeName(columnResultSet.getString(6));
          column.setSize(columnResultSet.getInt(7));
          column.setNullable(columnResultSet.getInt(11) > 0);
          column.setRemarks(columnResultSet.getString(12));
          columns.add(column);
        }

        table.setColumns(columns);
        return true;
      }
    } catch(Exception e) {
      LOG.error("", e);
      throw e;
    } finally {
      try {
        columnResultSet.close();
      } catch(Exception e) {
        LOG.error("", e);
      }

      if(connection != null) connectionPool.returnObject(connection);
      indexKey.decrementTableThreadCount();
    }

    return false;
  }
}
