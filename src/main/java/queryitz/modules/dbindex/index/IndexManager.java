/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.index;

import com.fasterxml.jackson.annotation.JsonIgnore;
import queryitz.modules.dbindex.domain.IndexState;
import queryitz.modules.user.domain.User;

import java.util.Collection;
import java.util.HashMap;

public class IndexManager {

  private static final HashMap<String, IndexState> indexes = new HashMap<>();

  @JsonIgnore
  public synchronized IndexState get(long dbProfileID, User user) {

    String id = dbProfileID + "" + user.getId();
    IndexState indexState = indexes.get(id);

    if(indexState == null) {
      indexState = new IndexState(dbProfileID, user);
      indexes.put(id, indexState);
    }

    return indexState;
  }

  public Collection<IndexState> getIndexStates() {
    return indexes.values();
  }
}
