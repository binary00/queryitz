/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.index;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import queryitz.modules.connection.ConnectionPool;
import queryitz.modules.dbindex.domain.IndexState;
import queryitz.modules.dbindex.domain.db.Database;
import queryitz.modules.dbindex.domain.db.Schema;
import queryitz.modules.dbprofile.domain.DBProfile;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class DatabaseIndexCallable implements Callable<Database> {

  static final Logger LOG = LoggerFactory.getLogger(DatabaseIndexCallable.class);

  private DBProfile dbProfile;
  private ThreadPoolTaskExecutor databaseExecutor;
  private ConnectionPool connectionPool;
  private IndexState indexKey;

  public DatabaseIndexCallable(
      DBProfile dbProfile,
      ConnectionPool connectionPool,
      IndexState indexKey) {

    this.dbProfile = dbProfile;
    this.connectionPool = connectionPool;
    this.indexKey = indexKey;

    databaseExecutor = new ThreadPoolTaskExecutor();
    databaseExecutor.setWaitForTasksToCompleteOnShutdown(true);
    databaseExecutor.setCorePoolSize(10);
    databaseExecutor.initialize();
  }

  @Override
  public Database call() throws Exception {
    indexKey.startRunning();
    indexKey.incrementDBThreadCount();
    Database database = new Database();
    Connection connection = null;
    ResultSet schemaResultSet = null;

    try {
      connection = connectionPool.borrowObject();
      DatabaseMetaData databaseMetaData = connection.getMetaData();

      List<Schema> schemas = new ArrayList<>();
      schemaResultSet = databaseMetaData.getSchemas();
      while(schemaResultSet.next()) {
        schemas.add(new Schema(schemaResultSet.getString(1)));
      }
      schemaResultSet.close();

      schemas.forEach(schema -> databaseExecutor.submit(new SchemaIndexRunnable(connectionPool, databaseExecutor, schema, indexKey)));

      database.setSchemas(schemas);
      database.setName(dbProfile.getName());

      while(databaseExecutor.getActiveCount() > 0) {
        Thread.sleep(1000);
      }

      databaseExecutor.shutdown();
    } catch(Exception e) {
      LOG.error("", e);
      throw e;
    } finally {
      try {
        schemaResultSet.close();
      } catch(Exception e) {
        LOG.error("", e);
      }

      if(connection != null) connectionPool.returnObject(connection);
      indexKey.decrementDBThreadCount();
      indexKey.stopRunning();
    }

    return database;
  }
}
