/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.dbindex.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import queryitz.modules.dbindex.domain.result.DatabaseResult;
import queryitz.modules.dbindex.index.DatabaseIndexer;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.service.UserService;

@Service
public class DatabaseIndexServiceImpl implements DatabaseIndexService {

  static final Logger LOG = LoggerFactory.getLogger(DatabaseIndexServiceImpl.class);

  @Autowired private DatabaseIndexer databaseIndexer;
  @Autowired private UserService userService;
  @Autowired private CacheManager cacheManager;

  private void getDatabaseDataThreaded(long dbProfileID, User user) {

    try {
      databaseIndexer.index(dbProfileID, user);
    } catch(Exception e) {
      LOG.error("Index failed for dbProfileID : " + dbProfileID + " userID : " + user.getId(), e);
    }

  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN') || hasPermission(#dbProfileID, 'queryitz.modules.dbprofile.domain.DBProfile', 'READ')")
  @Cacheable(value = "schemaResult", key = "#dbProfileID.toString().concat('-').concat(#userID)")
  public DatabaseResult getSchemaResult(long dbProfileID, long userID) {
    User user = userService.getUser(userID);
    User currentUser = userService.getCurrentUser();

    if(currentUser.equals(user)) {
      getDatabaseDataThreaded(dbProfileID, currentUser);
    }

    return null;
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN') || hasPermission(#dbProfileID, 'queryitz.modules.dbprofile.domain.DBProfile', 'READ')")
  public void reloadSchemaResult(long dbProfileID, long userID) {
    cacheManager.getCache("schemaResult").evict(dbProfileID + "-" + userID);
    getSchemaResult(dbProfileID, userID);
  }

}
