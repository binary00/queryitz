/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.auth.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import queryitz.modules.auth.domain.AuthStatus;
import queryitz.modules.auth.domain.LoginRequest;
import queryitz.modules.auth.service.AuthService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("auth")
public class AuthController {

  @Autowired private AuthService authService;

  @RequestMapping(value = "login", method = RequestMethod.POST)
  public void login(@RequestBody LoginRequest loginRequest, HttpServletRequest request) throws Exception {
    authService.login(loginRequest, request);
  }

  @RequestMapping(value = "logout", method = RequestMethod.DELETE)
  public void logout(HttpServletRequest request) throws Exception {
    authService.logout(request);
  }

  @RequestMapping(value = "status", method = RequestMethod.GET)
  public AuthStatus status(HttpServletRequest request) {
    return authService.status(request);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity handleError(HttpServletRequest req, Exception exception) {
    return new ResponseEntity(exception.getMessage(), HttpStatus.UNAUTHORIZED);
  }
}
