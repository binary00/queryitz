/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import queryitz.modules.auth.domain.AuthStatus;
import queryitz.modules.auth.domain.LoginRequest;
import queryitz.modules.user.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Service
public class AuthService {

  @Autowired private UserService userService;

  public void login(LoginRequest loginRequest, HttpServletRequest request) throws ServletException {
    request.login(loginRequest.getUsername(), loginRequest.getPassword());
  }

  public void logout(HttpServletRequest request) throws ServletException {
    request.logout();
  }

  public AuthStatus status(HttpServletRequest request) {
    AuthStatus authStatus = new AuthStatus(false);

    if(request.getUserPrincipal() != null) {
      UsernamePasswordAuthenticationToken principal = (UsernamePasswordAuthenticationToken) request.getUserPrincipal();
      authStatus.setAuthenticated(principal.isAuthenticated());
      if(principal.isAuthenticated()) {
        authStatus.setUserID(userService.getCurrentUser().getId());
      }
    }

    return authStatus;
  }
}
