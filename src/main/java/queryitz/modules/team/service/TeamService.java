/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.team.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import queryitz.modules.dbprofile.service.DBProfileService;
import queryitz.modules.team.dao.TeamDao;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.service.UserService;

import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class TeamService {

  @Autowired private TeamDao teamDao;
  @Autowired private UserService userService;
  @Autowired private DBProfileService dbProfileService;

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public Team createTeam(Team team) {
    teamDao.save(team);
    return team;
  }

  public Team getTeam(Long id) {
    return teamDao.getTeam(id);
  }

  public Team getTeamByName(String name) {
    return teamDao.getTeamByName(name);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public Team updateTeam(Team team) {
    teamDao.update(team);
    return team;
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteTeam(Long id) {
    Team team = teamDao.getTeam(id);
    dbProfileService.removeAllTeamPermissions(team);
    teamDao.delete(team);
  }

  public Set<Team> getAllTeams() {
    return teamDao.getTeams();
  }

  public Set<Team> getTeamsForUser(User user) {
    return teamDao.getTeamsForUser(user);
  }

  public Set<Team> getTeamsFromIDs(Set<Long> teamIDs) {
    if(teamIDs != null) {
      return teamDao.getTeams(teamIDs);
    }

    return null;
  }

  public Set<User> getTeamUsers(Long id) {
    Team team = getTeam(id);
    if(team != null) {
      Set<User> users = team.getUsers();
      return users;
    }

    return null;
  }

  public Set<User> getTeamsUsersByName(String name) {
    Team team = getTeamByName(name);
    if(team != null) return team.getUsers();

    return null;
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void addUser(Long userID, Long teamID) {
    User user = userService.getUser(userID);
    Team team = getTeam(teamID);
    addUser(user, team);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void addUser(User user, Team team) {
    Set<User> users = team.getUsers();
    if(users == null) users = new HashSet<>();

    users.add(user);
    team.setUsers(users);

    teamDao.update(team);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void addUserToTeams(User user, Set<Team> teams) {
    if(teams != null) {
      teams.forEach(team -> addUser(user, team));
    }
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void removeUser(Long userID, Long teamID) {
    User user = userService.getUser(userID);
    Team team = getTeam(teamID);
    removeUser(user, team);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void removeUser(User user, Team team) {
    Set<User> users = team.getUsers();
    if(users == null) users = new HashSet<>();

    users.remove(user);
    team.setUsers(users);

    teamDao.update(team);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void removeUserFromTeams(User user, Set<Team> teams) {
    if(teams != null) {
      teams.forEach(team -> removeUser(user, team));
    }
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteUserTeams(User user) {
    Set<Team> teams = getTeamsForUser(user);
    removeUserFromTeams(user, teams);
  }
}
