/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.team.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import queryitz.modules.team.domain.Team;
import queryitz.modules.team.service.TeamService;
import queryitz.modules.user.domain.User;

import java.util.Set;

@RestController
@RequestMapping("teams")
public class TeamsController {

  @Autowired private TeamService teamService;

  @RequestMapping(method = RequestMethod.POST)
  public Team createTeam(@RequestBody Team team) {
    return teamService.createTeam(team);
  }

  @RequestMapping(value = "{id}", method = RequestMethod.GET)
  public Team getTeam(@PathVariable Long id) {
    return teamService.getTeam(id);
  }

  @RequestMapping(method = RequestMethod.GET)
  public Set<Team> getTeams() {
    return teamService.getAllTeams();
  }

  @RequestMapping(value = "name/{name}", method = RequestMethod.GET)
  public Team getTeamByName(@PathVariable String name) {
    return teamService.getTeamByName(name);
  }

  @RequestMapping(value = "{id}/users", method = RequestMethod.GET)
  public Set<User> getTeamUsers(@PathVariable Long id) {
    return teamService.getTeamUsers(id);
  }

  @RequestMapping(value = "name/{name}/users", method = RequestMethod.GET)
  public Set<User> getTeamUsersByName(@PathVariable String name) {
    return teamService.getTeamsUsersByName(name);
  }

  @RequestMapping(value = "{id}", method = RequestMethod.PUT)
  public void updateTeam(@PathVariable Long id, @RequestBody Team team) {
    teamService.updateTeam(team);
  }

  @RequestMapping(value = "{id}/users/{userID}", method = RequestMethod.PUT)
  public void addUserToTeam(@PathVariable Long id, @PathVariable Long userID) {
    teamService.addUser(userID, id);
  }

  @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
  public void deleteTeam(@PathVariable Long id) {
    teamService.deleteTeam(id);
  }

  @RequestMapping(value = "{id}/users/{userID}", method = RequestMethod.DELETE)
  public void deleteUserFromTeam(@PathVariable Long id, @PathVariable Long userID) {
    teamService.removeUser(userID, id);
  }
}
