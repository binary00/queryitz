/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.team.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;

@Repository("teamDao")
public class TeamDao {

  @PersistenceContext
  private EntityManager entityManager;

  public Team getTeam(Long id) {
    return (Team) getSession().get(Team.class, id);
  }

  public Set<Team> getTeams(Set<Long> ids) {
    Criteria criteria = getSession().createCriteria(Team.class);
    criteria.add(Restrictions.in("id", ids));

    return new HashSet<Team>(criteria.list());
  }

  public Team getTeamByName(String name) {
    Criteria criteria = getSession().createCriteria(Team.class);
    criteria.add(Restrictions.eq("name", name));

    return (Team) criteria.uniqueResult();
  }

  public void save(Team team) {
    Session session = getSession();

    session.saveOrUpdate(team);
    session.flush();
    session.refresh(team);
  }

  public void update(Team team) {
    Session session = getSession();
    session.update(team);
    session.flush();
    session.refresh(team);
  }

  public void delete(Team team) {
    getSession().delete(team);
  }

  public Set<Team> getTeams() {
    Criteria criteria = getSession().createCriteria(Team.class);
    return new HashSet<Team>(criteria.list());
  }

  public Set<Team> getTeamsForUser(User user) {
    Criteria criteria = getSession().createCriteria(Team.class);
    criteria.createAlias("users", "user");
    criteria.add(Restrictions.eq("user.id", user.getId()));

    return new HashSet<Team>(criteria.list());
  }

  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }
}
