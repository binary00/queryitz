/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.query.service;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import queryitz.modules.connection.ConnectionPool;
import queryitz.modules.connection.ConnectionPoolManager;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.dbprofile.service.DBProfileService;
import queryitz.modules.query.async.AsyncQueryManager;
import queryitz.modules.query.domain.Query;
import queryitz.modules.query.domain.ResultData;
import queryitz.modules.user.domain.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import static org.apache.commons.lang.exception.ExceptionUtils.getRootCauseStackTrace;
import static org.apache.commons.lang.exception.ExceptionUtils.getStackTrace;
import static queryitz.modules.query.enums.QueryStatus.*;
import static queryitz.modules.query.util.QueryUtil.getResultData;
import static queryitz.modules.query.util.QueryUtil.isResultSetQuery;

@Service
@Transactional
public class QueryServiceImpl implements QueryService {

  static final Logger LOG = LoggerFactory.getLogger(QueryServiceImpl.class);

  @Autowired private DBProfileService dbProfileService;
  @Autowired private ConnectionPoolManager connectionPoolManager;
  @Autowired private AsyncQueryManager queryThreadManager;

  @SuppressWarnings("SpringJavaAutowiringInspection")
  @Autowired private SimpMessagingTemplate messagingTemplate;

  public Query doQuery(Query query) {
    Query queryResult = new Query(query);

    try {
      long dbProfileID = query.getDbProfileID();
      DBProfile dbProfile = dbProfileService.getDBProfile(dbProfileID);
      ConnectionPool connectionPool = connectionPoolManager.getPool(dbProfile);
      Connection connection = connectionPool.borrowObject();
      Statement statement = null;
      ResultSet resultSet = null;
      int resultCount = -1;

      try {
        String sQuery = query.getQuery();
        statement = connection.createStatement();

        Stopwatch stopWatch = Stopwatch.createStarted();

        if(isResultSetQuery(sQuery)) {
          resultSet = statement.executeQuery(sQuery);
        } else {
          resultCount = statement.executeUpdate(sQuery);
        }

        stopWatch.stop();

        ResultData resultData = getResultData(resultSet, resultCount);
        queryResult.setResultData(resultData);

        queryResult.setElapsedTime(stopWatch.toString());
        queryResult.setStatus(FINISHED);

      } catch(SQLException sqlE) {
        queryResult.setStatus(FINISHED);
        queryResult.setError(true);
        queryResult.setErrorString(sqlE.getMessage());

      } finally {
        connectionPool.returnObject(connection);
        if(resultSet != null) resultSet.close();
        if(statement != null) statement.close();
      }

    } catch(Exception e) {
      queryResult.setError(true);
      queryResult.setStatus(EXCEPTION);
      String[] stack = getRootCauseStackTrace(e);
      if(stack != null && stack.length > 1) {
        queryResult.setErrorString(stack[0] + " " + stack[1]);
      } else {
        queryResult.setErrorString(getStackTrace(e));
      }

      LOG.error("Unable to query : ", e);
    }

    return queryResult;
  }

  @Override
  public void doAsyncQuery(Query query, User user) {
    query.setStatus(RUNNING);
    query.setQueryID(UUID.randomUUID().toString());
    sendQueryUpdateToUser(query, user);

    try {
      queryThreadManager.startQuery(query, user);
    } catch(Exception e) {
      LOG.error("Async Query Exception: ", e);
      query.setError(true);
      query.setStatus(EXCEPTION);
      query.setErrorString(e.getMessage());
      sendQueryUpdateToUser(query, user);
    }
  }

  @Override
  public void asyncQueryCancel(Query query, User user) {
    boolean killed = queryThreadManager.killQuery(query, user);
    if(killed) {
      query.setStatus(CANCELLED);
      sendQueryUpdateToUser(query, user);
    }
  }

  @Override
  public void sendQueryUpdateToUser(Query queryResult, User user) {
    messagingTemplate.convertAndSendToUser(user.getUsername(), "/topic/query/message", queryResult);
  }
}
