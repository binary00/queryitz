/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.query.domain;

import java.util.List;

public class ResultData {

  private List<String> columnHeaders;
  private List<DBRowResult> rowResults;
  private int resultCount;

  public ResultData() {
  }

  public ResultData(List<String> columnHeaders, List<DBRowResult> rowResults, int resultCount) {
    this.columnHeaders = columnHeaders;
    this.rowResults = rowResults;
    this.resultCount = resultCount;
  }

  public List<String> getColumnHeaders() {
    return columnHeaders;
  }

  public void setColumnHeaders(List<String> columnHeaders) {
    this.columnHeaders = columnHeaders;
  }

  public List<DBRowResult> getRowResults() {
    return rowResults;
  }

  public void setRowResults(List<DBRowResult> rowResults) {
    this.rowResults = rowResults;
  }

  public int getResultCount() {
    return resultCount;
  }

  public void setResultCount(int resultCount) {
    this.resultCount = resultCount;
  }
}
