/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.query.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import queryitz.modules.query.enums.QueryStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Query {

  private String query;
  private long dbProfileID;
  private String queryID;
  private QueryStatus status;
  private boolean isError;
  private String errorString;
  private String elapsedTime;
  private ResultData resultData;

  public Query() {
  }

  public Query(long dbProfileID, String query) {
    this.dbProfileID = dbProfileID;
    this.query = query;
  }

  public Query(Query query) {
    this.dbProfileID = query.getDbProfileID();
    this.query = query.getQuery();
    this.queryID = query.getQueryID();
  }

  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public long getDbProfileID() {
    return dbProfileID;
  }

  public void setDbProfileID(long dbProfileID) {
    this.dbProfileID = dbProfileID;
  }

  public String getQueryID() {
    return queryID;
  }

  public void setQueryID(String queryID) {
    this.queryID = queryID;
  }

  public QueryStatus getStatus() {
    return status;
  }

  public void setStatus(QueryStatus status) {
    this.status = status;
  }

  public boolean isError() {
    return isError;
  }

  public void setError(boolean isError) {
    this.isError = isError;
  }

  public String getErrorString() {
    return errorString;
  }

  public void setErrorString(String errorString) {
    this.errorString = errorString;
  }

  public String getElapsedTime() {
    return elapsedTime;
  }

  public void setElapsedTime(String elapsedTime) {
    this.elapsedTime = elapsedTime;
  }

  public ResultData getResultData() {
    return resultData;
  }

  public void setResultData(ResultData resultData) {
    this.resultData = resultData;
  }

  @Override
  public String toString() {
    return "Query{" +
        "query='" + query + '\'' +
        ", dbProfileID=" + dbProfileID +
        ", queryID='" + queryID + '\'' +
        ", status=" + status +
        ", isError=" + isError +
        ", errorString='" + errorString + '\'' +
        ", elapsedTime='" + elapsedTime + '\'' +
        ", resultData=" + resultData +
        '}';
  }
}
