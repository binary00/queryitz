/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.query.util;

import queryitz.modules.query.domain.DBColumnResult;
import queryitz.modules.query.domain.DBRowResult;
import queryitz.modules.query.domain.Query;
import queryitz.modules.query.domain.ResultData;
import queryitz.modules.user.domain.User;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QueryUtil {

  public static String getAsyncQueryKey(User user, Query query) {
    return user.getId() + query.getQueryID();
  }

  public static boolean isResultSetQuery(String query) {
    // Should only be queries that Select?
    // Betting I'm wrong.

    String formattedQuery = query.toLowerCase();
    formattedQuery = formattedQuery.trim();

    return formattedQuery.startsWith("select");
  }

  public static ResultData getResultData(ResultSet resultSet, int resultsReturned) throws SQLException {
    ResultData resultData = null;

    if(resultSet != null) {
      List<String> columnHeaders = new ArrayList<>();
      List<DBRowResult> rowResults = new ArrayList<>();

      boolean firstIter = true;
      while(resultSet.next()) {
        DBRowResult rowResult = new DBRowResult();
        List<DBColumnResult> columnResults = new ArrayList<>();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        for(int i = 1; i <= columnCount; i++){
          DBColumnResult columnResult = new DBColumnResult();
          int type = metaData.getColumnType(i);
          String columnName = metaData.getColumnName(i);

          if(firstIter) columnHeaders.add(columnName);
          columnResult.setValue(resultSet.getObject(i));
          columnResult.setSqlType(type);
          columnResult.setName(columnName);
          columnResult.setSize(metaData.getColumnDisplaySize(i));
          columnResults.add(columnResult);
        }

        rowResult.setColumns(columnResults);
        rowResults.add(rowResult);
        firstIter = false;
      }

      resultData = new ResultData(columnHeaders, rowResults, rowResults.size());
    } else if(resultsReturned != -1) {
      resultData = new ResultData(null, null, resultsReturned);
    }

    return resultData;
  }
}
