/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.query.async;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import queryitz.modules.query.domain.Query;
import queryitz.modules.query.service.QueryService;
import queryitz.modules.user.domain.User;

import java.util.concurrent.Callable;

import static queryitz.modules.user.util.UserUtils.createUserDetails;
import static queryitz.modules.user.util.UserUtils.getGrantedAuthorities;

public class AsyncQueryCallable implements Callable<Query> {

  private Query query;
  private User user;
  private QueryService queryService;

  public AsyncQueryCallable(Query query, User user, QueryService queryService) {
    this.query = query;
    this.user = user;
    this.queryService = queryService;
  }

  @Override
  public Query call() throws Exception {
    // Spring security context doesn't propagate to new
    // threads. Have to set it manually. Needed for role based
    // methods.
    SecurityContext ctx = new SecurityContextImpl();
    ctx.setAuthentication(new UsernamePasswordAuthenticationToken(createUserDetails(user), null, getGrantedAuthorities(user)));
    SecurityContextHolder.setContext(ctx);

    return queryService.doQuery(query);
  }
}
