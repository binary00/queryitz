/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.query.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import queryitz.modules.query.domain.Query;
import queryitz.modules.query.service.QueryService;
import queryitz.modules.user.domain.User;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;

import static queryitz.modules.query.util.QueryUtil.getAsyncQueryKey;

public class AsyncQueryCallback implements ListenableFutureCallback<Query> {

  static final Logger LOG = LoggerFactory.getLogger(AsyncQueryCallback.class);

  private Query query;
  private User user;
  private QueryService queryService;
  private ConcurrentHashMap<String, ListenableFuture<Query>> futures;

  public AsyncQueryCallback(Query query, User user, QueryService queryService, ConcurrentHashMap<String, ListenableFuture<Query>> futures) {
    this.query = query;
    this.user = user;
    this.queryService = queryService;
    this.futures = futures;
  }

  @Override
  public void onSuccess(Query result) {
    queryService.sendQueryUpdateToUser(result, user);

    futures.remove(getAsyncQueryKey(user, query));
  }

  @Override
  public void onFailure(Throwable t) {
    if(!(t instanceof CancellationException)) {
      LOG.error("Async Query Failed: ", t);
    }

    futures.remove(getAsyncQueryKey(user, query));
  }
}
