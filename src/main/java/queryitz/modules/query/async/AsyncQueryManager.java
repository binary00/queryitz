/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.query.async;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import queryitz.modules.query.domain.Query;
import queryitz.modules.query.service.QueryService;
import queryitz.modules.user.domain.User;

import java.util.concurrent.ConcurrentHashMap;

import static queryitz.modules.query.util.QueryUtil.getAsyncQueryKey;

@Service
public class AsyncQueryManager {

  @Autowired private QueryService queryService;

  private ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
  private ConcurrentHashMap<String, ListenableFuture<Query>> futures = new ConcurrentHashMap<>();

  public AsyncQueryManager() {
    executor.initialize();
  }

  public void startQuery(Query query, User user) {

    ListenableFuture<Query> queryFuture = executor.submitListenable(new AsyncQueryCallable(query, user, queryService));
    futures.put(getAsyncQueryKey(user, query), queryFuture);

    queryFuture.addCallback(new AsyncQueryCallback(query, user, queryService, futures));
  }

  public boolean killQuery(Query query, User user) {

    ListenableFuture<Query> queryFuture = futures.remove(getAsyncQueryKey(user, query));
    if(queryFuture != null) {
      // TODO this should kill the actual JDBC query if the DB supports it.
      queryFuture.cancel(true);
      futures.remove(getAsyncQueryKey(user, query));
      return true;
    }

    return false;
  }
}
