/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.permission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.hierarchicalroles.NullRoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.model.Sid;
import org.springframework.security.acls.model.SidRetrievalStrategy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import queryitz.modules.permission.domain.TeamSid;
import queryitz.modules.permission.domain.UserSid;
import queryitz.modules.team.domain.Team;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.service.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class SidRetrievalStrategyImpl implements SidRetrievalStrategy {

  private RoleHierarchy roleHierarchy = new NullRoleHierarchy();
  @Autowired private UserService userService;

  @Override
  public List<Sid> getSids(Authentication authentication) {
    List<Sid> sids = new ArrayList();
    User user = userService.getUserByUserName(authentication.getName());

    if(user != null) {
      sids.add(new UserSid(user));

      Set<Team> teams = user.getTeams();
      if(teams != null) {
        sids.addAll(teams.stream().map(TeamSid::new).collect(Collectors.toList()));
      }
    }

    Collection<? extends GrantedAuthority> authorities = roleHierarchy.getReachableGrantedAuthorities(authentication.getAuthorities());
    sids.addAll(authorities.stream().map(GrantedAuthoritySid::new).collect(Collectors.toList()));

    return sids;
  }
}
