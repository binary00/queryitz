/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.permission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.acls.domain.DefaultPermissionFactory;
import org.springframework.security.acls.domain.ObjectIdentityRetrievalStrategyImpl;
import org.springframework.security.acls.domain.PermissionFactory;
import org.springframework.security.acls.model.*;
import org.springframework.security.core.Authentication;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class CustomPermissionEvaluator implements PermissionEvaluator {

  @Autowired private AclService aclService;
  @Autowired private SidRetrievalStrategy sidRetrievalStrategy;
  private ObjectIdentityRetrievalStrategy objectIdentityRetrievalStrategy = new ObjectIdentityRetrievalStrategyImpl();
  private ObjectIdentityGenerator objectIdentityGenerator = new ObjectIdentityRetrievalStrategyImpl();
  private PermissionFactory permissionFactory = new DefaultPermissionFactory();

  @Override
  public boolean hasPermission(Authentication authentication, Object domainObject, Object permission) {
    if(domainObject != null) {

      ObjectIdentity objectIdentity = objectIdentityRetrievalStrategy.getObjectIdentity(domainObject);
      return checkPermission(authentication, objectIdentity, permission);
    }

    return false;
  }

  @Override
  public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
    ObjectIdentity objectIdentity = objectIdentityGenerator.createObjectIdentity(targetId, targetType);

    return checkPermission(authentication, objectIdentity, permission);
  }

  private boolean checkPermission(Authentication authentication, ObjectIdentity oid, Object permission) {

    List<Sid> sids = sidRetrievalStrategy.getSids(authentication);
    List<Permission> requiredPermission = resolvePermission(permission);

    Acl acl = aclService.readAclById(oid, sids);

    try {
      if(acl.isGranted(requiredPermission, sids, false)) {
        return true;
      }
    } catch(NotFoundException e) {

    }

    return false;
  }

  private List<Permission> resolvePermission(Object permission) {
    if(permission instanceof String) {
      String permString = (String)permission;
      Permission p;

      try {
        p = permissionFactory.buildFromName(permString);
      } catch(IllegalArgumentException notfound) {
        p = permissionFactory.buildFromName(permString.toUpperCase());
      }

      if(p != null){
        return Arrays.asList(p);
      }
    }

    return null;
  }
}
