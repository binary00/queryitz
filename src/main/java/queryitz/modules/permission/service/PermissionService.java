/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.permission.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.model.*;
import org.springframework.stereotype.Service;
import queryitz.modules.dbprofile.domain.DBProfile;

import java.util.Arrays;
import java.util.List;

import static org.springframework.security.acls.domain.BasePermission.*;

@Service
public class PermissionService {

  @Autowired private MutableAclService aclService;

  public void addPermission(DBProfile dbProfile, Sid recipient, Permission permission) {
    MutableAcl acl;
    ObjectIdentity oid = new ObjectIdentityImpl(dbProfile);

    try {
      acl = (MutableAcl) aclService.readAclById(oid);
    } catch (NotFoundException nfe) {
      acl = aclService.createAcl(oid);
    }

    addPermissions(permission, recipient, acl);
    aclService.updateAcl(acl);
  }

  public MutableAcl getPermissions(DBProfile dbProfile) {
    try {
      ObjectIdentity oid = new ObjectIdentityImpl(dbProfile);
      return (MutableAcl) aclService.readAclById(oid);
    } catch(Exception e) {
      return null;
    }
  }

  public void removePermissions(DBProfile dbProfile, Sid recipient) {
    ObjectIdentity oid = new ObjectIdentityImpl(dbProfile);

    try {
      MutableAcl acl = (MutableAcl) aclService.readAclById(oid);
      List<AccessControlEntry> aces = acl.getEntries();

      for(int i = 0; i < aces.size(); i++) {
        AccessControlEntry ace = aces.get(i);
        Sid aceSid = ace.getSid();

        if(aceSid.equals(recipient)) {
          acl.deleteAce(i);
          break;
        }
      }

    } catch(Exception e) {}
  }

  public void removeAllPermissions(DBProfile dbProfile) {
    ObjectIdentity oid = new ObjectIdentityImpl(dbProfile);

    try {
      aclService.deleteAcl(oid, true);
    } catch(Exception e){}
  }

  /**
   * Permission hierarchy isn't by default with Spring Security. Need to add
   * lesser permissions of the initial permission. This was mainly for DBProfiles,
   * may want to re-address if used elsewhere.
   *
   * @param permission
   * @param recipient
   * @param acl
   */
  private void addPermissions(Permission permission, Sid recipient, MutableAcl acl) {
    acl.insertAce(acl.getEntries().size(), permission, recipient, true);

    List<Permission> lesserPerms = null;
    switch(permission.getMask()) {
      case 16: // Administration
        lesserPerms = Arrays.asList(DELETE, CREATE, WRITE, READ);
        break;
      case 8: // Delete
        lesserPerms = Arrays.asList(WRITE, READ);
        break;
      case 4: // Create
        lesserPerms = Arrays.asList(WRITE, READ);
        break;
      case 2: // Write
        lesserPerms = Arrays.asList(READ);
        break;
      case 1: // Read
        break;
    }

    if(lesserPerms != null) {
      lesserPerms.forEach(perm -> acl.insertAce(acl.getEntries().size(), perm, recipient, true));
    }
  }
}
