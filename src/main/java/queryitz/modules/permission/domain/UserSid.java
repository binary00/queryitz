/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.permission.domain;

import org.springframework.security.acls.domain.PrincipalSid;
import queryitz.modules.user.domain.User;

public class UserSid extends PrincipalSid {

  private static final String USER = "U_";

  public UserSid(String principal) {
    super(format(principal));
  }

  public UserSid(User user) {
    super(format(user.getUsername()));
  }

  private static String format(String principal) {
    return principal;
  }
}
