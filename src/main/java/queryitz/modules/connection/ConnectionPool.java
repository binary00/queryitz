/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.connection;

import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.AbandonedConfig;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;

public class ConnectionPool extends GenericObjectPool<Connection> {

  static final Logger LOG = LoggerFactory.getLogger(ConnectionPool.class);

  public ConnectionPool(PooledObjectFactory<Connection> factory) {
    super(factory);
  }

  public ConnectionPool(PooledObjectFactory<Connection> factory, GenericObjectPoolConfig config) {
    super(factory, config);
  }

  public ConnectionPool(PooledObjectFactory<Connection> factory, GenericObjectPoolConfig config, AbandonedConfig abandonedConfig) {
    super(factory, config, abandonedConfig);
  }

  @Override
  public void returnObject(Connection connection) {

    try {
      if(connection.isClosed()) {
        invalidateObject(connection);
        clear();
      } else {
        super.returnObject(connection);
      }
    } catch(Exception e) {
      LOG.error("", e);

      try {
        invalidateObject(connection);
        clear();
      } catch(Exception e1) {
        LOG.error("", e1);
      }
    }
  }
}
