/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.connection;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.dbprofile.domain.DBProfileCreds;
import queryitz.modules.driver.DriverManager;

import java.sql.Connection;
import java.sql.Driver;
import java.util.Properties;

public class ConnectionFactory extends BasePooledObjectFactory<Connection> {

  static final Logger LOG = LoggerFactory.getLogger(ConnectionFactory.class);

  private DBProfile dbProfile;
  private DBProfileCreds dbProfileCreds;

  public ConnectionFactory(DBProfile dbProfile, DBProfileCreds dbProfileCreds) {
    this.dbProfile = dbProfile;
    this.dbProfileCreds = dbProfileCreds;
  }

  @Override
  public Connection create() throws Exception {
    try {
      Driver driver = DriverManager.getDriver(dbProfile);
      Properties driverProps = new Properties();

      if(!StringUtils.isEmpty(dbProfileCreds.getUsername()) && !StringUtils.isEmpty(dbProfileCreds.getPassword())){
        driverProps.put("user", dbProfileCreds.getUsername());
        driverProps.put("password", dbProfileCreds.getPassword());
      }

      return driver.connect(dbProfile.getConnectionURL(), driverProps);
    } catch (Exception e) {
      LOG.error("Unable to create connection for " + dbProfile.getName(), e);
      throw e; // Need to throw so other classes can catch and message appropriately.
    }
  }

  @Override
  public void destroyObject(PooledObject<Connection> pooledObject) throws Exception {
    try {
      pooledObject.getObject().close();
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      super.destroyObject(pooledObject);
    }
  }

  @Override
  public PooledObject<Connection> wrap(Connection connection) {
    return new DefaultPooledObject<>(connection);
  }
}
