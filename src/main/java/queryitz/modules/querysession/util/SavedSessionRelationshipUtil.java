/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.querysession.util;

import org.springframework.stereotype.Component;
import queryitz.modules.querysession.domain.SavedResult;
import queryitz.modules.querysession.domain.SavedSession;

import java.util.List;

@Component
public class SavedSessionRelationshipUtil {

  public void setRelationships(SavedSession savedSession) {
    List<SavedResult> savedResults = savedSession.getResults();

    if(savedResults != null) {
      for(SavedResult savedResult : savedResults) {
        savedResult.setSavedSession(savedSession);
      }
    }
  }
}
