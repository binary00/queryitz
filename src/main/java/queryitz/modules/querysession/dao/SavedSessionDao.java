/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.querysession.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import queryitz.modules.querysession.domain.SavedSession;
import queryitz.modules.querysession.domain.SavedSessionID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository("savedSessionDao")
public class SavedSessionDao {

  @PersistenceContext
  private EntityManager entityManager;

  public SavedSession save(SavedSession savedSession) {
    getSession().saveOrUpdate(savedSession);

    return savedSession;
  }

  public SavedSession get(Long dbProfileID, Long userID) {
    SavedSessionID id = new SavedSessionID(dbProfileID, userID);

    return (SavedSession) getSession().get(SavedSession.class, id);
  }

  public void delete(Long dbProfileID, Long userID) {
    SavedSession savedSession = get(dbProfileID, userID);
    if(savedSession != null) getSession().delete(savedSession);
  }

  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }
}
