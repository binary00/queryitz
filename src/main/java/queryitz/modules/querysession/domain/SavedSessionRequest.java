/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.querysession.domain;

import java.util.List;

public class SavedSessionRequest {

  private Long dbProfileID;
  private String queries;
  private List<SavedResult> results;

  public Long getDbProfileID() {
    return dbProfileID;
  }

  public void setDbProfileID(Long dbProfileID) {
    this.dbProfileID = dbProfileID;
  }

  public String getQueries() {
    return queries;
  }

  public void setQueries(String queries) {
    this.queries = queries;
  }

  public List<SavedResult> getResults() {
    return results;
  }

  public void setResults(List<SavedResult> results) {
    this.results = results;
  }
}
