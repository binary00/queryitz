/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.querysession.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SAVED_RESULT")
public class SavedResult implements Serializable {

  private Long id;
  private String query;
  private String result;
  private SavedSession savedSession;

  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Lob
  @Column(length = 10000, name = "QUERY")
  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  @Lob
  @Column(length = 10000, name = "RESULT")
  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  @ManyToOne
  @JoinColumns({
      @JoinColumn(name = "DB_PROFILE_ID", referencedColumnName = "DB_PROFILE_ID"),
      @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
  })
  public SavedSession getSavedSession() {
    return savedSession;
  }

  public void setSavedSession(SavedSession savedSession) {
    this.savedSession = savedSession;
  }
}
