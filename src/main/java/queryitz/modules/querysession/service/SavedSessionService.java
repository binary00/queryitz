/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.querysession.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import queryitz.modules.dbprofile.dao.DBProfileDao;
import queryitz.modules.dbprofile.domain.DBProfile;
import queryitz.modules.querysession.dao.SavedSessionDao;
import queryitz.modules.querysession.domain.SavedSession;
import queryitz.modules.querysession.domain.SavedSessionID;
import queryitz.modules.querysession.domain.SavedSessionRequest;
import queryitz.modules.querysession.util.SavedSessionRelationshipUtil;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.service.UserService;

@Service
@Transactional
public class SavedSessionService {

  @Autowired private SavedSessionRelationshipUtil relationshipUtil;
  @Autowired private SavedSessionDao savedSessionDao;
  @Autowired private DBProfileDao dbProfileDao;
  @Autowired private UserService userService;

  public SavedSession createSavedSession(SavedSessionRequest savedSessionRequest) {

    User user = userService.getCurrentUser();
    DBProfile dbProfile = dbProfileDao.getProfile(savedSessionRequest.getDbProfileID());

    if(dbProfile != null && user != null) {
      SavedSessionID savedSessionID = new SavedSessionID();
      savedSessionID.setDbProfileID(dbProfile.getId());
      savedSessionID.setUserID(user.getId());

      SavedSession savedSession = new SavedSession();
      savedSession.setId(savedSessionID);
      savedSession.setResults(savedSessionRequest.getResults());
      savedSession.setQueries(savedSessionRequest.getQueries());

      relationshipUtil.setRelationships(savedSession);
      savedSessionDao.save(savedSession);
      return savedSession;
    }

    return null;
  }

  public SavedSession getSavedSession(Long dbProfileID) {
    User user = userService.getCurrentUser();

    if(user != null) {
      return savedSessionDao.get(dbProfileID, user.getId());
    }

    return null;
  }

  public void deleteSavedSession(Long dbProfileID) {
    User user = userService.getCurrentUser();

    if(user != null) savedSessionDao.delete(dbProfileID, user.getId());
  }
}
