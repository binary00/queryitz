/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.user.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import queryitz.modules.team.domain.Team;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "USERS")
public class User implements Serializable {

  private Long id;
  private String username;
  private String password;
  private String email;
  private boolean enabled;
  private boolean tempPassword;
  private Set<UserRole> userRoles;
  private Map<String, String> attributes = new HashMap<>();
  private Set<Team> teams;
	
  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
	
  @Column(name = "USERNAME", nullable = false, unique = true, length = 255)
  public String getUsername() {
 	return username;
  }
	
  public void setUsername(String username) {
    this.username = username;
  }
	
	@Column(name = "PASSWORD", nullable = false, length = 255)
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

  @Column(name = "EMAIL", nullable = false, unique = true, length = 254)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Column(name = "ENABLED", nullable = false)
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

  @Column(name = "TEMP_PASSWORD", nullable = false)
  public boolean isTempPassword() {
    return tempPassword;
  }

  public void setTempPassword(boolean tempPassword) {
    this.tempPassword = tempPassword;
  }

  @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
  public Set<UserRole> getUserRoles() {
    return userRoles;
  }

  public void setUserRoles(Set<UserRole> userRoles) {
    this.userRoles = userRoles;
  }

  public void addUserAuthority(UserRole userAuthority){
    if(this.userRoles == null) userRoles = new HashSet<>();

    if(userAuthority != null){
      this.userRoles.add(userAuthority);
    }
  }

  @ElementCollection(fetch = FetchType.EAGER)
  @MapKeyColumn(name = "name")
  @Column(name = "value")
  @CollectionTable(name = "USER_ATTR", joinColumns = @JoinColumn(name = "id"))
  public Map<String, String> getAttributes() {
    return attributes;
  }

  public void setAttributes(Map<String, String> attributes) {
    this.attributes = attributes;
  }

  @JsonIgnore
  public String getStringAttr(String name) {
    return getAttributes().get(name);
  }

  public void setStringAttr(String name, String value) {
    this.getAttributes().put(name, value);
  }

  @JsonIgnore
  @ManyToMany(mappedBy = "users", targetEntity = Team.class)
  public Set<Team> getTeams() {
    return teams;
  }

  public void setTeams(Set<Team> teams) {
    this.teams = teams;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    User user = (User) o;

    if(id != null ? !id.equals(user.id) : user.id != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", username='" + username + '\'' +
        ", password='" + password + '\'' +
        ", email='" + email + '\'' +
        ", enabled=" + enabled +
        ", tempPassword=" + tempPassword +
        ", userRoles=" + userRoles +
        ", attributes=" + attributes +
        ", teams=" + teams +
        '}';
  }
}
