/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.user.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import queryitz.modules.user.enums.Role;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "USER_ROLE")
public class UserRole implements Serializable {

	private Long id;
	private Role role;
  private User user;

  public UserRole() {
  }

  public UserRole(User user, Role role) {
    this.user = user;
    this.role = role;
  }

  @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

  @Enumerated(EnumType.STRING)
  @Column(name = "ROLE", nullable = false, length = 50)
  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "USER_ID")
  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
