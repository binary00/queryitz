/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.user.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import queryitz.modules.dbprofile.service.DBProfileService;
import queryitz.modules.team.service.TeamService;
import queryitz.modules.user.dao.UserDao;
import queryitz.modules.user.domain.PasswordChangeRequest;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.domain.UserCreateRequest;
import queryitz.modules.user.domain.UserRole;
import queryitz.modules.user.enums.Role;

import java.util.Set;

import static queryitz.modules.user.constants.Constants.DEFAULT_USER;
import static queryitz.modules.user.constants.UserAttr.GRAVATAR_HASH;

@Service
@Transactional
public class UserServiceImpl implements UserService {

  @Autowired private UserDao userDao;
  @Autowired private PasswordEncoder passwordEncoder;
  @Autowired private TeamService teamService;
  @Autowired private DBProfileService dbProfileService;

  private UserDetails getCurrentUserDetails() {
    UserDetails userDetails = null;

    Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    if(principal != null && principal instanceof UserDetails) {
      userDetails = (UserDetails) principal;
    }

    return userDetails;
  }

  @Override
  public User getCurrentUser() {
    UserDetails userDetails = getCurrentUserDetails();
    if(userDetails != null && userDetails.getUsername() != null) {
      return getUserByUserName(userDetails.getUsername());
    }

    return null;
  }

  @Override
  public User getUserByUserName(String userName) {
    return userDao.getUserByUserName(userName);
  }

  @Override
  public User getUser(Long id) {
    return userDao.getUser(id);
  }

  @Override
  public Set<User> getUsersFromIDs(Set<Long> userIDs) {
    if(userIDs != null) {
      return userDao.getUsersByIds(userIDs);
    }

    return null;
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public User createUser(UserCreateRequest createRequest) {
    User user = createUser(createRequest.getUser(), createRequest.getRole());

    if(user != null) {
      teamService.addUserToTeams(user, createRequest.getTeams());
    }

    return user;
  }

  @Override
  public User createUser(User user, Role role) {

    user.setPassword(passwordEncoder.encode(user.getPassword()));
    UserRole userRole = new UserRole(user, role);
    user.addUserAuthority(userRole);
    user.setStringAttr(GRAVATAR_HASH, DigestUtils.md5Hex(user.getEmail()));

    userDao.saveOrUpdateUser(user);

    return user;
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN') || #user.username == principal.username")
  public void updateUser(User user) {
    userDao.saveOrUpdateUser(user);
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteUser(Long id) throws Exception {
    User user = userDao.getUser(id);

    if(user != null) {

      if(DEFAULT_USER.equals(user.getUsername())) throw new Exception("Cannot delete default user.");

      teamService.deleteUserTeams(user);
      dbProfileService.deleteUserDBProfiles(user);
      dbProfileService.removeAllUserPermissions(user);
      userDao.deleteUser(user);
    }
  }

  @Override
  @PreAuthorize("hasRole('ROLE_ADMIN') || #request.username == principal.username")
  public boolean changePassword(PasswordChangeRequest request) {
    User user = getUserByUserName(request.getUsername());

    if(user != null) {
      String newPassword = passwordEncoder.encode(request.getNewPassword());
      boolean passwordsMatch = passwordEncoder.matches(request.getPassword(), user.getPassword());

      if(passwordsMatch) {
        user.setPassword(newPassword);
        user.setTempPassword(false);

        userDao.saveOrUpdateUser(user);

        if(user.getPassword().equals(newPassword)) {
          return true;
        }
      }
    }

    return false;
  }

  @Override
  public Set<User> getAllUsers() {
    return userDao.getUsers();
  }

  @Override
  public Role[] getAllRoles() {
    return Role.values();
  }
}
