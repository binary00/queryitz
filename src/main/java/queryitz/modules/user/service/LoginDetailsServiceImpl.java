/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import queryitz.modules.user.dao.UserDao;
import queryitz.modules.user.domain.User;

import static queryitz.modules.user.util.UserUtils.createUserDetails;

@Service
@Transactional
public class LoginDetailsServiceImpl implements UserDetailsService {

  @Autowired private UserDao userDAO;

  @Override
  public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException, DataAccessException {
    User user = userDAO.getUserByUserName(userName);

    return getUserDetails(user);
  }

  private UserDetails getUserDetails(User user) {
    return user != null ? createUserDetails(user) : null;
  }
}
