/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.user.service;

import queryitz.modules.user.domain.PasswordChangeRequest;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.domain.UserCreateRequest;
import queryitz.modules.user.enums.Role;

import java.util.Set;

public interface UserService {

  User getCurrentUser();
  User getUserByUserName(String userName);
  User getUser(Long id);
  Set<User> getUsersFromIDs(Set<Long> userIDs);
  User createUser(UserCreateRequest createRequest);
  User createUser(User user, Role role);
  void updateUser(User user);
  void deleteUser(Long id) throws Exception;
  boolean changePassword(PasswordChangeRequest request);
  Set<User> getAllUsers();
  Role[] getAllRoles();
}
