/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.user.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import queryitz.modules.user.domain.PasswordChangeRequest;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.domain.UserCreateRequest;
import queryitz.modules.user.enums.Role;
import queryitz.modules.user.service.UserService;

import java.util.Set;

@RestController
@RequestMapping("users")
public class UsersController {

  @Autowired private UserService userService;

  @RequestMapping(method = RequestMethod.POST)
  public User createUser(@RequestBody UserCreateRequest createRequest) {
    return userService.createUser(createRequest);
  }

  @RequestMapping(value = "{id}", method = RequestMethod.GET)
  public User getUser(@PathVariable Long id) {
    return userService.getUser(id);
  }

  @RequestMapping(value = "username/{username}", method = RequestMethod.GET)
  public User getUserByUsername(@PathVariable String username) {
    return userService.getUserByUserName(username);
  }

  @RequestMapping(method = RequestMethod.GET)
  public Set<User> getAllUsers() {
    return userService.getAllUsers();
  }

  @RequestMapping(value = "roles", method = RequestMethod.GET)
  public Role[] getAllRoles() {
    return userService.getAllRoles();
  }

  @RequestMapping(value = "{id}", method = RequestMethod.PUT)
  public void updateUser(@RequestBody User user) {
    userService.updateUser(user);
  }

  @RequestMapping(value = "{id}/password", method = RequestMethod.PUT)
  public void changePassword(@RequestBody PasswordChangeRequest passwordChangeRequest) throws Exception {
    boolean passwordChanged = userService.changePassword(passwordChangeRequest);
    if(!passwordChanged) {
      throw new Exception("Password Not Changed!");
    }
  }

  @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
  public void deleteUser(@PathVariable Long id) throws Exception {
    userService.deleteUser(id);
  }
}
