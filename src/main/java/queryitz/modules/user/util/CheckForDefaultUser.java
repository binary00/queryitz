/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.user.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import queryitz.modules.user.constants.Constants;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.enums.Role;
import queryitz.modules.user.service.UserService;

import javax.annotation.PostConstruct;

/**
 * Used to create a default user in the system
 * to login in with, otherwise there isn't any
 * access to create users. Also if more bootstrap
 * methods are needed, maybe a Bootstrap service
 * would be more appropriate.
 *
 *
 */
@Component
public class CheckForDefaultUser {

  @Autowired private UserService userService;

  @PostConstruct
  @Transactional
  public void init() {

    User user = userService.getUserByUserName(Constants.DEFAULT_USER);

    if(user == null) {
      user = new User();
      user.setUsername(Constants.DEFAULT_USER);
      user.setPassword(Constants.DEFAULT_USER_PWORD);
      user.setEnabled(true);
      user.setTempPassword(false);  // TODO Filter for env
      user.setEmail("");

      userService.createUser(user, Role.ROLE_ADMIN);
    }
  }
}
