/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.user.util;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import queryitz.modules.user.domain.User;
import queryitz.modules.user.domain.UserRole;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserUtils {

  public static UserDetails createUserDetails(User user) {
    return new org.springframework.security.core.userdetails.User(user.getUsername(),
        user.getPassword(),
        user.isEnabled(),
        true,
        !user.isTempPassword(),
        true,
        getGrantedAuthorities(user));
  }

  public static List<GrantedAuthority> getGrantedAuthorities(User user) {
    Set<UserRole> userRoles = user.getUserRoles();
    List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

    if(userRoles != null) {
      grantedAuthorities.addAll(userRoles.stream().map(userRole -> new SimpleGrantedAuthority(userRole.getRole().name())).collect(Collectors.toList()));
    }

    return grantedAuthorities;
  }
}
