/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.modules.user.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import queryitz.modules.user.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;

@Repository("userDao")
public class UserDaoImpl implements UserDao {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public Set<User> getUsers() {
    Criteria criteria = getSession().createCriteria(User.class);
    return new HashSet<>(criteria.list());
  }

  @Override
  public Set<User> getUsersByIds(Set<Long> ids) {
    Criteria criteria = getSession().createCriteria(User.class);
    criteria.add(Restrictions.in("id", ids));

    return new HashSet<>(criteria.list());
  }

  @Override
	public void saveOrUpdateUser(User user) {
    Session session = getSession();

    session.saveOrUpdate(user);
    session.flush();
    session.refresh(user);
	}

  @Override
	public User getUserByUserName(String userName) {

    Criteria criteria = getSession().createCriteria(User.class);
    criteria.add(Restrictions.eq("username", userName));

    return (User) criteria.uniqueResult();
	}

  @Override
  public User getUser(long id) {
    return (User) getSession().get(User.class, id);
  }

  @Override
  public void deleteUser(User user) {
    getSession().delete(user);
  }

  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }
}
