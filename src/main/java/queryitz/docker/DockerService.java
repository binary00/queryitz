/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.docker;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.*;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.core.command.LogContainerResultCallback;
import com.github.dockerjava.core.command.PullImageResultCallback;
import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.apache.commons.fileupload.util.Streams.asString;
import static org.junit.Assert.assertThat;

@Service
public class DockerService {

  static final Logger LOG = LoggerFactory.getLogger(DockerService.class);

  @Autowired private DockerClient dockerClient;

  public boolean loadImage(String imageName) {

    try {
      LOG.info("Pulling image " + imageName + ". If this is the first time, it can take a bit.");
      dockerClient.pullImageCmd(imageName)
          .withTag("latest")
          .exec(new PullImageResultCallback())
          .awaitCompletion();

    } catch(Exception e) {
      LOG.error("Unable to get image : " + imageName, e);
      return false;
    }

    return true;
  }

  public boolean startupContainer(ContainerStartupConfig startupConfig) {
    Stopwatch containerStopWatch = Stopwatch.createStarted();

    if(startupConfig.checkImage()) loadImage(startupConfig.getImageName());
    if(startupConfig.cleanOldContainer()) resetContainer(startupConfig.getContainerName());

    String tag = "";//startupConfig.getImageTag() == null ? "latest" : startupConfig.getImageTag();
    String containerImage = startupConfig.getImageName();//startupConfig.getImageName().concat(":").concat(tag);

    final Ports ports = new Ports();
    if(startupConfig.getPortBindings() != null) {
      startupConfig
          .getPortBindings()
          .forEach((internalPort, externalPort) -> ports.bind(ExposedPort.tcp(internalPort), Ports.Binding(externalPort)));
    }

    CreateContainerCmd createContainerCmd = dockerClient
        .createContainerCmd(containerImage)
        .withName(startupConfig.getContainerName())
        .withPortBindings(ports);

    if(startupConfig.isPrivileged()) createContainerCmd.withPrivileged(true);

    CreateContainerResponse createContainerResponse = createContainerCmd.exec();
    String containerID = createContainerResponse.getId();

    dockerClient
        .startContainerCmd(containerID)
        .exec();

    InspectContainerResponse.ContainerState containerState = dockerClient
        .inspectContainerCmd(containerID)
        .exec()
        .getState();

    if(!containerState.isRunning()) {
      return false;
    }

    LogVerify logVerify = startupConfig.getLogVerify();
    if(logVerify != null) {
      String contains = logVerify.getContains();
      long wait = logVerify.getWait();
      long startTime = System.currentTimeMillis();
      long elapsedTime;
      LogContainerResultCallback resultCallback;

      do {
        resultCallback = dockerClient
            .logContainerCmd(containerID)
            .withStdOut()
            .withStdErr()
            .withTail(10)
            .exec(new LogContainerResultCallback());

        try {
          resultCallback.awaitCompletion();
          if(resultCallback.toString().contains(contains)) break;
        } catch(Exception e) {
          LOG.error("Couldn't read log : ", e);
        }

        try {
          Thread.sleep(200);
        } catch(InterruptedException e) {}

        elapsedTime = (System.currentTimeMillis() - startTime);
      } while(elapsedTime < wait);

      LOG.info(startupConfig.getContainerName() + " took " + containerStopWatch.toString());
      return resultCallback.toString().contains(contains);
    }

    LOG.info(startupConfig.getContainerName() + " took " + containerStopWatch.toString());
    return true;
  }

  public void resetContainer(String containerName) {

    ListContainersCmd listContainersCmd = dockerClient.listContainersCmd();
    listContainersCmd.withShowAll(true);

    List<Container> containers = listContainersCmd.exec();
    containers.forEach(container -> {
      try {
        if(container.getNames() != null && container.getNames().length > 0 && ("/" + containerName).equals(container.getNames()[0])) {

          InspectContainerResponse.ContainerState containerState = dockerClient
              .inspectContainerCmd(container.getId())
              .exec()
              .getState();

          if(containerState.isRunning()) {
            dockerClient.stopContainerCmd(container.getId()).exec();
          }

          dockerClient.removeContainerCmd(container.getId()).exec();
        }
      } catch(Exception e) {
        LOG.error("Failed to reset container: ", e);
      }
    });
  }
}
