/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.docker;

import java.util.HashMap;
import java.util.Map;

public class ContainerStartupConfig {

  private String containerName;
  private String imageName;
  private String imageTag;
  private Map<Integer, Integer> portBindings;
  private boolean checkImage;
  private boolean cleanOldContainer;
  private boolean privileged;
  private LogVerify logVerify;

  public ContainerStartupConfig() {
  }

  public ContainerStartupConfig(String containerName, String imageName) {
    this.containerName = containerName;
    this.imageName = imageName;
  }

  public ContainerStartupConfig(String containerName, String imageName, String imageTag) {
    this.containerName = containerName;
    this.imageName = imageName;
    this.imageTag = imageTag;
  }

  public String getContainerName() {
    return containerName;
  }

  public void setContainerName(String containerName) {
    this.containerName = containerName;
  }

  public String getImageName() {
    return imageName;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public String getImageTag() {
    return imageTag;
  }

  public void setImageTag(String imageTag) {
    this.imageTag = imageTag;
  }

  public Map<Integer, Integer> getPortBindings() {
    return portBindings;
  }

  public void setPortBindings(Map<Integer, Integer> portBindings) {
    this.portBindings = portBindings;
  }

  public void addPortBinding(int internalPort, int externalPort) {
    if(portBindings == null) portBindings = new HashMap<>();
    portBindings.put(internalPort, externalPort);
  }

  public boolean checkImage() {
    return checkImage;
  }

  public void setCheckImage(boolean checkImage) {
    this.checkImage = checkImage;
  }

  public boolean cleanOldContainer() {
    return cleanOldContainer;
  }

  public void setCleanOldContainer(boolean cleanOldContainer) {
    this.cleanOldContainer = cleanOldContainer;
  }

  public boolean isPrivileged() {
    return privileged;
  }

  public void setPrivileged(boolean privileged) {
    this.privileged = privileged;
  }

  public LogVerify getLogVerify() {
    return logVerify;
  }

  public void setLogVerify(LogVerify logVerify) {
    this.logVerify = logVerify;
  }
}
