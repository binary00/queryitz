/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package queryitz.docker;

public class LogVerify {

  private long wait = 120000; // Defualt is 2 mins
  private String contains;

  public LogVerify() {
  }

  public LogVerify(String contains) {
    this.contains = contains;
  }

  public LogVerify(long wait, String contains) {
    this.wait = wait;
    this.contains = contains;
  }

  public long getWait() {
    return wait;
  }

  public void setWait(long wait) {
    this.wait = wait;
  }

  public String getContains() {
    return contains;
  }

  public void setContains(String contains) {
    this.contains = contains;
  }
}
