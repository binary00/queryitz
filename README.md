# DEVELOPER INSTRUCTIONS #

## TO GET STARTED ##
1. Java 8 + JCE Unlimited Strength install
2. Maven
3. Docker installed and running
4. Run: mvn clean spring-boot:run or Application.java
5. Hit: https://localhost:8443/ - default username/pass is admin/admin

- To run with postgresql:  --spring.config.location=classpath:/ci/prod.properties

-- Tables Spring Security ACL - http://docs.spring.io/spring-security/site/docs/3.0.x/reference/appendix-schema.html#dbschema-acl --